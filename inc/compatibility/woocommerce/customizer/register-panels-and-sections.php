<?php
/**
 * Register customizer panels & sections fro Woocommerce.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

/**
 * WooCommerce
 */
$wp_customize->add_section(
	new Wiz_WP_Customize_Section(
		$wp_customize, 'section-woo-group',
		array(
			'title'    => __( 'WooCommerce', 'wiz' ),
			'panel'    => 'panel-layout',
			'priority' => 60,
		)
	)
);

$wp_customize->add_section(
	new Wiz_WP_Customize_Section(
		$wp_customize, 'section-woo-general',
		array(
			'title'    => __( 'General', 'wiz' ),
			'panel'    => 'panel-layout',
			'section'  => 'section-woo-group',
			'priority' => 5,
		)
	)
);
$wp_customize->add_section(
	new Wiz_WP_Customize_Section(
		$wp_customize, 'section-woo-shop',
		array(
			'title'    => __( 'Shop', 'wiz' ),
			'panel'    => 'panel-layout',
			'section'  => 'section-woo-group',
			'priority' => 10,
		)
	)
);

$wp_customize->add_section(
	new Wiz_WP_Customize_Section(
		$wp_customize, 'section-woo-shop-single',
		array(
			'title'    => __( 'Single Product', 'wiz' ),
			'panel'    => 'panel-layout',
			'section'  => 'section-woo-group',
			'priority' => 15,
		)
	)
);

$wp_customize->add_section(
	new Wiz_WP_Customize_Section(
		$wp_customize, 'section-woo-shop-cart',
		array(
			'title'    => __( 'Cart Page', 'wiz' ),
			'panel'    => 'panel-layout',
			'section'  => 'section-woo-group',
			'priority' => 20,
		)
	)
);
