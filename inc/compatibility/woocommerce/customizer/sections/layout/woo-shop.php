<?php
/**
* WooCommerce Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.1.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Shop Columns
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-grids]', array(
        'default'           => array(
            'desktop'      => '',
            'tablet'       => '',
            'mobile'       => '',
            'desktop-unit' => 'col',
            'tablet-unit'  => 'col',
            'mobile-unit'  => 'col',
        ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[shop-grids]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-woo-shop',
            'priority'       => 10,
            'label'          => __( 'Shop Columns', 'wiz' ),
            'unit_choices'   => array( 'col' ),
            'units_attrs'   => array(
                'col' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 6,
                ),
            ),
        )
    )
);

/**
* Option: Products Per Page
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-no-of-products]', array(
        'default'           => wiz_get_option( 'shop-no-of-products' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[shop-no-of-products]', array(
        'section'     => 'section-woo-shop',
        'label'       => __( 'Products Per Page', 'wiz' ),
        'type'        => 'number',
        'priority'    => 15,
        'input_attrs' => array(
            'min'  => 1,
            'step' => 1,
            'max'  => 50,
        ),
    )
);

/**
* Option: Product Hover Style
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-hover-style]', array(
        'default'           => wiz_get_option( 'shop-hover-style' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);

$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[shop-hover-style]', array(
        'type'     => 'select',
        'section'  => 'section-woo-shop',
        'priority' => 20,
        'label'    => __( 'Product Image Hover Style', 'wiz' ),
        'choices'  => apply_filters(
            'wiz_woo_shop_hover_style',
            array(
                ''     => __( 'None', 'wiz' ),
                'swap' => __( 'Swap Images', 'wiz' ),
            )
        ),
    )
);

/**
* Option: Single Post Meta
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-product-structure]', array(
        'default'           => wiz_get_option( 'shop-product-structure' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Sortable(
        $wp_customize, WIZ_THEME_SETTINGS . '[shop-product-structure]', array(
            'type'     => 'leap-sortable',
            'section'  => 'section-woo-shop',
            'priority' => 30,
            'label'    => __( 'Shop Product Structure', 'wiz' ),
            'choices'  => array(
                'title'      => __( 'Title', 'wiz' ),
                'price'      => __( 'Price', 'wiz' ),
                'ratings'    => __( 'Ratings', 'wiz' ),
                'short_desc' => __( 'Short Description', 'wiz' ),
                'add_cart'   => __( 'Add To Cart', 'wiz' ),
                'category'   => __( 'Category', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Shop Archive Content Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-archive-width]', array(
        'default'           => wiz_get_option( 'shop-archive-width' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[shop-archive-width]', array(
        'type'     => 'select',
        'section'  => 'section-woo-shop',
        'priority' => 220,
        'label'    => __( 'Shop Archive Content Width', 'wiz' ),
        'choices'  => array(
            'default' => __( 'Default', 'wiz' ),
            'custom'  => __( 'Custom', 'wiz' ),
        ),
    )
);

/**
* Option: Enter Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[shop-archive-max-width]', array(
        'default'           => 1200,
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[shop-archive-max-width]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-woo-shop',
            'priority'    => 225,
            'label'       => __( 'Enter Width', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 768,
                'step' => 1,
                'max'  => 1920,
            ),
        )
    )
);
