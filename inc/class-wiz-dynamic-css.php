<?php
/**
 * Custom Styling output for Wiz Theme.
 *
 * @package     Wiz
 * @subpackage  Class
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Dynamic CSS
 */
if ( ! class_exists( 'Wiz_Dynamic_CSS' ) ) {

	/**
	 * Dynamic CSS
	 */
	class Wiz_Dynamic_CSS {

		/**
		 * Return CSS Output
		 *
		 * @return string Generated CSS.
		 */
		static public function return_output() {

			$dynamic_css = '';

			/**
			 *
			 * Contents
			 * - Variable Declaration
			 * - Global CSS
			 * - Typography
			 * - Page Layout
			 *   - Sidebar Positions CSS
			 *      - Full Width Layout CSS
			 *   - Fluid Width Layout CSS
			 *   - Box Layout CSS
			 *   - Padded Layout CSS
			 * - Blog
			 *   - Single Blog
			 * - Typography of Headings
			 * - Header
			 * - Footer
			 *   - Main Footer CSS
			 *     - Small Footer CSS
			 * 	   - Go Top Link
			 * - 404 Page
			 * - Secondary
			 * - Global CSS
			 */

			/**
			 * - Variable Declaration
			 */
			$site_content_width = wiz_get_option( 'site-content-width', 1200 );
			$header_logo_width  = wiz_get_option( 'leap-header-responsive-logo-width' );
            $site_identity_spacing = wiz_get_option( 'site-identity-spacing' );
			
			// Site Background Color.
			$box_bg_obj = wiz_get_option( 'site-layout-outside-bg-obj' );
            
        	 // Boxed inner Options
			$box_bg_inner_boxed = wiz_get_option( 'site-boxed-inner-bg' );
            $container_inner_spacing     = wiz_get_option( 'container-inner-spacing' );

			// Color Options.
			$text_color       = wiz_get_option( 'text-color' );
			$theme_color      = wiz_get_option( 'theme-color' );
			$link_color       = wiz_get_option( 'link-color', $theme_color );
			$link_hover_color = wiz_get_option( 'link-h-color' );
			
			// Typography.
			$body_font_size                  = wiz_get_option( 'font-size-body' );
			$body_line_height                = wiz_get_option( 'body-line-height' );
			$para_margin_bottom              = wiz_get_option( 'para-margin-bottom' );
			$body_text_transform             = wiz_get_option( 'body-text-transform' );
			$headings_font_family            = wiz_get_option( 'headings-font-family' );
			$headings_font_weight            = wiz_get_option( 'headings-font-weight' );
			$headings_text_transform         = wiz_get_option( 'headings-text-transform' );
			$site_title_font_size            = wiz_get_option( 'site-title-font-size' );
			$site_title_color                = wiz_get_option( 'site-title-color' );
			$site_title_h_color              = wiz_get_option( 'site-title-h-color' );
			$site_tagline_font_size          = wiz_get_option( 'font-size-site-tagline' );
			$single_post_title_font_size     = wiz_get_option( 'font-size-entry-title' );
         	$single_post_title_font_color    = wiz_get_option( 'font-color-entry-title' );
			$archive_summary_title_font_size = wiz_get_option( 'font-size-archive-summary-title' );
			$archive_post_title_font_size    = wiz_get_option( 'font-size-page-title' );
			$heading_h1_font_size            = wiz_get_option( 'font-size-h1' );
			$heading_h2_font_size            = wiz_get_option( 'font-size-h2' );
			$heading_h3_font_size            = wiz_get_option( 'font-size-h3' );
			$heading_h4_font_size            = wiz_get_option( 'font-size-h4' );
			$heading_h5_font_size            = wiz_get_option( 'font-size-h5' );
			$heading_h6_font_size            	= wiz_get_option( 'font-size-h6' );

			//Menu Items
			$menu_font_family 				= wiz_get_option( 'menu-items-font-family' );
			$menu_font_weight 				= wiz_get_option( 'menu-items-font-weight' );
			$menu_line_height                = wiz_get_option( 'menu-items-line-height' );
			$menu_link_bottom_border_color   = wiz_get_option( 'menu-link-bottom-border-color');
			$menu_text_transform             = wiz_get_option( 'menu-items-text-transform' );
			$menu_font_size					= wiz_get_option( 'menu-font-size' );

			// Sub Menu Typography
			$sub_menu_font_family 				= wiz_get_option( 'sub-menu-items-font-family' );
			$sub_menu_font_weight 				= wiz_get_option( 'sub-menu-items-font-weight' );
			$sub_menu_line_height                = wiz_get_option( 'sub-menu-items-line-height' );
			$sub_menu_text_transform             = wiz_get_option( 'sub-menu-items-text-transform' );

			//Layout Header
			$header_bg_obj             = wiz_get_option( 'header-bg-obj' );
			$space_header              = wiz_get_option( 'header-padding' );
			$header_separator       = wiz_get_option( 'header-main-sep' );
			
			// header menu
			$menu_bg_color            = wiz_get_option( 'menu-bg-color' );
			$menu_link_color         = wiz_get_option( 'menu-link-color' );
			$menu_link_h_color         = wiz_get_option( 'menu-link-h-color' );
			$menu_link_active_color         = wiz_get_option( 'menu-link-active-color' );
			$last_menu_items_spacing		= wiz_get_option( 'last-menu-item-spacing' );
			// SubMenu Top Border.
			$submenu_top_border_size       = wiz_get_option( 'submenu-top-border-size' );
			$submenu_top_border_color       = wiz_get_option( 'submenu-top-border-color' );
			// Mobile Menu Options
			$mobile_menu_icon_color        = wiz_get_option('mobile-menu-icon-color');
			$mobile_menu_icon_bg_color        = wiz_get_option('mobile-menu-icon-bg-color');
			$mobile_menu_icon_h_color        = wiz_get_option('mobile-menu-icon-h-color');
			$mobile_menu_icon_bg_h_color        = wiz_get_option('mobile-menu-icon-bg-h-color');
			$mobile_menu_items_color        = wiz_get_option('mobile-menu-items-color');
			$mobile_menu_items_bg_color        = wiz_get_option('mobile-menu-items-bg-color');
			$mobile_menu_items_h_color        = wiz_get_option('mobile-menu-items-h-color');

			//header submenu
			$submenu_bg_color               = wiz_get_option( 'submenu-bg-color' );
			$submenu_link_color             = wiz_get_option( 'submenu-link-color' );
			$submenu_link_h_color           = wiz_get_option( 'submenu-link-h-color' );
			$display_submenu_border  		= wiz_get_option( 'display-submenu-border' );
			$submenu_border_color  			= wiz_get_option( 'submenu-border-color' );

			//Content Heading Color
			$heading_h1_font_color            = wiz_get_option( 'font-color-h1' );
			$heading_h2_font_color            = wiz_get_option( 'font-color-h2' );
			$heading_h3_font_color            = wiz_get_option( 'font-color-h3' );
			$heading_h4_font_color            = wiz_get_option( 'font-color-h4' );
			$heading_h5_font_color            = wiz_get_option( 'font-color-h5' );
			$heading_h6_font_color            = wiz_get_option( 'font-color-h6' );

			// Button Styling.
			$btn_border_radius      = wiz_get_option( 'button-radius' );
			$btn_vertical_padding   = wiz_get_option( 'button-v-padding' );
			$btn_horizontal_padding = wiz_get_option( 'button-h-padding' );
			$highlight_link_color   = wiz_get_foreground_color( $link_color );
			$highlight_theme_color  = wiz_get_foreground_color( $theme_color );
			

			//Content
			$content_text_color         = wiz_get_option( 'content-text-color' );
			$content_link_color         = wiz_get_option( 'content-link-color' );
			$content_link_h_color         = wiz_get_option( 'content-link-h-color' );

			//Listing Post Page
			$listing_post_title_color         = wiz_get_option( 'listing-post-title-color' );
			$listing_post_content_color         = wiz_get_option( 'post-content-color' );
			$readmore_text_color      = wiz_get_option( 'readmore-text-color' );
			$meta_color      = wiz_get_option( 'post-meta-color' );
			$readmore_text_h_color    = wiz_get_option( 'readmore-text-h-color' );
			$readmore_padding    = wiz_get_option( 'readmore-padding' );
			$readmore_bg_color    = wiz_get_option( 'readmore-bg-color' );
			$readmore_bg_h_color   = wiz_get_option( 'readmore-bg-h-color' );
			$readmore_border_radius    = wiz_get_option( 'readmore-border-radius' );
			$readmore_border_size     = wiz_get_option( 'readmore-border-size' );
			$readmore_border_color    = wiz_get_option( 'readmore-border-color' );
			$readmore_border_h_color  = wiz_get_option( 'readmore-border-h-color' );
			$archive_post_meta_font_size = wiz_get_option( 'font-size-page-meta' );
            //Footer Font
			$footer_font_family            = wiz_get_option( 'footer-font-family' );
			$footer_font_weight            = wiz_get_option( 'footer-font-weight' );
			$footer_text_transform         = wiz_get_option( 'footer-text-transform' );
			$footer_line_height            = wiz_get_option( 'footer-line-height' );
			$footer_font_size              = wiz_get_option( 'footer-font-size' );

			// Footer Styling
			$space_footer        = wiz_get_option('footer-padding');
			$wiz_footer_widget_title_font_size  = wiz_get_option( 'wiz-footer-widget-title-font-size' );
			$wiz_footer_wgt_title_font_family    = wiz_get_option( 'wiz-footer-wgt-title-font-family' );
			$wiz_footer_wgt_title_font_weight    = wiz_get_option( 'wiz-footer-wgt-title-font-weight' );
			$wiz_footer_wgt_title_text_transform = wiz_get_option( 'wiz-footer-wgt-title-text-transform' );
			$wiz_footer_wgt_title_line_height    = wiz_get_option( 'wiz-footer-wgt-title-line-height' );
			// Footer widget Spacing
			$wiz_footer_space_widget = wiz_get_option('footer-widget-padding');


			// Footer widget Meta color
			$wiz_footer_widget_meta_color = wiz_get_option( 'wiz-footer-widget-meta-color' );

			// Footer Bar Colors.
			$footer_bg_obj       = wiz_get_option( 'footer-bg-obj' );
			$footer_color        = wiz_get_option( 'footer-color' );
			$footer_link_color   = wiz_get_option( 'footer-link-color' );
			$footer_link_h_color = wiz_get_option( 'footer-link-h-color' );

			// Footer Button color 
			$footer_button_color      = wiz_get_option( 'footer-button-color' );
			$footer_button_hover_color = wiz_get_option( 'footer-button-h-color' );
			$footer_button_bg_color    = wiz_get_option( 'footer-button-bg-color' );
			$footer_button_bg_h_color  = wiz_get_option( 'footer-button-bg-h-color' );
			$footer_button_border_radius      = wiz_get_option( 'footer-button-radius' );

			//Footer Input Color 
			$footer_input_color        = wiz_get_option( 'footer-input-color' );
			$footer_input_bg_color     = wiz_get_option( 'footer-input-bg-color' );
			$footer_input_border_color     = wiz_get_option( 'footer-input-border-color' );

			// Footer Bar Font.
			$footer_sml_font_size        = wiz_get_option( 'footer-copyright-font-size' );

			// Color.
			$wiz_footer_bg_obj             = wiz_get_option( 'wiz-footer-bg-obj' );
			$wiz_footer_text_color         = wiz_get_option( 'wiz-footer-text-color' );
			$wiz_footer_widget_title_color = wiz_get_option( 'wiz-footer-wgt-title-color' );
			$wiz_footer_link_color         = wiz_get_option( 'wiz-footer-link-color' );
			$wiz_footer_link_h_color       = wiz_get_option( 'wiz-footer-link-h-color' );

			// sidebar input color 
			$sidebar_input_color        = wiz_get_option( 'sidebar-input-color' );
			$sidebar_input_bg_color     = wiz_get_option( 'sidebar-input-bg-color' );
			$sidebar_input_border_color     = wiz_get_option( 'sidebar-input-border-color' );

			/**
			 * Apply text color depends on link color
			 */
			$btn_text_color = wiz_get_option( 'button-color' );
			if ( empty( $btn_text_color ) ) {
				$btn_text_color = wiz_get_foreground_color( $theme_color );
			}

			//sidebar
			$sidebar_bg_obj               = wiz_get_option( 'sidebar-bg-obj' );
			$sidebar_padding                = wiz_get_option( 'sidebar-padding' );
			$sidebar_text_color           = wiz_get_option( 'sidebar-text-color' );
			$sidebar_link_color           = wiz_get_option( 'sidebar-link-color' );
			$sidebar_link_h_color         = wiz_get_option( 'sidebar-link-h-color' );
			$Widget_bg_color              = wiz_get_option( 'Widget-bg-color' );
			$space_widget                 = wiz_get_option('widget-padding');
			$widget_margin_bottom         = wiz_get_option( 'widget-margin-bottom' );

			//Sidebar Widget Titles Sidebar 
			$widget_title_font_family            = wiz_get_option( 'widget-title-font-family' );
			$widget_title_font_weight            = wiz_get_option( 'widget-title-font-weight' );
			$widget_title_text_transform         = wiz_get_option( 'widget-title-text-transform' );
			$widget_title_line_height            = wiz_get_option( 'widget-title-line-height' );
			$widget_title_font_size              = wiz_get_option( 'widget-title-font-size' );
			$widget_title_color                  = wiz_get_option('widget-title-color');

			

			/**
			 * Apply text hover color depends on link hover color
			 */
			$btn_text_hover_color = wiz_get_option( 'button-h-color' );
			if ( empty( $btn_text_hover_color ) ) {
				$btn_text_hover_color = wiz_get_foreground_color( $link_hover_color );
			}
			$btn_bg_color       = wiz_get_option( 'button-bg-color', $theme_color );
			$btn_border_size     = wiz_get_option( 'btn-border-size' );
			$btn_border_color    = wiz_get_option( 'btn-border-color' );
			$btn_border_h_color  = wiz_get_option( 'btn-border-h-color' );				
			$btn_bg_hover_color = wiz_get_option( 'button-bg-h-color', $link_hover_color );

			// Spacing of Big Footer.
			$copyright_footer_divider_color = wiz_get_option( 'footer-copyright-divider-color' );
			$copyright_footer_divider       = wiz_get_option( 'footer-copyright-divider' );

			/**
			 * Small Footer Styling
			 */
			$copyright_footer_layout = wiz_get_option( 'copyright-footer-layout', 'copyright-footer-layout-1' );
			$wiz_footer_width  = wiz_get_option( 'footer-layout-width' );

			// Blog Post Title Typography Options.
			$single_post_max       = wiz_get_option( 'blog-single-width' );
			$single_post_max_width = wiz_get_option( 'blog-single-max-width' );
			$blog_width            = wiz_get_option( 'blog-width' );
			$blog_max_width        = wiz_get_option( 'blog-max-width' );

			//Search Style
			$search_input_bg_color = wiz_get_option( 'search-input-bg-color' );
			$search_input_color = wiz_get_option( 'search-input-color' );
			$search_border_color         = Wiz_get_option('search-border-color');
			$search_btn_bg_color         = wiz_get_option('search-btn-bg-color'); 
            $search_btn_h_bg_color       = wiz_get_option('search-btn-h-bg-color'); 
            $search_btn_color            = wiz_get_option('search-btn-color'); 
            $search_border_size     = wiz_get_option( 'search-border-size' );
			$css_output = array();
			// Body Font Family.
			$body_font_family = wiz_body_font_family();
			$body_font_weight = wiz_get_option( 'body-font-weight' );

			if ( is_array( $body_font_size ) ) {
				$body_font_size_desktop = ( isset( $body_font_size['desktop'] ) && '' != $body_font_size['desktop'] ) ? $body_font_size['desktop'] : 15;
			} else {
				$body_font_size_desktop = ( '' != $body_font_size ) ? $body_font_size : 15;
			}
			$css_output = array(

				// HTML.
				'html'                                    => array(
					'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 6.25, '%' ),
				),
				'a, .page-title'                          => array(
					'color' => esc_attr( $link_color ),
				),
				'a:hover, a:focus'                        => array(
					'color' => esc_attr( $link_hover_color ),
				),
				'body, button, input, select, textarea'   => array(
					'font-family'    => wiz_get_font_family( $body_font_family ),
					'font-weight'    => esc_attr( $body_font_weight ),
					'font-size'      => wiz_responsive_slider( $body_font_size, 'desktop' ),
					'line-height'    => esc_attr( $body_line_height ),
					'text-transform' => esc_attr( $body_text_transform ),
				),
				'p, .entry-content p'                     => array(
					'margin-bottom' => wiz_get_css_value( $para_margin_bottom, 'em' ),
				),
				'h1, .entry-content h1, .entry-content h1 a, h2, .entry-content h2, .entry-content h2 a, h3, .entry-content h3, .entry-content h3 a, h4, .entry-content h4, .entry-content h4 a, h5, .entry-content h5, .entry-content h5 a, h6, .entry-content h6, .entry-content h6 a, .site-title, .site-title a' => array(
					'font-family'    => wiz_get_css_value( $headings_font_family, 'font' ),
					'font-weight'    => wiz_get_css_value( $headings_font_weight, 'font' ),
					'text-transform' => esc_attr( $headings_text_transform ),
				),
				'.site-title'                             => array(
					'font-size' => wiz_responsive_slider( $site_title_font_size, 'desktop' ),
				),
				'.site-title a'                           => array(
					'color'     => esc_attr( $site_title_color ),
				),
				'.site-title a:hover'                           => array(
					'color'     => esc_attr( $site_title_h_color ),
				),
				'#sitehead .site-logo-img .custom-logo-link img' => array(
					'max-width' => wiz_responsive_slider($header_logo_width,'desktop'),
				),
				'.wiz-logo-svg'                         => array(
					'width' => wiz_responsive_slider($header_logo_width,'desktop'),
				),
                /* Site Identity Spacing */
				'#sitehead.site-header .leap-site-identity'  => array(
					'padding-top'    => wiz_responsive_spacing( $site_identity_spacing, 'top', 'desktop' ),
					'padding-right'  => wiz_responsive_spacing( $site_identity_spacing, 'right', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $site_identity_spacing, 'bottom', 'desktop' ),
					'padding-left'   => wiz_responsive_spacing( $site_identity_spacing, 'left', 'desktop' ),
				),
				'.leap-archive-description .leap-archive-title' => array(
					'font-size' => wiz_responsive_slider( $archive_summary_title_font_size, 'desktop' ),
				),
				'.site-header .site-description'          => array(
					'font-size' => wiz_responsive_slider( $site_tagline_font_size, 'desktop' ),
				),
				'body:not(.leap-single-post) .entry-title' => array(
					'font-size' => wiz_responsive_slider( $archive_post_title_font_size, 'desktop' ),
				),
				'body:not(.leap-single-post) .entry-meta' => array(
					'font-size' => wiz_responsive_slider( $archive_post_meta_font_size, 'desktop' ),
					'color' => esc_attr( $meta_color ),
				),
				'body:not(.leap-single-post) .entry-meta *' => array(
					'color' => esc_attr( $meta_color ),
				),
				'.comment-reply-title'                    => array(
					'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 1.66666 ),
				),
				'.leap-comment-list #cancel-comment-reply-link' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'desktop' ),
				),
				'h1, .entry-content h1, .entry-content h1 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h1_font_size, 'desktop' ),
				),
				'h2, .entry-content h2, .entry-content h2 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h2_font_size, 'desktop' ),
				),
				'h3, .entry-content h3, .entry-content h3 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h3_font_size, 'desktop' ),
				),
				'h4, .entry-content h4, .entry-content h4 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h4_font_size, 'desktop' ),
				),
				'h5, .entry-content h5, .entry-content h5 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h5_font_size, 'desktop' ),
				),
				'h6, .entry-content h6, .entry-content h6 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h6_font_size, 'desktop' ),
				),
				'.leap-single-post .entry-title' => array(
					'font-size' => wiz_responsive_slider( $single_post_title_font_size, 'desktop' ),
                    'color' => wiz_hex_to_rgba( $single_post_title_font_color ),
				),
				'#secondary, #secondary button, #secondary input, #secondary select, #secondary textarea' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'desktop' ),
				),

				// Global CSS.
				'::selection'                             => array(
					'background-color' => esc_attr( $theme_color ),
					'color'            => esc_attr( $highlight_theme_color ),
				),
				'body, h1, .entry-title a, .entry-content h1, .entry-content h1 a, h2, .entry-content h2, .entry-content h2 a, h3, .entry-content h3, .entry-content h3 a, h4, .entry-content h4, .entry-content h4 a, h5, .entry-content h5, .entry-content h5 a, h6, .entry-content h6, .entry-content h6 a' => array(
					'color' => esc_attr( $text_color ),
				),

				// Typography.
				'.tagcloud a:hover, .tagcloud a:focus, .tagcloud a.current-item' => array(
					'color'            => wiz_get_foreground_color( $link_color ),
					'border-color'     => esc_attr( $link_color ),
					'background-color' => esc_attr( $link_color ),
				),

				// Header - Main Header CSS.
				'.leap-header-custom-item a' => array(
					'color' => esc_attr( $text_color ),
				),
				'.main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon' => array(
					'border-bottom-width' => wiz_responsive_slider( $header_separator, 'desktop' ),
				),
				// Main - Menu Items.
				'.main-header-menu li:hover > a, .main-header-menu li:hover > .leap-menu-toggle, .main-header-menu .leap-sitehead-custom-menu-items a:hover, .main-header-menu li.focus > a, .main-header-menu li.focus > .leap-menu-toggle, .main-header-menu .current-menu-item > a, .main-header-menu .current-menu-ancestor > a, .main-header-menu .current_page_item > a' => array(
					'color' => esc_attr( $link_color ),
				),
				// Mobile Menu Color
				'.leap-mobile-menu-buttons .menu-toggle .menu-toggle-icon ' => array(
					'color'  => esc_attr( $mobile_menu_icon_color ),
				),
				'.leap-mobile-menu-buttons .menu-toggle' => array(
					'background-color'  => esc_attr( $mobile_menu_icon_bg_color ),
				),
				'.leap-mobile-menu-buttons .menu-toggle:hover, .leap-mobile-menu-buttons .menu-toggle.toggled' => array(
					'background-color'  => esc_attr( $mobile_menu_icon_bg_h_color ),
				),
				'.leap-mobile-menu-buttons .menu-toggle:hover .menu-toggle-icon, .leap-mobile-menu-buttons .menu-toggle.toggled .menu-toggle-icon' => array(
					'color'  => esc_attr( $mobile_menu_icon_h_color ),
				),
				'.toggle-on li a, .menu-toggle.toggled .menu-item-has-children>.leap-menu-toggle, .leap-header-break-point .toggle-on .menu-item-has-children>.leap-menu-toggle' => array(
					'color'  => esc_attr( $mobile_menu_items_color ),
				),
				'.toggle-on .main-header-menu li a:hover, .toggle-on .main-header-menu li.current-menu-item a, .toggle-on .main-header-menu li.current_page_item a, .toggle-on .main-header-menu .current-menu-ancestor > a, .leap-header-break-point .toggle-on .menu-item-has-children:hover>.leap-menu-toggle, .leap-header-break-point .toggle-on .menu-item-has-children.current_page_item>.leap-menu-toggle, .toggle-on .main-header-menu li a:hover, .toggle-on .main-header-menu li.current-menu-item a, .toggle-on .main-header-menu li.current_page_item a, .toggle-on .main-header-menu .current-menu-ancestor > a, .toggle-on .main-header-menu .sub-menu li:hover a' => array(
					'color'  => esc_attr( $mobile_menu_items_h_color ),
				),
				'.toggle-on .main-header-menu' => array(
					'background-color' => esc_attr( $mobile_menu_items_bg_color ),
				),
				'.toggle-on .main-header-menu .menu-item:hover > a' => array(
					'border-bottom-color'  => esc_attr( $mobile_menu_items_h_color ),
				),

				// Input tags.
				'input:focus, input[type="text"]:focus, input[type="email"]:focus, input[type="url"]:focus, input[type="password"]:focus, input[type="reset"]:focus, input[type="search"]:focus, textarea:focus' => array(
					'border-color' => esc_attr( $link_color ),
				),
				'input[type="radio"]:checked, input[type=reset], input[type="checkbox"]:checked, input[type="checkbox"]:hover:checked, input[type="checkbox"]:focus:checked, input[type=range]::-webkit-slider-thumb' => array(
					'border-color'     => esc_attr( $link_color ),
					'background-color' => esc_attr( $link_color ),
					'box-shadow'       => 'none',
				),
			    //header menu
				'.main-header-menu '  => array(
					'background-color' => esc_attr( $menu_bg_color),
				),
				'.main-header-menu a'  => array(
					'color' => esc_attr( $menu_link_color ),
					'font-family'    => wiz_get_font_family( $menu_font_family ),
					'font-weight'     => esc_attr( $menu_font_weight ),
					'text-transform'  => esc_attr( $menu_text_transform ),
					'font-size'	 	=> wiz_responsive_slider( $menu_font_size , 'desktop' ),		
				),
				'.main-header-menu > .menu-item > a, .main-header-menu > .menu-item'  => array(
					'line-height' => esc_attr( $menu_line_height ),
				), 
				'.main-header-menu > .menu-item:hover > a'  => array(
					'border-bottom-color' => esc_attr( $menu_link_bottom_border_color ),
				),
				'.main-header-menu li:hover a, .main-header-menu .leap-sitehead-custom-menu-items a:hover' => array (
					'color' => esc_attr( $menu_link_h_color ),
				),
				' .main-header-menu li.current-menu-item a, .main-header-menu li.current_page_item a, .main-header-menu .current-menu-ancestor > a'  => array(
					'color' => esc_attr( $menu_link_active_color ),
				),
				'.site-header .leap-sitehead-custom-menu-items' => array(
                'padding-top'    => wiz_responsive_spacing( $last_menu_items_spacing, 'top', 'desktop' ),
                'padding-bottom' => wiz_responsive_spacing( $last_menu_items_spacing, 'bottom', 'desktop' ),
                'padding-right' => wiz_responsive_spacing( $last_menu_items_spacing, 'right', 'desktop' ),
                'padding-left'  => wiz_responsive_spacing( $last_menu_items_spacing, 'left', 'desktop' ),
            	),
				//submenu
				'.main-header-menu ul.sub-menu'  => array(
					'background-color' => esc_attr( $submenu_bg_color),
					'border-top-width' => wiz_get_css_value( $submenu_top_border_size, 'px' ),
					'border-top-color' => esc_attr( $submenu_top_border_color ),
				),

				'.main-header-menu .sub-menu a' => array(
					'border-bottom-width' => ( true == $display_submenu_border ) ? '1px' : '0px',
					'border-style'        => 'solid',
					'border-bottom-color'        => esc_attr( $submenu_border_color ),
				),
				
				'.main-header-menu .sub-menu li a'  => array(
					'color' => esc_attr( $submenu_link_color ),
					'font-family'    => wiz_get_font_family( $sub_menu_font_family ),
					'font-weight'     => esc_attr( $sub_menu_font_weight ),
					'text-transform'  => esc_attr( $sub_menu_text_transform ),
					'line-height' => esc_attr( $sub_menu_line_height ),
				),
				'.main-header-menu .sub-menu li:hover > a'  => array(
					'color' => esc_attr( $submenu_link_h_color ),
				),

				// Small Footer.
				'.site-footer a:hover + .post-count, .site-footer a:focus + .post-count' => array(
					'background'   => esc_attr( $link_color ),
					'border-color' => esc_attr( $link_color ),
				),

				'.leap-footer-copyright'                       => array(
					'color' => esc_attr( $footer_color ),
					'font-size' => wiz_responsive_slider( $footer_sml_font_size, 'desktop' ),
				),
				'.leap-footer-copyright > .leap-footer-copyright-content' => wiz_get_background_obj( $footer_bg_obj ),

				'.leap-footer-copyright a'                     => array(
					'color' => esc_attr( $footer_link_color ),
				),
				'.leap-footer-copyright a:hover'               => array(
					'color' => esc_attr( $footer_link_h_color ),
				),

				// main Fotter styling/colors/fonts.
				'.wiz-footer .widget-title,.wiz-footer .widget-title a' => array(
					'color' => esc_attr( $wiz_footer_widget_title_color ),
				),

				'.site-footer'                          => array(
					'color' => esc_attr( $wiz_footer_text_color ),
				),
				
				'.wiz-footer .post-date'               => array(
					'color' => esc_attr( $wiz_footer_widget_meta_color),
				),

				'.wiz-footer button, .wiz-footer .leap-button, .wiz-footer .button, .wiz-footer input#submit, .wiz-footer input[type=button], .wiz-footer input[type=submit], .wiz-footerinput[type=reset]'  => array(
					'color' => esc_attr( $footer_button_color),
					'background-color' => esc_attr( $footer_button_bg_color),
					'border-radius'    => wiz_responsive_slider( $footer_button_border_radius, 'desktop' ),
				),

				'.wiz-footer'  => array(
					'font-size'      => wiz_responsive_slider( $footer_font_size , 'desktop' ),
					'font-family'    => wiz_get_font_family( $footer_font_family ),
					'font-weight'    => esc_attr( $footer_font_weight ),
					'text-transform' => esc_attr( $footer_text_transform ),
					'line-height'    => esc_attr( $footer_line_height ),
				),

				'.wiz-footer button:focus, .wiz-footer button:hover, .wiz-footer .leap-button:hover, .wiz-footer .button:hover, .wiz-footer input[type=reset]:hover, .wiz-footer input[type=reset]:focus, .wiz-footer input#submit:hover, .wiz-footer input#submit:focus, .wiz-footer input[type="button"]:hover, .wiz-footer input[type="button"]:focus, .wiz-footer input[type="submit"]:hover, .wiz-footer input[type="submit"]:focus' => array(
					'color' => esc_attr( $footer_button_hover_color),
					'background-color' => esc_attr( $footer_button_bg_h_color),
				),
				//header spacing
				'.main-header-bar' => array(
					'padding-top'    => wiz_responsive_spacing( $space_header, 'top', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $space_header, 'bottom', 'desktop' ),
					'padding-right' => wiz_responsive_spacing( $space_header, 'right', 'desktop' ),
					'padding-left'  => wiz_responsive_spacing( $space_header, 'left', 'desktop' ),
				),

				'.wiz-footer .leap-container' => array(
				'padding-top'    => wiz_responsive_spacing( $space_footer, 'top', 'desktop' ),
				'padding-bottom' => wiz_responsive_spacing( $space_footer, 'bottom', 'desktop' ),
				'padding-right' => wiz_responsive_spacing( $space_footer, 'right', 'desktop' ),
				'padding-left'  => wiz_responsive_spacing( $space_footer, 'left', 'desktop' ),
				),

				'.wiz-footer .widget-title'       => array(
					'font-size' => wiz_responsive_slider( $wiz_footer_widget_title_font_size , 'desktop' ),
                    'font-family'    => wiz_get_font_family( $wiz_footer_wgt_title_font_family ),
					'font-weight'    => esc_attr( $wiz_footer_wgt_title_font_weight ),
					'text-transform' => esc_attr( $wiz_footer_wgt_title_text_transform ),
					'line-height'    => esc_attr( $wiz_footer_wgt_title_line_height ),
				),	
                
				'.wiz-footer a'                           => array(
					'color' => esc_attr( $wiz_footer_link_color ),
				),

				'.wiz-footer .tagcloud a:hover, .wiz-footer .tagcloud a.current-item' => array(
					'border-color'     => esc_attr( $wiz_footer_link_color ),
					'background-color' => esc_attr( $wiz_footer_link_color ),
				),

				'.wiz-footer a:hover, .wiz-footer .no-widget-text a:hover, .wiz-footer a:focus, .wiz-footer .no-widget-text a:focus' => array(
					'color' => esc_attr( $wiz_footer_link_h_color ),
				),

				'.wiz-footer .calendar_wrap #today, .wiz-footer a:hover + .post-count' => array(
					'background-color' => esc_attr( $wiz_footer_link_color ),
				),

				'.wiz-footer input,.wiz-footer input[type="text"],.wiz-footer input[type="email"],.wiz-footer input[type="url"],.wiz-footer input[type="password"],.wiz-footer input[type="reset"],.wiz-footer input[type="search"],.wiz-footer textarea'  => array(
					'color' => esc_attr( $footer_input_color),
					'background' => esc_attr( $footer_input_bg_color),
					'border-color' => esc_attr( $footer_input_border_color),
				),

				'.wiz-footer-overlay'                     => wiz_get_background_obj( $wiz_footer_bg_obj ),
				'.wiz-footer .wiz-footer-widget' => array(
					'padding-top'    => wiz_responsive_spacing( $wiz_footer_space_widget,'top'   ,'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $wiz_footer_space_widget,'bottom','desktop' ),
					'padding-right'  => wiz_responsive_spacing( $wiz_footer_space_widget,'right' ,'desktop' ),
					'padding-left'   => wiz_responsive_spacing( $wiz_footer_space_widget,'left'  ,'desktop' ),
				),

				// Single Post Meta.
				'.leap-comment-meta'                       => array(
					'line-height' => '1.666666667',
					'font-size'   => wiz_get_font_css_value( (int) $body_font_size_desktop * 0.8571428571 ),
				),
				'.single .nav-links .nav-previous, .single .nav-links .nav-next, .single .leap-author-details .author-title, .leap-comment-meta' => array(
					'color' => esc_attr( $link_color ),
				),

				// Button Typography.
				'.menu-toggle, button, .leap-button, input[type=button], input[type=button]:focus, input[type=button]:hover, input[type=reset], input[type=reset]:focus, input[type=reset]:hover, input[type=submit], input[type=submit]:focus, input[type=submit]:hover' => array(
					'border-radius'    => wiz_responsive_slider( $btn_border_radius, 'desktop' ),
					'padding'          => wiz_get_css_value( $btn_vertical_padding, 'px' ) . ' ' . wiz_get_css_value( $btn_horizontal_padding, 'px' ),
					'color'            => esc_attr( $btn_text_color ),
					'background-color' => esc_attr( $btn_bg_color ),
					'border' 		   => 'solid',
					'border-color'     => esc_attr( $btn_border_color ),
					'border-width'     => wiz_get_css_value( $btn_border_size , 'px' ),
				),
				'button:focus, .menu-toggle:hover, button:hover, .leap-button:hover, .button:hover, input[type=reset]:hover, input[type=reset]:focus, input#submit:hover, input#submit:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="submit"]:hover, input[type="submit"]:focus' => array(
					'color'            => esc_attr( $btn_text_hover_color ),
					'border-color'     => esc_attr( $btn_border_h_color ),
					'background-color' => esc_attr( $btn_bg_hover_color ),
				),
				'.search-submit, .search-submit:hover, .search-submit:focus' => array(
					'color'            => wiz_get_foreground_color( $link_color ),
					'background-color' => esc_attr( $link_color ),
				),
				//Content
				'.entry-content' =>  array(
					'color' => esc_attr( $content_text_color ),
				),
				//Content Link color
				'.entry-content a' =>  array(
					'color' => esc_attr( $content_link_color ),
				),
				//Content Link Hover Color
				'.entry-content a:hover' =>  array(
					'color' => esc_attr( $content_link_h_color ),
				),
				//Listing Post Page
				'.content-area .entry-title a' =>  array(
					'color' => esc_attr( $listing_post_title_color ),
				),
				'.content-area .read-more' => array(
					'color' => esc_attr( $readmore_text_color ),
					'padding-top'    => wiz_responsive_spacing( $readmore_padding, 'top', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $readmore_padding, 'bottom', 'desktop' ),
					'padding-right' => wiz_responsive_spacing( $readmore_padding, 'right', 'desktop' ),
					'padding-left'  => wiz_responsive_spacing( $readmore_padding, 'left', 'desktop' ),
				),
				'.content-area .read-more a' => array(
					'color' => esc_attr( $readmore_text_color ),
					'padding-top'    => wiz_responsive_spacing( $readmore_padding, 'top', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $readmore_padding, 'bottom', 'desktop' ),
					'padding-right' => wiz_responsive_spacing( $readmore_padding, 'right', 'desktop' ),
					'padding-left'  => wiz_responsive_spacing( $readmore_padding, 'left', 'desktop' ),
					'background-color' => esc_attr( $readmore_bg_color),
					'border-radius'    => wiz_responsive_slider( $readmore_border_radius, 'desktop' ),
					'border-color'     => esc_attr( $readmore_border_color),
					'border-width' => wiz_responsive_slider( $readmore_border_size , 'desktop'),
				),
				'.content-area .read-more a:hover' =>  array(
					'color' => esc_attr( $readmore_text_h_color ),
					'background-color'  => esc_attr ( $readmore_bg_h_color ),
					'border-color'     => esc_attr( $readmore_border_h_color),
				),
				
				//Content Heading Color
				' h1, .entry-content h1, .entry-content h1 a' =>  array(
					'color' => esc_attr( $heading_h1_font_color ),
				),
				' h2, .entry-content h2, .entry-content h2 a' =>  array(
					'color' => esc_attr( $heading_h2_font_color ),
				),
				' h3, .entry-content h3, .entry-content h3 a' =>  array(
					'color' => esc_attr( $heading_h3_font_color ),
				),
				' h4, .entry-content h4, .entry-content h4 a' =>  array(
					'color' => esc_attr( $heading_h4_font_color ),
				),
				' h5, .entry-content h5, .entry-content h5 a' =>  array(
					'color' => esc_attr( $heading_h5_font_color ),
				),
				' h6, .entry-content h6, .entry-content h6 a' =>  array(
					'color' => esc_attr( $heading_h6_font_color ),
				),

				// Blog Post Meta Typography.
				'.entry-meta, .entry-meta *'              => array(
					'line-height' => '1.45',
					'color'       => esc_attr( $link_color ),
				),
				'.entry-meta a:hover, .entry-meta a:hover *, .entry-meta a:focus, .entry-meta a:focus *' => array(
					'color' => esc_attr( $link_hover_color ),
				),

				//sidebar
				'.sidebar-main' => wiz_get_background_obj( $sidebar_bg_obj ),

				'div.sidebar-main' => array(
					'padding-top'    => wiz_responsive_spacing( $sidebar_padding, 'top', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $sidebar_padding, 'bottom', 'desktop' ),
					'padding-right' => wiz_responsive_spacing( $sidebar_padding, 'right', 'desktop' ),
					'padding-left'  => wiz_responsive_spacing( $sidebar_padding, 'left', 'desktop' ),
				),
				'.sidebar-main *' =>  array(
					'color' => esc_attr( $sidebar_text_color ),
				),
				'.sidebar-main a' =>  array(
					'color' => esc_attr( $sidebar_link_color ),
				),
				'.sidebar-main a:hover' =>  array(
					'color' => esc_attr( $sidebar_link_h_color ),
				),
				'.sidebar-main .widget'                     => array(
					'margin-bottom' => wiz_get_css_value( $widget_margin_bottom, 'em' ),
					'background-color' => esc_attr( $Widget_bg_color),
				),
				//sidebar input style 
				'.sidebar-main input,.sidebar-main input[type="text"],.sidebar-main input[type="email"],.sidebar-main input[type="url"],.sidebar-main input[type="password"],.sidebar-main input[type="reset"],.sidebar-main input[type="search"],.sidebar-main textarea ,.sidebar-main select'  => array(
					'color' => esc_attr( $sidebar_input_color),
					'background' => esc_attr( $sidebar_input_bg_color),
					'border-color' => esc_attr( $sidebar_input_border_color),
				),

				//Sidebar Widget Titles Sidebar 
				'.sidebar-main .widget-title '   => array(
						'font-family'    => wiz_get_font_family( $widget_title_font_family ),
						'font-weight'    => esc_attr( $widget_title_font_weight ),
						'font-size'      => wiz_responsive_slider( $widget_title_font_size, 'desktop' ),
						'line-height'    => esc_attr( $widget_title_line_height ),
						'text-transform' => esc_attr( $widget_title_text_transform ),
						'color'          => esc_attr( $widget_title_color ),
				),

				//widget Spacing
				'.sidebar-main ' => array(
					'padding-top'    => wiz_responsive_spacing( $space_widget, 'top', 'desktop' ),
					'padding-bottom' => wiz_responsive_spacing( $space_widget, 'bottom', 'desktop' ),
					'padding-right' => wiz_responsive_spacing( $space_widget, 'right', 'desktop' ),
					'padding-left'  => wiz_responsive_spacing( $space_widget, 'left', 'desktop' ),
				),
			 	//layout header
			    '.site-header .main-header-bar '  => wiz_get_background_obj( $header_bg_obj ),
				// Blockquote Text Color.
				'blockquote, blockquote a'                => array(
					'color' => wiz_adjust_brightness( $text_color, 75, 'darken' ),
				),

				// 404 Page.
				'.leap-404-layout .leap-404-text'         => array(
					'font-size' => wiz_get_font_css_value( '200' ),
				),

				// Widget Title.
				'.widget-title'                           => array(
					'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 1.428571429 ),
					'color'     => esc_attr( $text_color ),
				),
				'#cat option, .secondary .calendar_wrap thead a, .secondary .calendar_wrap thead a:visited' => array(
					'color' => esc_attr( $link_color ),
				),
				'.secondary .calendar_wrap #today, .leap-progress-val span' => array(
					'background' => esc_attr( $link_color ),
				),
				'.secondary a:hover + .post-count, .secondary a:focus + .post-count' => array(
					'background'   => esc_attr( $link_color ),
					'border-color' => esc_attr( $link_color ),
				),
				'.calendar_wrap #today > a'               => array(
					'color' => wiz_get_foreground_color( $link_color ),
				),

				// Pagination.
				'.leap-pagination a, .page-links .page-link, .single .post-navigation a' => array(
					'color' => esc_attr( $link_color ),
				),
				'.leap-pagination a:hover, .leap-pagination a:focus, .leap-pagination > span:hover:not(.dots), .leap-pagination > span.current, .page-links > .page-link, .page-links .page-link:hover, .post-navigation a:hover' => array(
					'color' => esc_attr( $link_hover_color ),
				),

            // Layout - Container
            '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single, .leap-separate-container .leap-woocommerce-container' => wiz_get_background_obj( $box_bg_inner_boxed ),
    
            /**
            * Content Spacing Desktop
            */
            '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single, .leap-separate-container .leap-woocommerce-container ' => array(
            'padding-top'    => wiz_responsive_spacing( $container_inner_spacing, 'top', 'desktop' ),
            'padding-bottom' => wiz_responsive_spacing( $container_inner_spacing, 'bottom', 'desktop' ),
            'padding-right' => wiz_responsive_spacing( $container_inner_spacing, 'right', 'desktop' ),
            'padding-left'  => wiz_responsive_spacing( $container_inner_spacing, 'left', 'desktop' ),
			   ),
			//Search From Style
			'.leap-search-menu-icon form .search-field' => array(
					'background-color' => esc_attr($search_input_bg_color),
					'color' 		=> esc_attr($search_input_color),
				),
			'.leap-search-menu-icon form' => array(
					'border-color' => esc_attr($search_border_color),
                    'background-color' => esc_attr($search_border_color),
				),
			'.leap-search-menu-icon .search-submit' => array(
                    'background-color' => esc_attr($search_btn_bg_color),
                    'color' => esc_attr($search_btn_color),
                ),
                '.leap-search-menu-icon .search-submit:hover' => array(
					'background-color' => esc_attr($search_btn_h_bg_color),
				),
			'.search-box .leap-search-menu-icon form , .top-bar-search-box .wiz-top-header-section .leap-search-menu-icon .search-form' => array(
					'border-width'     => wiz_get_css_value( $search_border_size , 'px' , '0' ),
				),				
			);

			/* Parse CSS from array() */
			$parse_css = wiz_parse_css( $css_output );

			// Foreground color.
			if ( ! empty( $wiz_footer_link_color ) ) {
				$wiz_footer_tagcloud = array(
					'.wiz-footer .tagcloud a:hover, .wiz-footer .tagcloud a.current-item' => array(
						'color' => wiz_get_foreground_color( $wiz_footer_link_color ),
					),
					'.wiz-footer .calendar_wrap #today' => array(
						'color' => wiz_get_foreground_color( $wiz_footer_link_color ),
					),
				);
				$parse_css          .= wiz_parse_css( $wiz_footer_tagcloud );
			}

			/* Width for Footer */
			if ( 'content' != $wiz_footer_width ) {
				$genral_global_responsive = array(
					'.leap-footer-copyright .leap-container' => array(
						'max-width'     => '100%',
						'padding-left'  => '35px',
						'padding-right' => '35px',
					),
				);

				/* Parse CSS from array()*/
				$parse_css .= wiz_parse_css( $genral_global_responsive, '769' );
			}

			/* Width for Comments for Full Width / Stretched Template */
			$page_builder_comment = array(
				'.leap-page-builder-template .comments-area, .single.leap-page-builder-template .entry-header, .single.leap-page-builder-template .post-navigation' => array(
					'max-width'    => wiz_get_css_value( $site_content_width + 40, 'px' ),
					'margin-left'  => 'auto',
					'margin-right' => 'auto',
				),
			);

			/* Parse CSS from array()*/
			$parse_css .= wiz_parse_css( $page_builder_comment, '545' );

			$separate_container_css = array(
				'body, .leap-separate-container' => wiz_get_background_obj( $box_bg_obj ),
            
			);
			$parse_css             .= wiz_parse_css( $separate_container_css );

			$tablet_typo = array();

			if ( isset( $body_font_size['tablet'] ) && '' != $body_font_size['tablet'] ) {

					$tablet_typo = array(
						'.comment-reply-title' => array(
							'font-size' => wiz_get_font_css_value( (int) $body_font_size['tablet'] * 1.66666, 'px', 'tablet' ),
						),
						// Single Post Meta.
						'.leap-comment-meta'    => array(
							'font-size' => wiz_get_font_css_value( (int) $body_font_size['tablet'] * 0.8571428571, 'px', 'tablet' ),
						),
						// Widget Title.
						'.widget-title'        => array(
							'font-size' => wiz_get_font_css_value( (int) $body_font_size['tablet'] * 1.428571429, 'px', 'tablet' ),
						),
					);
			}

			/* Tablet Typography */
			$tablet_typography = array(
				'body, button, input, select, textarea' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'tablet' ),
				),
				'.leap-comment-list #cancel-comment-reply-link' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'tablet' ),
				),
				'#secondary, #secondary button, #secondary input, #secondary select, #secondary textarea' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'tablet' ),
				),
				'.site-title'                           => array(
					'font-size' => wiz_responsive_slider( $site_title_font_size, 'tablet' ),
				),
				'.leap-footer-copyright'                       => array(
					'font-size' => wiz_responsive_slider( $footer_sml_font_size, 'tablet' ),
				),
				'.wiz-footer .widget-title'                           => array(
				'font-size' => wiz_responsive_slider( $wiz_footer_widget_title_font_size , 'tablet' ),
				),
				'.sidebar-main .widget-title '   => array(
					'font-size'      => wiz_responsive_slider( $widget_title_font_size, 'tablet' ),
				),
				'#sitehead.site-header .leap-site-identity'  => array(
					'padding-top'    => wiz_responsive_spacing( $site_identity_spacing, 'top', 'tablet' ),
					'padding-right'  => wiz_responsive_spacing( $site_identity_spacing, 'right', 'tablet' ),
					'padding-bottom' => wiz_responsive_spacing( $site_identity_spacing, 'bottom', 'tablet' ),
					'padding-left'   => wiz_responsive_spacing( $site_identity_spacing, 'left', 'tablet' ),
				),
				'.content-area .read-more a' => array(
					'border-radius'    => wiz_responsive_slider( $readmore_border_radius, 'tablet' ),
					'border-width' => wiz_responsive_slider( $readmore_border_size , 'tablet'),
				),
				'.site-header .leap-sitehead-custom-menu-items' => array(
                'padding-top'    => wiz_responsive_spacing( $last_menu_items_spacing, 'top', 'tablet' ),
                'padding-bottom' => wiz_responsive_spacing( $last_menu_items_spacing, 'bottom', 'tablet' ),
                'padding-right' => wiz_responsive_spacing( $last_menu_items_spacing, 'right', 'tablet' ),
                'padding-left'  => wiz_responsive_spacing( $last_menu_items_spacing, 'left', 'tablet' ),
				),
				'.main-header-menu a'  => array(
					'font-size'	 	=> wiz_responsive_slider( $menu_font_size , 'tablet' ),		
				),
				// Button Typography.
				'.menu-toggle, button, .leap-button, input[type=button], input[type=button]:focus, input[type=button]:hover, input[type=reset], input[type=reset]:focus, input[type=reset]:hover, input[type=submit], input[type=submit]:focus, input[type=submit]:hover' => array(
					'border-radius'    => wiz_responsive_slider( $btn_border_radius, 'tablet' ),
				),
				//header
				'.main-header-bar' => array(
					'padding-top'    => wiz_responsive_spacing( $space_header, 'top', 'tablet' ),
					'padding-bottom' => wiz_responsive_spacing( $space_header, 'bottom', 'tablet' ),
					'padding-right' => wiz_responsive_spacing( $space_header, 'right', 'tablet' ),
					'padding-left'  => wiz_responsive_spacing( $space_header, 'left', 'tablet' ),
				),
				'.main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon' => array(
					'border-bottom-width' => wiz_responsive_slider( $header_separator, 'tablet' ),
				),
				//Sidebar Spacing
				'.sidebar-main' => array(
					'padding-top'    => wiz_responsive_spacing( $sidebar_padding, 'top', 'tablet' ),
					'padding-bottom' => wiz_responsive_spacing( $sidebar_padding, 'bottom', 'tablet' ),
					'padding-right' => wiz_responsive_spacing( $sidebar_padding, 'right', 'tablet' ),
					'padding-left'  => wiz_responsive_spacing( $sidebar_padding, 'left', 'tablet' ),
				),

				//Widget Spacing
				'.sidebar-main ' => array(
					'padding-top'    => wiz_responsive_spacing( $space_widget, 'top', 'tablet' ),
					'padding-bottom' => wiz_responsive_spacing( $space_widget, 'bottom', 'tablet' ),
					'padding-right' => wiz_responsive_spacing( $space_widget, 'right', 'tablet' ),
					'padding-left'  => wiz_responsive_spacing( $space_widget, 'left', 'tablet' ),
				),
				//post readmore spacing
				'.content-area p.read-more' => array(
					'padding-top'    => wiz_responsive_spacing( $readmore_padding, 'top', 'tablet' ),
					'padding-bottom' => wiz_responsive_spacing( $readmore_padding, 'bottom', 'tablet' ),
					'padding-right' => wiz_responsive_spacing( $readmore_padding, 'right', 'tablet' ),
					'padding-left'  => wiz_responsive_spacing( $readmore_padding, 'left', 'tablet' ),
				),
				'.leap-archive-description .leap-archive-title' => array(
					'font-size' => wiz_responsive_slider( $archive_summary_title_font_size, 'tablet' ),
				),
				'.site-header .site-description'        => array(
					'font-size' => wiz_responsive_slider( $site_tagline_font_size, 'tablet' ),
				),
				'body:not(.leap-single-post) .entry-title'                          => array(
					'font-size' => wiz_responsive_slider( $archive_post_title_font_size, 'tablet' ),
				),
				'body:not(.leap-single-post) .entry-meta'                            => array(
					'font-size' => wiz_responsive_slider( $archive_post_meta_font_size, 'tablet' ),
				),
				'h1, .entry-content h1, .entry-content h1 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h1_font_size, 'tablet' ),
				),
				'h2, .entry-content h2, .entry-content h2 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h2_font_size, 'tablet' ),
				),
				'h3, .entry-content h3, .entry-content h3 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h3_font_size, 'tablet' ),
				),
				'h4, .entry-content h4, .entry-content h4 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h4_font_size, 'tablet' ),
				),
				'h5, .entry-content h5, .entry-content h5 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h5_font_size, 'tablet' ),
				),
				'h6, .entry-content h6, .entry-content h6 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h6_font_size, 'tablet' ),
				),
				'.leap-single-post .entry-title, .page-title' => array(
					'font-size' => wiz_responsive_slider( $single_post_title_font_size, 'tablet'),
				),
				'#sitehead .site-logo-img .custom-logo-link img' => array(
					'max-width' => wiz_responsive_slider($header_logo_width,'tablet'),
				),
				'.wiz-logo-svg'                       => array(
					'width' => wiz_responsive_slider($header_logo_width,'tablet'),
				),
				'.wiz-footer' => array(
					'font-size' => wiz_responsive_slider( $footer_font_size , 'tablet' ),
				),


                /**
                * Content Spacing Tablet
                */
            '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single ' => array(
                'padding-top'    => wiz_responsive_spacing( $container_inner_spacing, 'top', 'tablet' ),
                'padding-bottom' => wiz_responsive_spacing( $container_inner_spacing, 'bottom', 'tablet' ),
                'padding-right' => wiz_responsive_spacing( $container_inner_spacing, 'right', 'tablet' ),
                'padding-left'  => wiz_responsive_spacing( $container_inner_spacing, 'left', 'tablet' ),
            ),
            
            '.wiz-footer .leap-container ' => array(
                'padding-top'    => wiz_responsive_spacing( $space_footer, 'top', 'tablet' ),
                'padding-bottom' => wiz_responsive_spacing( $space_footer, 'bottom', 'tablet' ),
                'padding-right' => wiz_responsive_spacing( $space_footer, 'right', 'tablet' ),
                'padding-left'  => wiz_responsive_spacing( $space_footer, 'left', 'tablet' ),
			),
			// Footer Widget
			'.wiz-footer .wiz-footer-widget' => array(
				'padding-top'    => wiz_responsive_spacing( $wiz_footer_space_widget,'top'   ,'tablet' ),
				'padding-bottom' => wiz_responsive_spacing( $wiz_footer_space_widget,'bottom','tablet' ),
				'padding-right'  => wiz_responsive_spacing( $wiz_footer_space_widget,'right' ,'tablet' ),
				'padding-left'   => wiz_responsive_spacing( $wiz_footer_space_widget,'left'  ,'tablet' ),
			),
			//Main Footer
			'.wiz-footer button, .wiz-footer .leap-button, .wiz-footer .button, .wiz-footer input#submit, .wiz-footer input[type=button], .wiz-footer input[type=submit], .wiz-footerinput[type=reset]'  => array(
					'border-radius'    => wiz_responsive_slider( $footer_button_border_radius, 'tablet' ),
			),
			);

			/* Parse CSS from array()*/
			$parse_css .= wiz_parse_css( array_merge( $tablet_typo, $tablet_typography ), '', '768' );

			$mobile_typo = array();
			if ( isset( $body_font_size['mobile'] ) && '' != $body_font_size['mobile'] ) {
				$mobile_typo = array(
					'.comment-reply-title' => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size['mobile'] * 1.66666, 'px', 'mobile' ),
					),
					// Single Post Meta.
					'.leap-comment-meta'    => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size['mobile'] * 0.8571428571, 'px', 'mobile' ),
					),
					// Widget Title.
					'.widget-title'        => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size['mobile'] * 1.428571429, 'px', 'mobile' ),
					),
				);
			}

			/* Mobile Typography */
			$mobile_typography = array(
				'body, button, input, select, textarea' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'mobile' ),
				),
				'.leap-comment-list #cancel-comment-reply-link' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'mobile' ),
				),
				'#secondary, #secondary button, #secondary input, #secondary select, #secondary textarea' => array(
					'font-size' => wiz_responsive_slider( $body_font_size, 'mobile' ),
				),
				'.site-title'                           => array(
					'font-size' => wiz_responsive_slider( $site_title_font_size, 'mobile' ),
				),
				'.leap-footer-copyright'                       => array(
					'font-size' => wiz_responsive_slider( $footer_sml_font_size, 'mobile' ),
				),
				'#sitehead.site-header .leap-site-identity'  => array(
					'padding-top'    => wiz_responsive_spacing( $site_identity_spacing, 'top', 'mobile' ),
					'padding-right'  => wiz_responsive_spacing( $site_identity_spacing, 'right', 'mobile' ),
					'padding-bottom' => wiz_responsive_spacing( $site_identity_spacing, 'bottom', 'mobile' ),
					'padding-left'   => wiz_responsive_spacing( $site_identity_spacing, 'left', 'mobile' ),
				),
				'.content-area .read-more a' => array(
					'border-radius'    => wiz_responsive_slider( $readmore_border_radius, 'mobile' ),
					'border-width' => wiz_responsive_slider( $readmore_border_size , 'mobile'),
				),
				'.site-header .leap-sitehead-custom-menu-items' => array(
                'padding-top'    => wiz_responsive_spacing( $last_menu_items_spacing, 'top', 'mobile' ),
                'padding-bottom' => wiz_responsive_spacing( $last_menu_items_spacing, 'bottom', 'mobile' ),
                'padding-right' => wiz_responsive_spacing( $last_menu_items_spacing, 'right', 'mobile' ),
                'padding-left'  => wiz_responsive_spacing( $last_menu_items_spacing, 'left', 'mobile' ),
            	),
				//Widget Spacing
				'.sidebar-main ' => array(
					'padding-top'    => wiz_responsive_spacing( $space_widget, 'top', 'mobile' ),
					'padding-bottom' => wiz_responsive_spacing( $space_widget, 'bottom', 'mobile' ),
					'padding-right' => wiz_responsive_spacing( $space_widget, 'right', 'mobile' ),
					'padding-left'  => wiz_responsive_spacing( $space_widget, 'left', 'mobile' ),
				),
				//Header
				'.main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon' => array(
					'border-bottom-width' => wiz_responsive_slider( $header_separator, 'mobile' ),
				),
				// Button Typography.
				'.menu-toggle, button, .leap-button, input[type=button], input[type=button]:focus, input[type=button]:hover, input[type=reset], input[type=reset]:focus, input[type=reset]:hover, input[type=submit], input[type=submit]:focus, input[type=submit]:hover' => array(
					'border-radius'    => wiz_responsive_slider( $btn_border_radius, 'mobile' ),
				),
				//post readmore spacing
				'.content-area p.read-more' => array(
					'padding-top'    => wiz_responsive_spacing( $readmore_padding, 'top', 'mobile' ),
					'padding-bottom' => wiz_responsive_spacing( $readmore_padding, 'bottom', 'mobile' ),
					'padding-right' => wiz_responsive_spacing( $readmore_padding, 'right', 'mobile' ),
					'padding-left'  => wiz_responsive_spacing( $readmore_padding, 'left', 'mobile' ),
				),

				//Main Footer
				'.wiz-footer button, .wiz-footer .leap-button, .wiz-footer .button, .wiz-footer input#submit, .wiz-footer input[type=button], .wiz-footer input[type=submit], .wiz-footerinput[type=reset]'  => array(
						'border-radius'    => wiz_responsive_slider( $footer_button_border_radius, 'mobile' ),
				),

				'.leap-archive-description .leap-archive-title' => array(
					'font-size' => wiz_responsive_slider( $archive_summary_title_font_size, 'mobile' ),
				),
				'.site-header .site-description'        => array(
					'font-size' => wiz_responsive_slider( $site_tagline_font_size, 'mobile' ),
				),
				'body:not(.leap-single-post) .entry-title'                          => array(
					'font-size' => wiz_responsive_slider( $archive_post_title_font_size, 'mobile' ),
				),
				'body:not(.leap-single-post) .entry-meta'                            => array(
					'font-size' => wiz_responsive_slider( $archive_post_meta_font_size, 'mobile' ),
				),
				'h1, .entry-content h1, .entry-content h1 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h1_font_size, 'mobile', 30 ),
				),
				'h2, .entry-content h2, .entry-content h2 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h2_font_size, 'mobile' ),
				),
				'h3, .entry-content h3, .entry-content h3 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h3_font_size, 'mobile', 20 ),
				),
				'h4, .entry-content h4, .entry-content h4 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h4_font_size, 'mobile' ),
				),
				'h5, .entry-content h5, .entry-content h5 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h5_font_size, 'mobile' ),
				),
				'h6, .entry-content h6, .entry-content h6 a' => array(
					'font-size' => wiz_responsive_slider( $heading_h6_font_size, 'mobile' ),
				),
				'.leap-single-post .entry-title, .page-title' => array(
					'font-size' => wiz_responsive_slider( $single_post_title_font_size, 'mobile'),
				),
				'.leap-header-break-point .site-branding img, .leap-header-break-point #sitehead .site-logo-img .custom-logo-link img' => array(
					'max-width' => wiz_responsive_slider($header_logo_width,'mobile'),
				),
				'.wiz-logo-svg'                       => array(
					'width' => wiz_responsive_slider($header_logo_width,'mobile'),
				),

				//Widget Font
				'.sidebar-main .widget-title '   => array(
					'font-size'      => wiz_responsive_slider( $widget_title_font_size, 'mobile' ),
				),

				'.wiz-footer' => array(
					'font-size' => wiz_responsive_slider( $footer_font_size , 'mobile' ),
				),
				'.main-header-menu a'  => array(
					'font-size'	 	=> wiz_responsive_slider( $menu_font_size , 'tablet' ),		
				),
           /**
			* Content Spacing Mobile
			*/
            '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single ' => array(
                'padding-top'    => wiz_responsive_spacing( $container_inner_spacing, 'top', 'mobile' ),
                'padding-bottom' => wiz_responsive_spacing( $container_inner_spacing, 'bottom', 'mobile' ),
                'padding-right' => wiz_responsive_spacing( $container_inner_spacing, 'right', 'mobile' ),
                'padding-left'  => wiz_responsive_spacing( $container_inner_spacing, 'left', 'mobile' ),
			),
			//header spacing
			'.main-header-bar' => array(
				'padding-top'    => wiz_responsive_spacing( $space_header, 'top', 'mobile' ),
				'padding-bottom' => wiz_responsive_spacing( $space_header, 'bottom', 'mobile' ),
				'padding-right' => wiz_responsive_spacing( $space_header, 'right', 'mobile' ),
				'padding-left'  => wiz_responsive_spacing( $space_header, 'left', 'mobile' ),
			),
			//Sidebar Spacing
			'div.sidebar-main' => array(
				'padding-top'    => wiz_responsive_spacing( $sidebar_padding, 'top', 'mobile' ),
				'padding-bottom' => wiz_responsive_spacing( $sidebar_padding, 'bottom', 'mobile' ),
				'padding-right' => wiz_responsive_spacing( $sidebar_padding, 'right', 'mobile' ),
				'padding-left'  => wiz_responsive_spacing( $sidebar_padding, 'left', 'mobile' ),
			),
                
            '.wiz-footer .leap-container ' => array(
                'padding-top'    => wiz_responsive_spacing( $space_footer, 'top', 'mobile' ),
                'padding-bottom' => wiz_responsive_spacing( $space_footer, 'bottom', 'mobile' ),
                'padding-right' => wiz_responsive_spacing( $space_footer, 'right', 'mobile' ),
                'padding-left'  => wiz_responsive_spacing( $space_footer, 'left', 'mobile' ),
			),

			// Footer Widget
			'.wiz-footer .wiz-footer-widget' => array(
				'padding-top'    => wiz_responsive_spacing( $wiz_footer_space_widget,'top'   ,'tablet' ),
				'padding-bottom' => wiz_responsive_spacing( $wiz_footer_space_widget,'bottom','tablet' ),
				'padding-right'  => wiz_responsive_spacing( $wiz_footer_space_widget,'right' ,'tablet' ),
				'padding-left'   => wiz_responsive_spacing( $wiz_footer_space_widget,'left'  ,'tablet' ),
			),
			
			'.wiz-footer .widget-title'                           => array(
				'font-size' => wiz_responsive_slider( $wiz_footer_widget_title_font_size , 'mobile' ),
			),
			);
					
			/* Parse CSS from array()*/
			$parse_css .= wiz_parse_css( array_merge( $mobile_typo, $mobile_typography ), '', '544' );

			/*
			 *  Responsive Font Size for Tablet & Mobile to the root HTML element
			 */

			// Tablet Font Size for HTML tag.
			if ( '' == $body_font_size['tablet'] ) {
				$html_tablet_typography = array(
					'html' => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 5.7, '%' ),
					),
				);
				$parse_css             .= wiz_parse_css( $html_tablet_typography, '', '768' );
			}
			// Mobile Font Size for HTML tag.
			if ( '' == $body_font_size['mobile'] ) {
				$html_mobile_typography = array(
					'html' => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 5.7, '%' ),
					),
				);
			} else {
				$html_mobile_typography = array(
					'html' => array(
						'font-size' => wiz_get_font_css_value( (int) $body_font_size_desktop * 6.25, '%' ),
					),
				);
			}
			/* Parse CSS from array()*/
			$parse_css .= wiz_parse_css( $html_mobile_typography, '', '544' );

			/* Site width Responsive */
			$site_width = array(
				'.leap-container' => array(
					'max-width' => wiz_get_css_value( $site_content_width + 40, 'px' ),
				),
			);

			/* Parse CSS from array()*/
			$parse_css .= wiz_parse_css( $site_width, '769' );

			/**
			 * Wiz Fonts
			 */
			if ( apply_filters( 'wiz_enable_default_fonts', true ) ) {
				$wiz_fonts          = '@font-face {';
					$wiz_fonts     .= 'font-family: "Wiz-font";';
					$wiz_fonts     .= 'src: url( ' . WIZ_THEME_URI . 'assets/fonts/wiz-font.woff) format("woff"),';
						$wiz_fonts .= 'url( ' . WIZ_THEME_URI . 'assets/fonts/wiz-font.ttf) format("truetype"),';
						$wiz_fonts .= 'url( ' . WIZ_THEME_URI . 'assets/fonts/wiz-font.svg#wiz) format("svg");';
					$wiz_fonts     .= 'font-weight: normal;';
					$wiz_fonts     .= 'font-style: normal;';
				$wiz_fonts         .= '}';
				$parse_css           .= $wiz_fonts;
			}

			/* Blog */
			if ( 'custom' === $blog_width ) :

				/* Site width Responsive */
				$blog_css   = array(
					'.blog .site-content > .leap-container, .archive .site-content > .leap-container, .search .site-content > .leap-container' => array(
						'max-width' => wiz_get_css_value( $blog_max_width, 'px' ),
					),
				);
				$parse_css .= wiz_parse_css( $blog_css, '769' );
			endif;

			/* Single Blog */
			if ( 'custom' === $single_post_max ) :

				/* Site width Responsive */
				$single_blog_css = array(
					'.single-post .site-content > .leap-container' => array(
						'max-width' => wiz_get_css_value( $single_post_max_width, 'px' ),
					),
				);
				$parse_css      .= wiz_parse_css( $single_blog_css, '769' );
			endif;

			/* Small Footer CSS */
			if ( 'disabled' != $copyright_footer_layout ) :
				$sml_footer_css = array(
					'.leap-footer-copyright' => array(
						'border-top-style' => 'solid',
						'border-top-width' => wiz_get_css_value( $copyright_footer_divider, 'px' ),
						'border-top-color' => esc_attr( $copyright_footer_divider_color ),
					),
				);
				$parse_css     .= wiz_parse_css( $sml_footer_css );

				if ( 'copyright-footer-layout-2' != $copyright_footer_layout ) {
					$sml_footer_css = array(
						'.leap-footer-copyright-wrap' => array(
							'text-align' => 'center',
						),
					);
					$parse_css     .= wiz_parse_css( $sml_footer_css );
				}
			endif;

			/* 404 Page */
			$parse_css .= wiz_parse_css(
				array(
					'.leap-404-layout .leap-404-text' => array(
						'font-size' => wiz_get_font_css_value( 100 ),
					),
				), '', '920'
			);

			$dynamic_css = $parse_css;
			$custom_css  = wiz_get_option( 'custom-css' );

			if ( '' != $custom_css ) {
				$dynamic_css .= $custom_css;
			}

			// trim white space for faster page loading.
			$dynamic_css = Wiz_Enqueue_Scripts::trim_css( $dynamic_css );

			return $dynamic_css;
		}

		/**
		 * Return post meta CSS
		 *
		 * @param  boolean $return_css Return the CSS.
		 * @return mixed              Return on print the CSS.
		 */
		static public function return_meta_output( $return_css = false ) {

			/**
			 * - Page Layout
			 *
			 *   - Sidebar Positions CSS
			 */
			$secondary_width = wiz_get_option( 'site-sidebar-width' );
			$primary_width   = absint( 100 - $secondary_width );
			$meta_style      = '';

			// Header Separator.
			$header_separator_color = wiz_get_option( 'header-main-sep-color' );

			$meta_style = array(
				'.leap-header-break-point .site-header' => array(
					'border-bottom-color' => esc_attr( $header_separator_color ),
				),
			);
		
			//Widget Title Border
			$widget_title_border_size       = wiz_get_option( 'widget-title-border-size' );
			$widget_title_border_color      = wiz_get_option( 'widget-title-border-color' );
			$meta_style = array(
				'.sidebar-main .widget-title ' => array(
					'border-bottom-style' => 'solid',
					'border-bottom-width' => wiz_get_css_value( $widget_title_border_size , 'px' ),
					'border-bottom-color' => esc_attr( $widget_title_border_color ),
				),
			);


			$parse_css = wiz_parse_css( $meta_style );

			$meta_style = array(
				'.main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon' => array(
					'border-bottom-color' => esc_attr( $header_separator_color ),
				),
			);

			$parse_css .= wiz_parse_css( $meta_style, '769' );

			if ( 'no-sidebar' !== wiz_layout() ) :

				$meta_style = array(
					'#primary'   => array(
						'width' => wiz_get_css_value( $primary_width, '%' ),
					),
					'#secondary' => array(
						'width' => wiz_get_css_value( $secondary_width, '%' ),
					),
				);

				$parse_css .= wiz_parse_css( $meta_style, '769' );

			endif;

			$dynamic_css = $parse_css;
			if ( false != $return_css ) {
				return $dynamic_css;
			}

			wp_add_inline_style( 'wiz-theme-css', $dynamic_css );
		}
	}
}
