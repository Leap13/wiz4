<?php
/**
 * Single Blog Helper Functions
 *
 * @package Wiz
 */

/**
 * Adds custom classes to the array of body classes.
 */
if ( ! function_exists( 'wiz_single_body_class' ) ) {

	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 * @return array
	 */
	function wiz_single_body_class( $classes ) {

		// Blog layout.
		if ( is_single() ) {
			$classes[] = 'leap-blog-single-style-1';

			if ( 'post' != get_post_type() ) {
				$classes[] = 'leap-custom-post-type';
			}
		}

		if ( is_singular() ) {
			$classes[] = 'leap-single-post';
		}

		return $classes;
	}
}

add_filter( 'body_class', 'wiz_single_body_class' );

/**
 * Adds custom classes to the array of body classes.
 */
if ( ! function_exists( 'wiz_single_post_class' ) ) {

	/**
	 * Adds custom classes to the array of body classes.
	 *
	 * @param array $classes Classes for the body element.
	 * @return array
	 */
	function wiz_single_post_class( $classes ) {

		// Blog layout.
		if ( is_singular() ) {
			$classes[] = 'leap-article-single';

			// Remove hentry from page.
			if ( 'page' == get_post_type() ) {
				$classes = array_diff( $classes, array( 'hentry' ) );
			}
		}

		return $classes;
	}
}

add_filter( 'post_class', 'wiz_single_post_class' );

/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
if ( ! function_exists( 'wiz_single_get_post_meta' ) ) {

	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 *
	 * @param boolean $echo   Output print or return.
	 * @return string|void
	 */
	function wiz_single_get_post_meta( $echo = true ) {

		$enable_meta = apply_filters( 'wiz_single_post_meta_enabled', '__return_true' );
		$post_meta   = wiz_get_option( 'blog-single-meta' );

		$output = '';
		if ( is_array( $post_meta ) && 'post' == get_post_type() && $enable_meta ) {

			$output_str = wiz_get_post_meta( $post_meta );
			if ( ! empty( $output_str ) ) {
				$output = apply_filters( 'wiz_single_post_meta', '<div class="entry-meta">' . $output_str . '</div>', $output_str ); // WPCS: XSS OK.
			}
		}
		if ( $echo ) {
			echo wp_kses_post( $output );;
		} else {
			return $output;
		}
	}
}

/**
 * Template for comments and pingbacks.
 */
if ( ! function_exists( 'wiz_comment' ) ) {

	/**
	 * Template for comments and pingbacks.
	 *
	 * Used as a callback by wp_list_comments() for displaying the comments.
	 *
	 * @param  string $comment Comment.
	 * @param  array  $args    Comment arguments.
	 * @param  number $depth   Depth.
	 * @return mixed          Comment markup.
	 */
	function wiz_comment( $comment, $args, $depth ) {

		switch ( $comment->comment_type ) {

			case 'pingback':
			case 'trackback':
				// Display trackbacks differently than normal comments.
			?>
				<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					<p><?php esc_html_e( 'Pingback:', 'wiz' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'wiz' ), '<span class="edit-link">', '</span>' ); ?></p>
				</li>
				<?php
				break;

			default:
				// Proceed with normal comments.
				global $post;
				?>
				<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">

					<article id="comment-<?php comment_ID(); ?>" class="leap-comment">
						<div class='leap-comment-avatar-wrap'><?php echo get_avatar( $comment, 50 ); ?></div><!-- Remove 1px Space
						--><div class="leap-comment-data-wrap">
							<div class="leap-comment-meta-wrap">
								<header class="leap-comment-meta leap-row leap-comment-author vcard capitalize">

									<?php

									printf(
										'<div class="leap-comment-cite-wrap leap-col-lg-12"><cite><b class="fn">%1$s</b> %2$s</cite></div>',
										get_comment_author_link(),
										// If current post author is also comment author, make it known visually.
										( $comment->user_id === $post->post_author ) ? '<span class="leap-highlight-text leap-cmt-post-author"></span>' : ''
									);

									printf(
										'<div class="leap-comment-time leap-col-lg-12"><span  class="timendate"><a href="%1$s"><time datetime="%2$s">%3$s</time></a></span></div>',
										esc_url( get_comment_link( $comment->comment_ID ) ),
										esc_html(get_comment_time( 'c' )),
										/* translators: 1: date, 2: time */
										sprintf( esc_html__( '%1$s at %2$s', 'wiz' ), esc_html(get_comment_date() ), esc_html(get_comment_time()) )
									);

									?>

								</header> <!-- .leap-comment-meta -->
							</div>
							<section class="leap-comment-content comment">
								<?php comment_text(); ?>
								<div class="leap-comment-edit-reply-wrap">
									<?php edit_comment_link( wiz_theme_strings( 'string-comment-edit-link', false ), '<span class="leap-edit-link">', '</span>' ); ?>
									<?php
									comment_reply_link(
										array_merge(
											$args, array(
												'reply_text' => wiz_theme_strings( 'string-comment-reply-link', false ),
												'add_below' => 'comment',
												'depth'  => $depth,
												'max_depth' => $args['max_depth'],
												'before' => '<span class="leap-reply-link">',
												'after'  => '</span>',
											)
										)
									);
									?>
								</div>
								<?php if ( '0' == $comment->comment_approved ) : ?>
									<p class="leap-highlight-text comment-awaiting-moderation"><?php echo esc_html( wiz_theme_strings( 'string-comment-awaiting-moderation', false ) ); ?></p>
								<?php endif; ?>
							</section> <!-- .leap-comment-content -->
						</div>
					</article><!-- #comment-## -->
				<!-- </li> -->
				<?php
				break;
		}
	}
}

/**
 * Get Post Navigation
 */
if ( ! function_exists( 'wiz_single_post_navigation_markup' ) ) {

	/**
	 * Get Post Navigation
	 *
	 * Checks post navigation, if exists return as button.
	 *
	 * @return mixed Post Navigation Buttons
	 */
	function wiz_single_post_navigation_markup() {

		$single_post_navigation_enabled = apply_filters( 'wiz_single_post_navigation_enabled', true );

		if ( is_single() && $single_post_navigation_enabled ) {

			$post_obj = get_post_type_object( get_post_type() );

			$next_text = sprintf(
				wiz_theme_strings( 'string-single-navigation-next', false ),
				$post_obj->labels->singular_name
			);

			$prev_text = sprintf(
				wiz_theme_strings( 'string-single-navigation-previous', false ),
				$post_obj->labels->singular_name
			);
			/**
			 * Filter the post pagination markup
			 */
			the_post_navigation(
				apply_filters(
					'wiz_single_post_navigation', array(
						'next_text' => $next_text,
						'prev_text' => $prev_text,
					)
				)
			);

		}
	}
}

add_action( 'wiz_entry_after', 'wiz_single_post_navigation_markup' );
