<?php
/**
 * Wiz Theme Customizer Controls.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

$control_dir = WIZ_THEME_DIR . 'inc/customizer/custom-controls';

require $control_dir . '/sortable/class-wiz-control-sortable.php';
require $control_dir . '/radio-image/class-wiz-control-radio-image.php';
require $control_dir . '/slider/class-wiz-control-slider.php';
require $control_dir . '/responsive-slider/class-wiz-control-responsive-slider.php';
require $control_dir . '/responsive/class-wiz-control-responsive.php';
require $control_dir . '/typography/class-wiz-control-typography.php';
require $control_dir . '/responsive-spacing/class-wiz-control-responsive-spacing.php';
require $control_dir . '/title/class-wiz-control-title.php';
require $control_dir . '/color/class-wiz-control-color.php';
require $control_dir . '/background/class-wiz-control-background.php';
require $control_dir . '/icon-select/class-wiz-control-icon-select.php';
