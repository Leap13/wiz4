<?php
/**
* Favicon Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-site-logo-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Logo Settings', 'wiz' ),
            'section'  => 'title_tagline',
            'priority' => 0,
            'settings' => array(),
        )
    )
);

/**
* Option: Retina logo selector
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[leap-header-retina-logo]', array(
        'default'           => wiz_get_option( 'leap-header-retina-logo' ),
        'type'              => 'option',
        'sanitize_callback' => 'esc_url_raw',
    )
);

$wp_customize->add_control(
    new WP_Customize_Image_Control(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-header-retina-logo]', array(
            'section'        => 'title_tagline',
            'priority'       => 5,
            'label'          => __( 'Retina Logo', 'wiz' ),
        )
    )
);

/**
* Option: Display Title
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[display-site-title]', array(
        'default'           => wiz_get_option( 'display-site-title' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[display-site-title]', array(
        'type'     => 'checkbox',
        'section'  => 'title_tagline',
        'label'    => __( 'Display Site Title', 'wiz' ),
        'priority' => 10,
    )
);

/**
* Option: Display Tagline
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[display-site-tagline]', array(
        'default'           => wiz_get_option( 'display-site-tagline' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
        'priority'          => 15,
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[display-site-tagline]', array(
        'type'    => 'checkbox',
        'section' => 'title_tagline',
        'label'   => __( 'Display Site Tagline', 'wiz' ),
    )
);

/**
* Option: Disable Menu
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[logo-title-inline]', array(
        'default'           => wiz_get_option( 'logo-title-inline' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[logo-title-inline]', array(
        'type'     => 'checkbox',
        'section'  => 'title_tagline',
        'label'    => __( 'Inline Logo & Site Title', 'wiz' ),
        'priority' => 20,
    )
);

/**
* Option - Site Identity Padding
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[site-identity-spacing]', array(
        'default'           => wiz_get_option( 'site-identity-spacing' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[site-identity-spacing]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'title_tagline',
            'priority'       => 30,
            'label'          => __( 'Site Identity Space', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Site Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[site-title-font-size]', array(
        'default'           => wiz_get_option( 'site-title-font-size' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[site-title-font-size]', array(
            'type'        => 'leap-responsive-slider',
            'section'     => 'title_tagline',
            'priority'    => 35,
            'label'       => __( 'Site Title Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Site Title Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[site-title-color]', array(
        'default'           => wiz_get_option( 'site-title-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[site-title-color]', array(
            'label'   => __( 'Site Title Color', 'wiz' ),
            'priority'       => 40,
            'section' => 'title_tagline',
        )
    )
);

/**
* Option: Site Title Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[site-title-h-color]', array(
        'default'           => wiz_get_option( 'site-title-h-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[site-title-h-color]', array(
            'label'   => __( 'Site Title Hover Color', 'wiz' ),
            'priority'       => 45,
            'section' => 'title_tagline',
        )
    )
);

/**
* Option: Site Tagline Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-site-tagline]', array(
        'default'           => wiz_get_option( 'font-size-site-tagline' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-site-tagline]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'title_tagline',
            'priority'       => 50,
            'label'          => __( 'Tagline Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-site-icon-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Site Icon', 'wiz' ),
            'section'  => 'title_tagline',
            'priority' => 55,
            'settings' => array(),
        )
    )
);

/**
* Option - Site Logo Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[leap-header-responsive-logo-width]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-header-responsive-logo-width]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'title_tagline',
            'priority'       => 1,
            'label'          => __( 'Logo Width', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 50,
                    'step' => 1,
                    'max' =>600,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
                '%' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 100,
                ),
			),
			'description'  => __('This option will not increase your uploaded logo width beyond its original size.', 'wiz'),
        )
    )
);

