<?php
/**
* Buttons for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Button Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[button-color]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Text Color', 'wiz' ),
            'priority'    => 5,
        )
    )
);

/**
* Option: Button Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[button-h-color]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Text Hover Color', 'wiz' ),
            'priority'    => 10,
        )
    )
);

/**
* Option: Button Background Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-bg-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[button-bg-color]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Background Color', 'wiz' ),
            'priority'    => 15,
        )
    )
);

/**
* Option: Button Background Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-bg-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[button-bg-h-color]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Background Hover Color', 'wiz' ),
            'priority'    => 20,
        )
    )
);

/**
* Option: Button Border Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[btn-border-size]', array(
        'default'           => wiz_get_option( 'btn-border-size' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[btn-border-size]', array(
				'type'        => 'leap-slider',
				'section'     => 'section-buttons',
				'priority'    => 25,
				'label'       => __( 'Button Border Size', 'wiz' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 0,
					'step' => 1,
					'max'  => 100,
				),
			)
		)
	);
/**
* Option: Button Background Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[btn-border-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[btn-border-color]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Border Color', 'wiz' ),
            'priority'    => 30,
        )
    )
);

/**
* Option: Button Background Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[btn-border-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[btn-border-h-colo]', array(
            'section' => 'section-buttons',
            'label'   => __( 'Button Border Hover color', 'wiz' ),
            'priority'    => 35,
        )
    )
);

/**
* Option: Button Radius
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-radius]', array(
        'default'           => wiz_get_option('button-radius'),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[button-radius]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-buttons',
            'priority'       => 40,
            'label'          => __( 'Button Radius', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>100,
                ),
                'em' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 10,
                ),
                '%' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 100,
                ),
            ),
        )
    )
);

/**
* Option: Vertical Padding
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-v-padding]', array(
        'default'           => wiz_get_option( 'button-v-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[button-v-padding]', array(
        'section'     => 'section-buttons',
        'label'       => __( 'Vertical Padding', 'wiz' ),
        'type'        => 'number',
        'priority'    => 45,
        'input_attrs' => array(
            'min'  => 1,
            'step' => 1,
            'max'  => 200,
        ),
    )
);

/**
* Option: Horizontal Padding
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[button-h-padding]', array(
        'default'           => wiz_get_option( 'button-h-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[button-h-padding]', array(
        'section'     => 'section-buttons',
        'label'       => __( 'Horizontal Padding', 'wiz' ),
        'type'        => 'number',
        'priority'    => 50,
        'input_attrs' => array(
            'min'  => 1,
            'step' => 1,
            'max'  => 200,
        ),
    )
);
