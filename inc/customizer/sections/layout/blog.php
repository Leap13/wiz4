<?php
/**
* Blog Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
/**
* Option: Blog Content Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-width]', array(
        'default'           => wiz_get_option( 'blog-width' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[blog-width]', array(
        'type'     => 'select',
        'section'  => 'section-blog',
        'priority' => 5,
        'label'    => __( 'Blog Content Width', 'wiz' ),
        'choices'  => array(
            'default' => __( 'Default', 'wiz' ),
            'custom'  => __( 'Custom', 'wiz' ),
        ),
    )
);

/**
* Option: Enter Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-max-width]', array(
        'default'           => 1200,
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-max-width]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-blog',
            'priority'    => 10,
            'label'       => __( 'Enter Width', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 768,
                'step' => 1,
                'max'  => 1920,
            ),
        )
    )
);

/**
* Option: Display Post Structure
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-post-structure]', array(
        'default'           => wiz_get_option( 'blog-post-structure' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Sortable(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-post-structure]', array(
            'type'     => 'leap-sortable',
            'section'  => 'section-blog',
            'priority' => 15,
            'label'    => __( 'Blog Post Structure', 'wiz' ),
            'choices'  => array(
                'image'      => __( 'Featured Image', 'wiz' ),
                'title-meta' => __( 'Title & Blog Meta', 'wiz' ),
                'content-readmore' => __( 'Content & Readmore', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Display Post Meta
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-meta]', array(
        'default'           => wiz_get_option( 'blog-meta' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Sortable(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-meta]', array(
            'type'     => 'leap-sortable',
            'section'  => 'section-blog',
            'priority' => 20,
            'label'    => __( 'Blog Meta', 'wiz' ),
            'choices'  => array(
                'comments' => __( 'Comments', 'wiz' ),
                'category' => __( 'Category', 'wiz' ),
                'author'   => __( 'Author', 'wiz' ),
                'date'     => __( 'Publish Date', 'wiz' ),
                'tag'      => __( 'Tag', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Blog Post Content
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-post-content]', array(
        'default'           => wiz_get_option( 'blog-post-content' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[blog-post-content]', array(
        'section'  => 'section-blog',
        'label'    => __( 'Blog Post Content', 'wiz' ),
        'type'     => 'select',
        'priority' => 25,
        'choices'  => array(
            'full-content' => __( 'Full Content', 'wiz' ),
            'excerpt'      => __( 'Excerpt', 'wiz' ),
        ),
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-archive-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Archive Summary Box Title', 'wiz' ),
            'section'  => 'section-blog',
            'priority' => 30,
            'settings' => array(),
        )
    )
);

/**
* Option: Archive Summary Box Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-archive-summary-title]', array(
        'default'           => wiz_get_option( 'font-size-archive-summary-title' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-archive-summary-title]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-blog',
            'priority'       => 35,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-blog-post-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Blog Post', 'wiz' ),
            'section'  => 'section-blog',
            'priority' => 44,
            'settings' => array(),
        )
    )
);
/**
* Option:Post Meta Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[post-meta-color]', array(
        'default'           => wiz_get_option( 'listing-post-meta-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[post-meta-color]', array(
            'label'   => __( 'Post Meta Color', 'wiz' ),
            'priority'       => 44,
            'section' => 'section-blog',
        )
    )
);
/**
* Option: Blog - Post Meta Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-page-meta]', array(
        'default'           => wiz_get_option( 'font-size-page-meta' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-page-meta]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-blog',
            'priority'       => 44,
            'label'          => __( 'Meta Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);
/**
* Option: Blog - Post Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-page-title]', array(
        'default'           => wiz_get_option( 'font-size-page-title' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-page-title]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-blog',
            'priority'       => 45,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option:Post Title Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[listing-post-title-color]', array(
        'default'           => wiz_get_option( 'listing-post-title-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[listing-post-title-color]', array(
            'label'   => __( 'Listing Post Title Color', 'wiz' ),
            'priority'       => 50,
            'section' => 'section-blog',
        )
    )
);

/**
* Option:Post Read More Text Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-text-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-text-color]', array(
            'label'   => __( 'Read More Color', 'wiz' ),
            'priority'       => 55,
            'section' => 'section-blog',
        )
    )
);

/**
* Option:Post Read More Text Color Hover
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-text-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-text-h-color]', array(
            'label'   => __( 'Read More Hover Color', 'wiz' ),
            'priority'       => 60,
            'section' => 'section-blog',
        )
    )
);

/**
* Option - Read More Spacing
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-padding]', array(
        'default'           => wiz_get_option( 'readmore-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-padding]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'section-blog',
            'priority'       => 65,
            'label'          => __( 'Read More Spacing', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Read More Background Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-bg-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-bg-color]', array(
            'section'  => 'section-blog',
            'priority' => 70,
            'label'    => __( 'Read More Background Color', 'wiz' ),
        )
    )
);
/**
* Option: Read More Background Color Hover
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-bg-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-bg-h-color]', array(
            'section'  => 'section-blog',
            'priority' => 75,
            'label'    => __( 'Read More Background Color Hover', 'wiz' ),
        )
    )
);
/**
* Option: Read More Border Radius
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-border-radius]', array(
        'default'           => wiz_get_option( 'readmore-border-radius' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
		new Wiz_Control_Responsive_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[readmore-border-radius]', array(
				'type'           => 'leap-responsive-slider',
				'section'        => 'section-blog',
				'priority'       => 80,
				'label'          => __( 'Read More Border Radius', 'wiz' ),
				'unit_choices'   => array(
					 'px' => array(
						 'min' => 1,
						 'step' => 1,
						 'max' => 100,
					 ),
					 'em' => array(
						 'min' => 0.1,
						 'step' => 0.1,
						 'max' => 10,
                     ),
                     '%' => array(
                        'min' => 1,
                        'step' => 1,
                        'max' => 100,
                    ),
                 ),
			)
		)
	);
/**
* Option: Read More Border Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-border-size]', array(
        'default'           => wiz_get_option( 'readmore-border-size' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
		new Wiz_Control_Responsive_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[readmore-border-size]', array(
				'type'           => 'leap-responsive-slider',
				'section'        => 'section-blog',
				'priority'       => 85,
				'label'          => __( 'Read More Border Size', 'wiz' ),
				'unit_choices'   => array(
					 'px' => array(
						 'min' => 1,
						 'step' => 1,
						 'max' => 15,
					 ),
                 ),
			)
		)
	);
/**
* Option: Read More Border Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-border-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-border-color]', array(
            'section'  => 'section-blog',
            'priority' => 90,
            'label'    => __( 'Read More Border Color', 'wiz' ),
        )
    )
);

/**
* Option: Read More Border Color Hover
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[readmore-border-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[readmore-border-h-color]', array(
            'section'  => 'section-blog',
            'priority' => 95,
            'label'    => __( 'Read More Border Hover Color', 'wiz' ),
        )
    )
);
