<?php
/**
 * Main Menu Options for Wiz Theme.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$header_rt_sections = array(
	'none'      => __( 'None', 'wiz' ),
	'search'    => __( 'Search', 'wiz' ),
	'text-html' => __( 'Text / HTML', 'wiz' ),
	'widget'    => __( 'Widget', 'wiz' ),
);
	/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-menu-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Main Menu Settings', 'wiz' ),
				'section'  => 'section-menu-header',
				'priority' => 0,
				'settings' => array(),
			)
		)
	);
	/**
	 * Option: Disable Menu
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[disable-primary-nav]', array(
			'default'           => wiz_get_option( 'disable-primary-nav' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[disable-primary-nav]', array(
			'type'     => 'checkbox',
			'section'  => 'section-menu-header',
			'label'    => __( 'Disable Menu', 'wiz' ),
			'priority' => 5,
		)
	);

	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-main-rt-section]', array(
			'default'           => wiz_get_option( 'header-main-rt-section' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
		)
	);
	$wp_customize->add_control(
         new Wiz_Control_Sortable(
            $wp_customize, WIZ_THEME_SETTINGS . '[header-main-rt-section]', array(
			'type'     => 'leap-sortable',
			'section'  => 'section-menu-header',
			'priority' => 10,
			'label'    => __( 'Last Custom Menu Item', 'wiz' ),
			'choices'  => apply_filters(
				'wiz_header_elements',
				array(
					'search'    => __( 'Search', 'wiz' ),
					'text-html' => __( 'Text / HTML', 'wiz' ),
					'widget'    => __( 'Widget', 'wiz' ),
				),
				'primary-header'
			),
		)
                        )
	);

	/**
	* Option: Menu Font Size
	*/
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-font-size]', array(
			'default'           => wiz_get_option( 'menu-font-size' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-font-size]', array(
				'type'           => 'leap-responsive-slider',
				'section'        => 'section-menu-header',
				'priority'       => 14,
				'label'          => __( 'Font Size', 'wiz' ),
				'unit_choices'   => array(
					'px' => array(
						'min' => 1,
						'step' => 1,
						'max' =>200,
					),
					'em' => array(
						'min' => 0.1,
						'step' => 0.1,
						'max' => 10,
					),
				),
			)
		)
	);
	/**
	 * Option: Display outside menu
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-display-outside-menu]', array(
			'default'           => wiz_get_option( 'header-display-outside-menu' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[header-display-outside-menu]', array(
			'type'     => 'checkbox',
			'section'  => 'section-menu-header',
			'label'    => __( 'Take custom menu item outside', 'wiz' ),
			'priority' => 15,
		)
	);

	/**
	 * Option: Last Menu Item Left Spacing
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[last-menu-item-spacing]', array(
			'default'           => wiz_get_option( 'last-menu-item-spacing' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Responsive_Spacing(
			$wp_customize, WIZ_THEME_SETTINGS . '[last-menu-item-spacing]', array(
				'type'           => 'leap-responsive-spacing',
				'section'        => 'section-menu-header',
				'priority'       => 16,
				'label'          => __( 'Last menu Item spacing', 'wiz' ),
				'linked_choices' => true,
				'unit_choices'   => array( 'px', 'em', '%' ),
				'choices'        => array(
					'top'    => __( 'Top', 'wiz' ),
					'right'  => __( 'Right', 'wiz' ),
					'bottom' => __( 'Bottom', 'wiz' ),
					'left'   => __( 'Left', 'wiz' ),
				),
			)
		)
	);
	/**
     * Option: Page Title Alignment
     */
    $wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[menu-alignment]',array(
					'default'           => wiz_get_option('menu-alignment'),
					'type'              => 'option',
					'sanitize_callback' => array('Wiz_Customizer_Sanitizes','sanitize_choices')
			)
	);
	$wp_customize->add_control(
		new Wiz_Control_Icon_Select(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-alignment]', array(
				'priority'       => 18,
				'section' => 'section-menu-header',
				'label'   => __( 'Menu Alignment', 'wiz' ),
				'choices'  => array(
					'menu-left' => array(
						'icon' => 'dashicons-editor-alignleft'
					),
					'menu-center' => array(
						'icon' => 'dashicons-editor-aligncenter'
					),
					'menu-right' => array(
						'icon' => 'dashicons-editor-alignright'
					),	
				),
			)
		)
	);
	/**
	 * Option: Right Section Text / HTML
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-main-rt-section-html]', array(
			'default'           => wiz_get_option( 'header-main-rt-section-html' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_html' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[header-main-rt-section-html]', array(
			'type'     => 'textarea',
			'section'  => 'section-menu-header',
			'priority' => 20,
			'label'    => __( 'Custom Menu Text / HTML', 'wiz' ),
		)
	);

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			WIZ_THEME_SETTINGS . '[header-main-rt-section-html]', array(
				'selector'            => '.main-header-bar .leap-sitehead-custom-menu-items .leap-custom-html',
				'container_inclusive' => false,
				'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_header_main_rt_section_html' ),
			)
		);
	}

	/**
	 * Option: Search Style
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-style]', array(
			'default'           => 'search-box',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[search-style]', array(
			'type'     => 'select',
			'section'  => 'section-menu-header',
			'priority' => 25,
			'label'    => __( 'Search Style', 'wiz' ),
			'choices'  => array(
				'search-box'    => __( 'Search Box', 'wiz' ),
				'search-icon'   => __( 'Icon', 'wiz' ),
			),
		)
	);	
	/**
	 * Option: Search Box Shadow
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-box-shadow]', array(
			'default'           => false,
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[search-box-shadow]', array(
			'type'     => 'checkbox',
			'section'  => 'section-menu-header',
			'priority' => 30,
			'label'    => __( 'Enable Search Box Shadow', 'wiz' ),
		)
	);	
	/**
   	* Option: Search Button Background Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-btn-bg-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-btn-bg-color]', array(
			'label'   => __( 'Search Button Background Color', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 35,
		  )
		)
	);
	/**
   	* Option: Search Button Hover Background Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-btn-h-bg-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-btn-h-bg-color]', array(
			'label'   => __( 'Search Button Hover', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 40,
		  )
		)
	);
	/**
   	* Option: Search Button Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-btn-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-btn-color]', array(
			'label'   => __( 'Search Button Color', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 45,
		  )
		)
	);
	/**
	 * Option: Search Form Border Size
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-border-size]', array(
			'default'           => 1,
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[search-border-size]', array(
			'type'        => 'number',
			'section'     => 'section-menu-header',
			'priority'    => 50,
			'label'       => __( 'Search Form Border Size', 'wiz' ),
			'input_attrs' => array(
				'min'  => 0,
				'step' => 1,
				'max'  => 15,
			),
		)
	);
	/**
   	* Option: Search Border Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-border-color]', array(
		  'default'           => '#a7a7a7',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-border-color]', array(
			'label'   => __( 'Search Border Color', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 55,
		  )
		)
	);
	/**
    * Option - Search Background
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-input-bg-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-input-bg-color]', array(
			'label'   => __( 'Search Form Background Color', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 60,
		  )
		)
	);
	/**
    * Option - Search Font Color
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[search-input-color]', array(
		  'default'           => '',
		  'type'              => 'option',
		  'transport'         => 'postMessage',
		  'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
		  $wp_customize, WIZ_THEME_SETTINGS . '[search-input-color]', array(
			'label'   => __( 'Search Form Font Color', 'wiz' ),
			'section' => 'section-menu-header',
			'priority' => 65,
		  )
		)
	);

	/**
	 * Option: Menu Background Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-bg-color]', array(
			'default'           => wiz_get_option( 'menu-bg-color' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-bg-color]', array(
        'priority'       => 93,
        'section' => 'section-menu-header',
				'label'   => __( 'Menu Background Color', 'wiz' ),
			)
		)
	);

	/**
	 * Option:Menu Items Typography
	 * Option: Font Family
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-items-font-family]', array(
			'default'           => wiz_get_option( 'menu-items-font-family' ),
			'type'              => 'option',
			'sanitize_callback' => 'sanitize_text_field',
		)
	);

	$wp_customize->add_control(
		new Wiz_Control_Typography(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-items-font-family]', array(
				'type'        => 'leap-font-family',
				'section'     => 'section-menu-header',
				'priority'    => 75,
				'label'       => __( 'Font Family', 'wiz' ),
				'connect'     => WIZ_THEME_SETTINGS . '[menu-items-font-weight]',
			)
		)
	);

	/**
	 * Option: Font Weight
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-items-font-weight]', array(
			'default'           => wiz_get_option( 'menu-items-font-weight' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Typography(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-items-font-weight]', array(
				'type'        => 'leap-font-weight',
				'section'     => 'section-menu-header',
				'priority'    => 80,
				'label'       => __( 'Font Weight', 'wiz' ),
				'connect'     => WIZ_THEME_SETTINGS . '[menu-items-font-family]',
			)
		)
	);

	/**
	 * Option: Menu Items Text Transform
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-items-text-transform]', array(
			'default'           => wiz_get_option( 'menu-items-text-transform' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[menu-items-text-transform]', array(
			'type'     => 'select',
			'section'  => 'section-menu-header',
			'priority' => 85,
			'label'    => __( 'Text Transform', 'wiz' ),
			'choices'  => array(
				''           => __( 'Default', 'wiz' ),
				'none'       => __( 'None', 'wiz' ),
				'capitalize' => __( 'Capitalize', 'wiz' ),
				'uppercase'  => __( 'Uppercase', 'wiz' ),
				'lowercase'  => __( 'Lowercase', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Menu Items Line Height
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-items-line-height]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-items-line-height]', array(
				'type'        => 'leap-slider',
				'section'     => 'section-menu-header',
				'priority'    => 90,
				'label'       => __( 'Line Height', 'wiz' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 1,
					'step' => 0.01,
					'max'  => 5,
				),
			)
		)
	);
		/**
		 * Option:Menu Link Color
		*/
		$wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[menu-link-color]', array(
				'default'           => wiz_get_option( 'menu-link-h-color' ),
				'type'              => 'option',
				'transport'         => 'postMessage',
				'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
			)
		);
		$wp_customize->add_control(
			new Wiz_Control_Color(
				$wp_customize, WIZ_THEME_SETTINGS . '[menu-link-color]', array(
					'label'   => __( 'Menu Link Color', 'wiz' ),
					'priority'       => 95,
					'section' => 'section-menu-header',
				)
			)
		);
		
	  /**
      * Option:Menu Link Hover Color
      */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[menu-link-h-color]', array(
			'default'           => wiz_get_option( 'menu-link-h-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[menu-link-h-color]', array(
				'label'   => __( 'Menu Link Hover Color', 'wiz' ),
				'priority'       => 100,
				'section' => 'section-menu-header',
			)
		)
	);

		/**
		 * Option:Menu Link Bottom Border Color
		*/
		$wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[menu-link-bottom-border-color]', array(
				'default'           => wiz_get_option( 'menu-link-bottom-border-color' ),
				'type'              => 'option',
				'transport'         => 'postMessage',
				'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
			)
		);
		$wp_customize->add_control(
			new Wiz_Control_Color(
				$wp_customize, WIZ_THEME_SETTINGS . '[menu-link-bottom-border-color]', array(
					'label'   => __( 'Menu Link Bottom Border Color', 'wiz' ),
					'priority'       => 105,
					'section' => 'section-menu-header',
				)
			)
		);

		/**
      * Option:Menu Active Link Color
      */
			$wp_customize->add_setting(
				WIZ_THEME_SETTINGS . '[menu-link-active-color]', array(
					'default'           => wiz_get_option( 'menu-link-active-color' ),
					'type'              => 'option',
					'transport'         => 'postMessage',
					'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
				)
			);
			$wp_customize->add_control(
				new Wiz_Control_Color(
					$wp_customize, WIZ_THEME_SETTINGS . '[menu-link-active-color]', array(
						'label'   => __( 'Menu Link Active Color', 'wiz' ),
						'priority'       => 110,
						'section' => 'section-menu-header',
					)
				)
			);
	/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-submenu-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Submenu Settings', 'wiz' ),
				'section'  => 'section-menu-header',
				'priority' => 115,
				'settings' => array(),
			)
		)
	);
	/**
	 * Option: SubMenu Background Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-bg-color]', array(
			'default'           => wiz_get_option( 'submenu-bg-color' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[submenu-bg-color]', array(
        'priority'       => 125,
        'section' => 'section-menu-header',
				'label'   => __( 'SubMenu Background Color', 'wiz' ),
			)
		)
	);
	/**
      * Option:SubMenu Link Color
      */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-link-color]', array(
			'default'           => wiz_get_option( 'submenu-link-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[submenu-link-color]', array(
				'label'   => __( 'SubMenu Link Color', 'wiz' ),
				'priority'       => 130,
				'section' => 'section-menu-header',
			)
		)
	);
	/**
	 * Option:Sub Menu Items Typography
	 * Option: Font Family
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[sub-menu-items-font-family]', array(
			'default'           => wiz_get_option( 'sub-menu-items-font-family' ),
			'type'              => 'option',
			'sanitize_callback' => 'sanitize_text_field',
		)
	);

	$wp_customize->add_control(
		new Wiz_Control_Typography(
			$wp_customize, WIZ_THEME_SETTINGS . '[sub-menu-items-font-family]', array(
				'type'        => 'leap-font-family',
				'section'     => 'section-menu-header',
				'priority'    => 135,
				'label'       => __( 'Submenu Font Family', 'wiz' ),
				'connect'     => WIZ_THEME_SETTINGS . '[sub-menu-items-font-weight]',
			)
		)
	);

	/**
	 * Option: SubMenu Font Weight
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[sub-menu-items-font-weight]', array(
			'default'           => 'inherit',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Typography(
			$wp_customize, WIZ_THEME_SETTINGS . '[sub-menu-items-font-weight]', array(
				'type'        => 'leap-font-weight',
				'section'     => 'section-menu-header',
				'priority'    => 140,
				'label'       => __( 'Font Weight', 'wiz' ),
				'connect'     => WIZ_THEME_SETTINGS . '[sub-menu-items-font-family]',
			)
		)
	);

	/**
	 * Option: SubMenu Items Text Transform
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[sub-menu-items-text-transform]', array(
			'default'           => wiz_get_option( 'sub-menu-items-text-transform' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[sub-menu-items-text-transform]', array(
			'type'     => 'select',
			'section'  => 'section-menu-header',
			'priority' => 145,
			'label'    => __( 'Text Transform', 'wiz' ),
			'choices'  => array(
				''           => __( 'Default', 'wiz' ),
				'none'       => __( 'None', 'wiz' ),
				'capitalize' => __( 'Capitalize', 'wiz' ),
				'uppercase'  => __( 'Uppercase', 'wiz' ),
				'lowercase'  => __( 'Lowercase', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Menu Items Line Height
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[sub-menu-items-line-height]', array(
			'default'           => '',
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[sub-menu-items-line-height]', array(
				'type'        => 'leap-slider',
				'section'     => 'section-menu-header',
				'priority'    => 150,
				'label'       => __( 'Submenu Line Height', 'wiz' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 1,
					'step' => 0.01,
					'max'  => 5,
				),
			)
		)
	);
	/**
      * Option:SubMenu Link Hover Color
      */
	  $wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-link-h-color]', array(
			'default'           => wiz_get_option( 'submenu-link-h-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[submenu-link-h-color]', array(
				'label'   => __( 'SubMenu Link Hover Color', 'wiz' ),
				'priority'       => 155,
				'section' => 'section-menu-header',
			)
		)
	);

		/**
	 * Option: submenu Top Border Size
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-top-border-size]', array(
			'default'           => wiz_get_option( 'submenu-top-border-size' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[submenu-top-border-size]', array(
			'type'        => 'number',
			'section'     => 'section-menu-header',
			'priority'    => 160,
			'label'       => __( 'Submenu Top Border Size', 'wiz' ),
			'input_attrs' => array(
				'min'  => 0,
				'step' => 1,
				'max'  => 600,
			),
		)
	);
	/**
	 * Option: top Border Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-top-border-color]', array(
			'default'           =>  wiz_get_option( 'submenu-top-border-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[submenu-top-border-color]', array(
				'section'  => 'section-menu-header',
				'priority' => 165,
				'label'    => __( 'Submenu Top Border Color', 'wiz' ),
			)
		)
	);

	/**
	 * Option: top Border Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[display-submenu-border]', array(
			'default'           =>  wiz_get_option( 'display-submenu-border' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[display-submenu-border]', array(
				'section'  => 'section-menu-header',
				'type'     => 'checkbox',
				'priority' => 170,
				'label'    => __( 'Display Submenu Border', 'wiz' ),
			)
		)
	);

/**
	 * Option: Sub menu Border Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[submenu-border-color]', array(
			'default'           =>  wiz_get_option( 'submenu-border-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[submenu-border-color]', array(
				'section'  => 'section-menu-header',
				'priority' => 175,
				'label'    => __( 'Submenu Border Color', 'wiz' ),
			)
		)
	);

	/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-mobile-menu-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Mobile Menu Settings', 'wiz' ),
				'section'  => 'section-menu-header',
				'priority' => 180,
				'settings' => array(),
			)
		)
	);

    /**
	 * Option: Mobile Menu Label
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-main-menu-label]', array(
			'default'           => wiz_get_option( 'header-main-menu-label' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[header-main-menu-label]', array(
			'section'  => 'section-menu-header',
			'priority' => 185,
			'label'    => __( 'Menu Label on Small Devices', 'wiz' ),
			'type'     => 'text',
		)
	);

	/**
	 * Option: Mobile Menu Alignment
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[header-main-menu-align]', array(
			'default'           => wiz_get_option( 'header-main-menu-align' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[header-main-menu-align]', array(
			'type'     => 'select',
			'section'  => 'section-menu-header',
			'priority' => 190,
			'label'    => __( 'Mobile Menu Alignment', 'wiz' ),
			'choices'  => array(
				'inline' => __( 'Inline', 'wiz' ),
				'stack'  => __( 'Stack', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Mobile Menu Style options
	*/
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-icon-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-icon-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-icon-color]', array(
				'label'   => __( 'Mobile Menu Icon Color', 'wiz' ),
				'priority'       => 195,
				'section' => 'section-menu-header',
			)
		)
	);

	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-icon-bg-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-icon-bg-color' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-icon-bg-color]', array(
        'priority'       => 200,
        'section' => 'section-menu-header',
				'label'   => __( 'Mobile Menu Background Color', 'wiz' ),
			)
		)
	);

		$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-icon-h-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-icon-h-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-icon-h-color]', array(
				'label'   => __( 'Mobile Menu Icon Hover Color', 'wiz' ),
				'priority'       => 205,
				'section' => 'section-menu-header',
			)
		)
	);

		$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-icon-bg-h-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-icon-bg-h-color' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-icon-bg-h-color]', array(
        'priority'       => 210,
        'section' => 'section-menu-header',
				'label'   => __( 'Mobile Menu Background Hover Color', 'wiz' ),
			)
		)
	);

		$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-items-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-items-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-items-color]', array(
				'label'   => __( 'Mobile Menu Items Color', 'wiz' ),
				'priority'       => 215,
				'section' => 'section-menu-header',
			)
		)
	);

	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-items-bg-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-items-bg-color' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-items-bg-color]', array(
        'priority'       => 220,
        'section' => 'section-menu-header',
				'label'   => __( 'Mobile Menu Items Background Color', 'wiz' ),
			)
		)
	);
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[mobile-menu-items-h-color]', array(
			'default'           => wiz_get_option( 'mobile-menu-items-h-color' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[mobile-menu-items-h-color]', array(
				'label'   => __( 'Mobile Menu Items Hover Color', 'wiz' ),
				'priority'       => 225,
				'section' => 'section-menu-header',
			)
		)
	);


