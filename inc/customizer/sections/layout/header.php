<?php
/**
* Header Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Header Layout
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-layouts]', array(
        'default'           => wiz_get_option( 'header-layouts' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);

$wp_customize->add_control(
    new Wiz_Control_Radio_Image(
        $wp_customize, WIZ_THEME_SETTINGS . '[header-layouts]', array(
            'section'  => 'section-header',
            'priority' => 5,
            'label'    => __( 'Header Layout', 'wiz' ),
            'type'     => 'leap-radio-image',
            'choices'  => array(
                'header-main-layout-1' => array(
                    'label' => __( 'Logo Left', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/header-layout-01.png',
                ),
                'header-main-layout-2' => array(
                    'label' => __( 'Logo Center', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/header-layout-02.png',
                ),
                'header-main-layout-3' => array(
                    'label' => __( 'Logo Right', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/header-layout-03.png',
                ),
                'header-main-layout-4' => array(
                    'label' => __( 'Right Left Menu', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/header-layout-04.png',
                ),
            ),
        )
    )
);

/**
* Option: Transparent header
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[enable-transparent]', array(
        'default'           => wiz_get_option( 'enable-transparent' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[enable-transparent]', array(
        'type'            => 'checkbox',
        'section'         => 'section-header',
        'label'           => __( 'Enable Overlay Header', 'wiz' ),
        'priority'        => 10,
    )
);

/**
* Option: Header Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-main-layout-width]', array(
        'default'           => wiz_get_option( 'header-main-layout-width' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[header-main-layout-width]', array(
        'type'     => 'select',
        'section'  => 'section-header',
        'priority' => 20,
        'label'    => __( 'Header Width', 'wiz' ),
        'choices'  => array(
            'full'    => __( 'Full Width', 'wiz' ),
            'content' => __( 'Content Width', 'wiz' ),
        ),
    )
);

/**
* Option: header Background
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-bg-obj]', array(
        'default'           => wiz_get_option('header-bg-obj'),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Background(
        $wp_customize, WIZ_THEME_SETTINGS . '[header-bg-obj]', array(
            'type'    => 'leap-background',
            'section' => 'section-header',
            'priority' => 20,
            'label'   => __( 'Header Background', 'wiz' ),
        )
    )
);
/**
* Option - header Spacing
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-padding]', array(
        'default'           => wiz_get_option( 'header-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[header-padding]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'section-header',
            'priority'       => 25,
            'label'          => __( 'Header Spacing', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

// if ( isset( $wp_customize->selective_refresh ) ) {
// 	$wp_customize->selective_refresh->add_partial(
// 		WIZ_THEME_SETTINGS . '[header-main-rt-section-html]', array(
// 			'selector'            => '.main-header-bar .leap-sitehead-custom-menu-items .leap-custom-html',
// 			'container_inclusive' => false,
// 			'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_header_main_rt_section_html' ),
// 		 )
// 	 );
// }

/**
* Option: Bottom Border Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-main-sep]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[header-main-sep]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-header',
            'priority'       => 30,
            'label'          => __( 'Bottom Border Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 15,
                ),
            ),
        )
    )
);
/**
* Option: Border Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[header-main-sep-color]', array(
        'default'           => wiz_get_option( 'header-main-sep-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[header-main-sep-color]', array(
            'section'  => 'section-header',
            'priority' => 35,
            'label'    => __( 'Border Color', 'wiz' ),
        )
    )
);