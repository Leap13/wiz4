<?php
/**
* Bottom Footer Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Footer Bar Layout
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[copyright-footer-layout]', array(
        'default'           => wiz_get_option( 'copyright-footer-layout' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Radio_Image(
        $wp_customize, WIZ_THEME_SETTINGS . '[copyright-footer-layout]', array(
            'type'     => 'leap-radio-image',
            'section'  => 'section-footer-copyright',
            'priority' => 5,
            'label'    => __( 'Footer Bar Layout', 'wiz' ),
            'choices'  => array(
                'disabled'            => array(
                    'label' => __( 'Disabled', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/disable-copyright-area.png',
                ),
                'copyright-footer-layout-1' => array(
                    'label' => __( 'Footer Bar Layout 1', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/copyright-area-layout-1.png',
                ),
                'copyright-footer-layout-2' => array(
                    'label' => __( 'Footer Bar Layout 2', 'wiz' ),
                    'path'  => WIZ_THEME_URI . 'assets/images/copyright-area-layout-2.png',
                ),
            ),
        )
    )
);

/**
* Option: Section 1
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-1]', array(
        'default'           => wiz_get_option( 'footer-copyright-section-1' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-1]', array(
        'type'     => 'select',
        'section'  => 'section-footer-copyright',
        'priority' => 10,
        'label'    => __( 'Section 1', 'wiz' ),
        'choices'  => array(
            ''       => __( 'None', 'wiz' ),
            'menu'   => __( 'Footer Menu', 'wiz' ),
            'custom' => __( 'Custom Text', 'wiz' ),
            'widget' => __( 'Widget', 'wiz' ),
        ),
    )
);

/**
* Option: Section 1 Custom Text
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-1-part]', array(
        'default'           => wiz_get_option( 'footer-copyright-section-1-part' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_html' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-1-part]', array(
        'type'     => 'textarea',
        'section'  => 'section-footer-copyright',
        'priority' => 15,
        'label'    => __( 'Section 1 Custom Text', 'wiz' ),
    )
);

if ( isset( $wp_customize->selective_refresh ) ) {
    $wp_customize->selective_refresh->add_partial(
        WIZ_THEME_SETTINGS . '[footer-copyright-section-1-part]', array(
            'selector'            => '.leap-footer-copyright-section-1',
            'container_inclusive' => false,
            'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_footer_copyright_section_1_part' ),
        )
    );
}

/**
* Option: Section 2
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-2]', array(
        'default'           => wiz_get_option( 'footer-copyright-section-2' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-2]', array(
        'type'     => 'select',
        'section'  => 'section-footer-copyright',
        'priority' => 20,
        'label'    => __( 'Section 2', 'wiz' ),
        'choices'  => array(
            ''       => __( 'None', 'wiz' ),
            'menu'   => __( 'Footer Menu', 'wiz' ),
            'custom' => __( 'Custom Text', 'wiz' ),
            'widget' => __( 'Widget', 'wiz' ),
        ),
    )
);

/**
* Option: Section 2 Custom Text
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-2-part]', array(
        'default'           => wiz_get_option( 'footer-copyright-section-2-part' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_html' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-copyright-section-2-part]', array(
        'type'     => 'textarea',
        'section'  => 'section-footer-copyright',
        'priority' => 25,
        'label'    => __( 'Section 2 Custom Text', 'wiz' ),
    )
);

if ( isset( $wp_customize->selective_refresh ) ) {
    $wp_customize->selective_refresh->add_partial(
        WIZ_THEME_SETTINGS . '[footer-copyright-section-2-part]', array(
            'selector'            => '.leap-footer-copyright-section-2',
            'container_inclusive' => false,
            'render_callback'     => array( 'Wiz_Customizer_Partials', '_render_footer_copyright_section_2_part' ),
        )
    );
}

/**
* Option: Footer Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-layout-width]', array(
        'default'           => wiz_get_option( 'footer-layout-width' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-layout-width]', array(
        'type'     => 'select',
        'section'  => 'section-footer-copyright',
        'priority' => 30,
        'label'    => __( 'Footer Bar Width', 'wiz' ),
        'choices'  => array(
            'full'    => __( 'Full Width', 'wiz' ),
            'content' => __( 'Content Width', 'wiz' ),
        ),
    )
);

/**
* Option: Footer Background
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-bg-obj]', array(
        'default'           => wiz_get_option( 'footer-bg-obj' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Background(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-bg-obj]', array(
            'type'    => 'leap-background',
            'section' => 'section-footer-copyright',
            'priority' => 35,
            'label'   => __( 'Background', 'wiz' ),
        )
    )
);

/**
* Option: Footer Top Border
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-divider]', array(
        'default'           => wiz_get_option( 'footer-copyright-divider' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-copyright-divider]', array(
        'type'        => 'number',
        'section'     => 'section-footer-copyright',
        'priority'    => 40,
        'label'       => __( 'Footer Bar Top Border Size', 'wiz' ),
        'input_attrs' => array(
            'min'  => 0,
            'step' => 1,
            'max'  => 600,
        ),
    )
);

/**
* Option: Footer Top Border Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-divider-color]', array(
        'default'           => '#7a7a7a',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-copyright-divider-color]', array(
            'section'  => 'section-footer-copyright',
            'priority' => 45,
            'label'    => __( 'Footer Bar Top Border Color', 'wiz' ),
        )
    )
);

/**
* Option: Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-color]', array(
            'label'   => __( 'Text Color', 'wiz' ),
            'section' => 'section-footer-copyright',
            'priority' => 50,
        )
    )
);

/**
* Option: Footer Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-copyright-font-size]', array(
        'default'           => wiz_get_option( 'footer-copyright-font-size' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-copyright-font-size]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-footer-copyright',
            'priority'       => 55,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Link Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-link-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-link-color]', array(
            'label'   => __( 'Link Color', 'wiz' ),
            'section' => 'section-footer-copyright',
            'priority'    => 60,
        )
    )
);

/**
* Option: Link Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-link-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-link-h-color]', array(
            'label'   => __( 'Link Hover Color', 'wiz' ),
            'section' => 'section-footer-copyright',
            'priority' => 65,
        )
    )
);
