<?php
/**
* Content Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-content-styling-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Content Styling', 'wiz' ),
            'section'  => 'section-contents',
            'priority' => 0,
            'settings' => array(),
        )
    )
);
/**
* Option: Content Text Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[content-text-color]', array(
        'default'           => wiz_get_option( 'content-text-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[content-text-color]', array(
            'label'   => __( 'Text Color', 'wiz' ),
            'priority'       => 5,
            'section' => 'section-contents',
        )
    )
);

/**
* Option: Body Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-body]', array(
        'default'           => wiz_get_option( 'font-size-body' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-body]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 10,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Font Family
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[body-font-family]', array(
        'default'           => wiz_get_option( 'body-font-family' ),
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[body-font-family]', array(
            'type'        => 'leap-font-family',
            //'leap_inherit' => __( 'Default System Font', 'wiz' ),
            'section'     => 'section-contents',
            'priority'    => 15,
            'label'       => __( 'Font Family', 'wiz' ),
            'connect'     => WIZ_THEME_SETTINGS . '[body-font-weight]',
        )
    )
);

/**
* Option: Font Weight
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[body-font-weight]', array(
        'default'           => wiz_get_option( 'body-font-weight' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[body-font-weight]', array(
            'type'        => 'leap-font-weight',
            //'leap_inherit' => __( 'Default', 'wiz' ),
            'section'     => 'section-contents',
            'priority'    => 20,
            'label'       => __( 'Font Weight', 'wiz' ),
            'connect'     => WIZ_THEME_SETTINGS . '[body-font-family]',
        )
    )
);

/**
* Option: Body Text Transform
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[body-text-transform]', array(
        'default'           => wiz_get_option( 'body-text-transform' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[body-text-transform]', array(
        'type'     => 'select',
        'section'  => 'section-contents',
        'priority' => 25,
        'label'    => __( 'Text Transform', 'wiz' ),
        'choices'  => array(
            ''           => __( 'Default', 'wiz' ),
            'none'       => __( 'None', 'wiz' ),
            'capitalize' => __( 'Capitalize', 'wiz' ),
            'uppercase'  => __( 'Uppercase', 'wiz' ),
            'lowercase'  => __( 'Lowercase', 'wiz' ),
        ),
    )
);

/**
* Option: Body Line Height
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[body-line-height]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[body-line-height]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-contents',
            'priority'    => 30,
            'label'       => __( 'Line Height', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 1,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option: Paragraph Margin Bottom
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[para-margin-bottom]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[para-margin-bottom]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-contents',
            'priority'    => 35,
            'label'       => __( 'Paragraph Margin Bottom', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 0.5,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option: Content Link Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[content-link-color]', array(
        'default'           => wiz_get_option( 'content-link-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[content-link-color]', array(
            'label'   => __( 'link Color', 'wiz' ),
            'priority'       => 40,
            'section' => 'section-contents',
        )
    )
);
/**
* Option: Content Link Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[content-link-h-color]', array(
        'default'           => wiz_get_option( 'content-link-h-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[content-link-h-color]', array(
            'label'   => __( 'Link Hover Color', 'wiz' ),
            'priority'       => 45,
            'section' => 'section-contents',
        )
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-heading-styling-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Heading Styling', 'wiz' ),
            'section'  => 'section-contents',
            'priority' => 46,
            'settings' => array(),
        )
    )
);

/**
* Option: Headings Font Family
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[headings-font-family]', array(
        'default'           => wiz_get_option( 'headings-font-family' ),
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[headings-font-family]', array(
            'type'     => 'leap-font-family',
            'label'    => __( 'Font Family', 'wiz' ),
            'section'  => 'section-contents',
            'priority' => 55,
            'connect'  => WIZ_THEME_SETTINGS . '[headings-font-weight]',
        )
    )
);

/**
* Option: Font Weight
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[headings-font-weight]', array(
        'default'           => wiz_get_option( 'headings-font-weight' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[headings-font-weight]', array(
            'type'        => 'leap-font-weight',
            'leap_inherit' => __( 'Default', 'wiz' ),
            'section'     => 'section-contents',
            'priority'    => 60,
            'label'       => __( 'Font Weight', 'wiz' ),
            'connect'     => WIZ_THEME_SETTINGS . '[headings-font-family]',
        )
    )
);

/**
* Option: Headings Text Transform
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[headings-text-transform]', array(
        'default'           => wiz_get_option( 'headings-text-transform' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[headings-text-transform]', array(
        'section'  => 'section-contents',
        'label'    => __( 'Text Transform', 'wiz' ),
        'type'     => 'select',
        'priority' => 65,
        'choices'  => array(
            ''           => __( 'Inherit', 'wiz' ),
            'none'       => __( 'None', 'wiz' ),
            'capitalize' => __( 'Capitalize', 'wiz' ),
            'uppercase'  => __( 'Uppercase', 'wiz' ),
            'lowercase'  => __( 'Lowercase', 'wiz' ),
        ),
    )
);

/**
* Option: Heading 1 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h1]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h1]', array(
            'label'   => __( 'H1 Font Color', 'wiz' ),
            'priority'       => 70,
            'section' => 'section-contents',
        )
    )
);

/**
* Option: Heading 1 ( H1 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h1]', array(
        'default'           => wiz_get_option( 'font-size-h1' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h1]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 75,
            'label'          => __( 'H1 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Heading 2 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h2]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h2]', array(
            'label'   => __( 'H2 Font Color', 'wiz' ),
            'priority'       => 80,
            'section' => 'section-contents',
        )
    )
);
/**
* Option: Heading 2 ( H2 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h2]', array(
        'default'           => wiz_get_option( 'font-size-h2' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h2]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 85,
            'label'          => __( 'H2 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Heading 3 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h3]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h3]', array(
            'label'   => __( 'H3 Font Color', 'wiz' ),
            'priority'       => 90,
            'section' => 'section-contents',
        )
    )
);
/**
* Option: Heading 3 ( H3 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h3]', array(
        'default'           => wiz_get_option( 'font-size-h3' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h3]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 95,
            'label'          => __( 'H3 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Heading 4 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h4]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h4]', array(
            'label'   => __( 'H4 Font Color', 'wiz' ),
            'priority'       => 100,
            'section' => 'section-contents',
        )
    )
);

/**
* Option: Heading 4 ( H4 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h4]', array(
        'default'           => wiz_get_option( 'font-size-h4' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h4]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 105,
            'label'          => __( 'H4 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);
/**
* Option: Heading 5 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h5]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h5]', array(
            'label'   => __( 'H5 Font Color', 'wiz' ),
            'priority'       => 110,
            'section' => 'section-contents',
        )
    )
);

/**
* Option: Heading 5 ( H5 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h5]', array(
        'default'           => wiz_get_option( 'font-size-h5' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h5]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 115,
            'label'          => __( 'H5 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);
/**
* Option: Heading 6 Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-h6]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-h6]', array(
            'label'   => __( 'H6 Font Color', 'wiz' ),
            'priority'       => 120,
            'section' => 'section-contents',
        )
    )
);
/**
* Option: Heading 6 ( H6 ) Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-h6]', array(
        'default'           => wiz_get_option( 'font-size-h6' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-h6]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 125,
            'label'          => __( 'H6 Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);
