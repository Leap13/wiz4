<?php
/**
 * sidebar Options for Wiz Theme.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


    /**
     * Option: Sidebar Width
     */
    $wp_customize->add_setting(
        WIZ_THEME_SETTINGS . '[site-sidebar-width]', array(
            'default'           => 30,
            'type'              => 'option',
            'transport'         => 'postMessage',
            'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
        )
    );
    $wp_customize->add_control(
        new Wiz_Control_Slider(
            $wp_customize, WIZ_THEME_SETTINGS . '[site-sidebar-width]', array(
                'type'        => 'leap-slider',
                'section'     => 'section-sidebars',
                'priority'    => 5,
                'label'       => __( 'Sidebar Width', 'wiz' ),
                'suffix'      => '%',
                'input_attrs' => array(
                    'min'  => 15,
                    'step' => 1,
                    'max'  => 50,
                ),
                'description'  => __('Sidebar width will apply only when one of the following sidebar is set.', 'wiz'),
            )
        )
    );


    /**
     * Option: Default Sidebar Position
     */
    $wp_customize->add_setting(
        WIZ_THEME_SETTINGS . '[site-sidebar-layout]', array(
            'default'           => wiz_get_option( 'site-sidebar-layout' ),
            'type'              => 'option',
            'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
        )
    );

    $wp_customize->add_control(
        WIZ_THEME_SETTINGS . '[site-sidebar-layout]', array(
            'type'     => 'select',
            'section'  => 'section-sidebars',
            'priority' => 15,
            'label'    => __( 'Default Layout', 'wiz' ),
            'choices'  => array(
                'no-sidebar'    => __( 'No Sidebar', 'wiz' ),
                'left-sidebar'  => __( 'Left Sidebar', 'wiz' ),
                'right-sidebar' => __( 'Right Sidebar', 'wiz' ),
            ),
        )
    );

    /**
     * Option: Page
     */
    $wp_customize->add_setting(
        WIZ_THEME_SETTINGS . '[single-page-sidebar-layout]', array(
            'default'           => wiz_get_option( 'single-page-sidebar-layout' ),
            'type'              => 'option',
            'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
        )
    );
    $wp_customize->add_control(
        WIZ_THEME_SETTINGS . '[single-page-sidebar-layout]', array(
            'type'     => 'select',
            'section'  => 'section-sidebars',
            'priority' => 20,
            'label'    => __( 'Pages', 'wiz' ),
            'choices'  => array(
                'default'       => __( 'Default', 'wiz' ),
                'no-sidebar'    => __( 'No Sidebar', 'wiz' ),
                'left-sidebar'  => __( 'Left Sidebar', 'wiz' ),
                'right-sidebar' => __( 'Right Sidebar', 'wiz' ),
            ),
        )
    );

    /**
         * Option: Blog Post Archive
         */
        $wp_customize->add_setting(
            WIZ_THEME_SETTINGS . '[archive-post-sidebar-layout]', array(
                'default'           => wiz_get_option( 'archive-post-sidebar-layout' ),
                'type'              => 'option',
                'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
            )
        );
        $wp_customize->add_control(
            WIZ_THEME_SETTINGS . '[archive-post-sidebar-layout]', array(
                'type'     => 'select',
                'section'  => 'section-sidebars',
                'priority' => 25,
                'label'    => __( 'Blog Post Archives', 'wiz' ),
                'choices'  => array(
                    'default'       => __( 'Default', 'wiz' ),
                    'no-sidebar'    => __( 'No Sidebar', 'wiz' ),
                    'left-sidebar'  => __( 'Left Sidebar', 'wiz' ),
                    'right-sidebar' => __( 'Right Sidebar', 'wiz' ),
                ),
            )
        );

    /**
     * Option: Blog Post
     */
    $wp_customize->add_setting(
        WIZ_THEME_SETTINGS . '[single-post-sidebar-layout]', array(
            'default'           => wiz_get_option( 'single-post-sidebar-layout' ),
            'type'              => 'option',
            'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
        )
    );
    $wp_customize->add_control(
        WIZ_THEME_SETTINGS . '[single-post-sidebar-layout]', array(
            'type'     => 'select',
            'section'  => 'section-sidebars',
            'priority' => 30,
            'label'    => __( 'Blog Posts', 'wiz' ),
            'choices'  => array(
                'default'       => __( 'Default', 'wiz' ),
                'no-sidebar'    => __( 'No Sidebar', 'wiz' ),
                'left-sidebar'  => __( 'Left Sidebar', 'wiz' ),
                'right-sidebar' => __( 'Right Sidebar', 'wiz' ),
            ),
        )
    );

    /**
	 * Option: sidebar Background
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[sidebar-bg-obj]', array(
			'default'           => wiz_get_option( 'sidebar-bg-obj' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Background(
			$wp_customize, WIZ_THEME_SETTINGS . '[sidebar-bg-obj]', array(
				'type'    => 'leap-background',
                'section' => 'section-sidebars',
                'priority' => 35,
				'label'   => __( 'Sidebar Background', 'wiz' ),
			)
		)
    );

            /**
         * Option: Text Color
        */
        $wp_customize->add_setting(
            WIZ_THEME_SETTINGS . '[sidebar-text-color]', array(
                'default'           => '',
                'type'              => 'option',
                'transport'         => 'postMessage',
                'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
            )
        );
        $wp_customize->add_control(
            new Wiz_Control_Color(
                $wp_customize, WIZ_THEME_SETTINGS . '[sidebar-text-color]', array(
                    'label'   => __( 'Text Color', 'wiz' ),
                    'priority'       => 40,
                    'section' => 'section-sidebars',
                )
            )
        );


        /**
        * Option: Link Color
        */
		$wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[sidebar-link-color]', array(
				'default'           => '',
				'type'              => 'option',
				'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
			)
		);
		$wp_customize->add_control(
			new Wiz_Control_Color(
				$wp_customize, WIZ_THEME_SETTINGS . '[sidebar-link-color]', array(
					'label'   => __( 'Link Color', 'wiz' ),
					'priority'       => 45,
					'section' => 'section-sidebars',
				)
			)
        );
        
        /**
        * Option: Link Hover Color
        */
		$wp_customize->add_setting(
			WIZ_THEME_SETTINGS . '[sidebar-link-h-color]', array(
				'default'           => '',
				'type'              => 'option',
				'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
			)
		);
		$wp_customize->add_control(
			new Wiz_Control_Color(
				$wp_customize, WIZ_THEME_SETTINGS . '[sidebar-link-h-color]', array(
					'label'   => __( 'Link Hover Color', 'wiz' ),
					'priority'       => 50,
					'section' => 'section-sidebars',
				)
			)
        );
        /**
    * Option - sidebar Spacing
    */
    $wp_customize->add_setting(
        WIZ_THEME_SETTINGS . '[sidebar-padding]', array(
            'default'           => wiz_get_option( 'sidebar-padding' ),
            'type'              => 'option',
            'transport'         => 'postMessage',
            'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
        )
    );
    $wp_customize->add_control(
        new Wiz_Control_Responsive_Spacing(
            $wp_customize, WIZ_THEME_SETTINGS . '[sidebar-padding]', array(
                'type'           => 'leap-responsive-spacing',
                'section'        => 'section-sidebars',
                'priority'       => 55,
                'label'          => __( 'sidebar Padding', 'wiz' ),
                'linked_choices' => true,
                'unit_choices'   => array( 'px', 'em', '%' ),
                'choices'        => array(
                    'top'    => __( 'Top', 'wiz' ),
                    'right'  => __( 'Right', 'wiz' ),
                    'bottom' => __( 'Bottom', 'wiz' ),
                    'left'   => __( 'Left', 'wiz' ),
                ),
            )
        )
    );

    /**
         * Option: Sidebar Input border Color
         */
        $wp_customize->add_setting(
            WIZ_THEME_SETTINGS . '[sidebar-input-border-color]', array(
                'default'           => wiz_get_option( 'sidebar-input-border-color' ),
                'type'              => 'option',
                'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
            )
        );
        $wp_customize->add_control(
            new Wiz_Control_Color(
                $wp_customize, WIZ_THEME_SETTINGS . '[sidebar-input-border-color]', array(
                    'priority'       => 60,
                    'section' => 'section-sidebars',
                    'label'   => __( 'Sidebar Input Border Color', 'wiz' ),
                )
            )
        );

        /**
         * Option: Sidebar Input color
         */
        $wp_customize->add_setting(
            WIZ_THEME_SETTINGS . '[sidebar-input-color]', array(
                'default'           => wiz_get_option( 'sidebar-input-color' ),
                'type'              => 'option',
                'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
            )
        );
        $wp_customize->add_control(
            new Wiz_Control_Color(
                $wp_customize, WIZ_THEME_SETTINGS . '[sidebar-input-color]', array(
                    'section' => 'section-sidebars',
                    'label'   => __( 'Sidebar Input Color', 'wiz' ),
                    'priority'       =>65,
                )
            )
        );
        /**
         * Option: Sidebar Input Background Color
         */
        $wp_customize->add_setting(
            WIZ_THEME_SETTINGS . '[sidebar-input-bg-color]', array(
                'default'           => wiz_get_option( 'sidebar-input-bg-color' ),
                'type'              => 'option',
                'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
            )
        );
        $wp_customize->add_control(
            new Wiz_Control_Color(
                $wp_customize, WIZ_THEME_SETTINGS . '[sidebar-input-bg-color]', array(
                    'priority'       => 70,
                    'section' => 'section-sidebars',
                    'label'   => __( 'Sidebar Input Background Color', 'wiz' ),
                )
            )
        );
        