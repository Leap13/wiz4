<?php
/**
* Single Post Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Display Post Structure
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-single-post-structure]', array(
        'default'           => wiz_get_option( 'blog-single-post-structure' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Sortable(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-single-post-structure]', array(
            'type'     => 'leap-sortable',
            'section'  => 'section-blog-single',
            'priority' => 5,
            'label'    => __( 'Single Post Structure', 'wiz' ),
            'choices'  => array(
                'single-image'      => __( 'Featured Image', 'wiz' ),
                'single-title-meta' => __( 'Title & Blog Meta', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Single Post Meta
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-single-meta]', array(
        'default'           => wiz_get_option( 'blog-single-meta' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_multi_choices' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Sortable(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-single-meta]', array(
            'type'     => 'leap-sortable',
            'section'  => 'section-blog-single',
            'priority' => 10,
            'label'    => __( 'Single Post Meta', 'wiz' ),
            'choices'  => array(
                'author'   => __( 'Author', 'wiz' ),
                'category' => __( 'Category', 'wiz' ),
                'date'     => __( 'Publish Date', 'wiz' ),
                'comments' => __( 'Comments', 'wiz' ),
                'tag'      => __( 'Tag', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-single-post-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Single Post Settings', 'wiz' ),
            'section'  => 'section-blog-single',
            'priority' => 15,
            'settings' => array(),
        )
    )
);

/**
* Option: Single Post Content Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-single-width]', array(
        'default'           => wiz_get_option( 'blog-single-width' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[blog-single-width]', array(
        'type'     => 'select',
        'section'  => 'section-blog-single',
        'priority' => 20,
        'label'    => __( 'Single Post Content Width', 'wiz' ),
        'choices'  => array(
            'default' => __( 'Default', 'wiz' ),
            'custom'  => __( 'Custom', 'wiz' ),
        ),
    )
);

/**
* Option: Enter Width
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[blog-single-max-width]', array(
        'default'           => 1200,
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[blog-single-max-width]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-blog-single',
            'priority'    => 25,
            'label'       => __( 'Enter Width', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 768,
                'step' => 1,
                'max'  => 1920,
            ),
        )
    )
);

/**
* Option: Single Post / Page Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-size-entry-title]', array(
        'default'           => wiz_get_option( 'font-size-entry-title' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-size-entry-title]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-blog-single',
            'priority'       => 30,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Single Post / Page Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[font-color-entry-title]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[font-color-entry-title]', array(
            'type'    => 'leap-color',
            'priority'    => 35,
            'label'   => __( 'Font Color', 'wiz' ),
            'section' => 'section-blog-single',
        )
    )
);
