<?php
/**
 * General Options for Wiz Theme.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	/**
	* Option: Site Content Width
	*/
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[site-content-width]', array(
			'default'           => 1200,
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'validate_site_width' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Slider(
			$wp_customize, WIZ_THEME_SETTINGS . '[site-content-width]', array(
				'type'        => 'leap-slider',
				'section'     => 'section-container-layout',
				'priority'    => 5,
				'label'       => __( 'Container Width', 'wiz' ),
				'suffix'      => '',
				'input_attrs' => array(
					'min'  => 768,
					'step' => 1,
					'max'  => 1920,
				),
			)
		)
	);
    
   /**
	* Option: Site Content Layout
	*/
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[site-content-layout]', array(
			'default'           => wiz_get_option( 'site-content-layout' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[site-content-layout]', array(
			'type'     => 'select',
			'section'  => 'section-container-layout',
			'priority' => 10,
			'label'    => __( 'Default Container', 'wiz' ),
			'choices'  => array(
				'boxed-container'         => __( 'Boxed Layout', 'wiz' ),
				'content-boxed-container' => __( 'Boxed Content', 'wiz' ),
				'plain-container'         => __( 'Full Width Content', 'wiz' ),
				'page-builder'            => __( 'Stretched Content', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Single Page Content Layout
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[single-page-content-layout]', array(
			'default'           => wiz_get_option( 'single-page-content-layout' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[single-page-content-layout]', array(
			'type'     => 'select',
			'section'  => 'section-container-layout',
			'label'    => __( 'Container for Pages', 'wiz' ),
			'priority' => 15,
			'choices'  => array(
				'default'                 => __( 'Default', 'wiz' ),
				'boxed-container'         => __( 'Boxed Layout', 'wiz' ),
				'content-boxed-container' => __( 'Boxed Content', 'wiz' ),
				'plain-container'         => __( 'Full Width Conten', 'wiz' ),
				'page-builder'            => __( 'Stretched Content', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Single Post Content Layout
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[single-post-content-layout]', array(
			'default'           => wiz_get_option( 'single-post-content-layout' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[single-post-content-layout]', array(
			'type'     => 'select',
			'section'  => 'section-container-layout',
			'priority' => 20,
			'label'    => __( 'Container for Blog Posts', 'wiz' ),
			'choices'  => array(
				'default'                 => __( 'Default', 'wiz' ),
				'boxed-container'         => __( 'Boxed Layout', 'wiz' ),
				'content-boxed-container' => __( 'Boxed Content', 'wiz' ),
				'plain-container'         => __( 'Full Width Conten', 'wiz' ),
				'page-builder'            => __( 'Stretched Content', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Archive Post Content Layout
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[archive-post-content-layout]', array(
			'default'           => wiz_get_option( 'archive-post-content-layout' ),
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
		)
	);
	$wp_customize->add_control(
		WIZ_THEME_SETTINGS . '[archive-post-content-layout]', array(
			'type'     => 'select',
			'section'  => 'section-container-layout',
			'priority' => 25,
			'label'    => __( 'Container for Blog Archives', 'wiz' ),
			'choices'  => array(
				'default'                 => __( 'Default', 'wiz' ),
				'boxed-container'         => __( 'Boxed Layout', 'wiz' ),
				'content-boxed-container' => __( 'Boxed Content', 'wiz' ),
				'plain-container'         => __( 'Full Width Conten', 'wiz' ),
				'page-builder'            => __( 'Stretched Content', 'wiz' ),
			),
		)
	);

	/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-site-body-bg-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Body Background', 'wiz' ),
				'section'  => 'section-container-layout',
				'priority' => 30,
				'settings' => array(),
			)
		)
	);

	/**
	 * Option: Body Background
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[site-layout-outside-bg-obj]', array(
			'default'           => wiz_get_option( 'site-layout-outside-bg-obj' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Background(
			$wp_customize, WIZ_THEME_SETTINGS . '[site-layout-outside-bg-obj]', array(
				'type'     => 'leap-background',
				'section'  => 'section-container-layout',
				'priority' => 35,
				'label'    => __( 'Body Background', 'wiz' ),
			)
		)
	);
   	/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-site-boxed-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Boxed Inner Background', 'wiz' ),
				'section'  => 'section-container-layout',
				'priority' => 36,
				'settings' => array(),
			)
		)
	);
   /**
    * Option: Boxed Inner Background
    */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[site-boxed-inner-bg]', array(
			'default'           => wiz_get_option( 'site-boxed-inner-bg' ),
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Background(
			$wp_customize, WIZ_THEME_SETTINGS . '[site-boxed-inner-bg]', array(
				'type'     => 'leap-background',
				'section'  => 'section-container-layout',
				'priority' => 40,
				'label'    => __( 'Boxed Inner Background', 'wiz' ),
			)
		)
	);
		/**
	 * Option: Title
	 */
	$wp_customize->add_control(
		new Wiz_Control_Title(
			$wp_customize, WIZ_THEME_SETTINGS . '[leap-site-body-spacing-title]', array(
				'type'     => 'leap-title',
				'label'    => __( 'Body Spacing', 'wiz' ),
				'section'  => 'section-container-layout',
				'priority' => 45,
				'settings' => array(),
			)
		)
	);
    
   /**
    * Option - Container Inner Spacing
    */
   $wp_customize->add_setting(
       WIZ_THEME_SETTINGS . '[container-inner-spacing]', array(
           'default'           => wiz_get_option( 'container-inner-spacing' ),
           'type'              => 'option',
           'transport'         => 'postMessage',
           'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
       )
   );
   $wp_customize->add_control(
       new Wiz_Control_Responsive_Spacing(
           $wp_customize, WIZ_THEME_SETTINGS . '[container-inner-spacing]', array(
               'type'           => 'leap-responsive-spacing',
               'section'        => 'section-container-layout',
               'priority'       => 50,
               'label'          => __( 'Inner Container Spacing', 'wiz' ),
               'linked_choices' => true,
               'unit_choices'   => array( 'px', 'em', '%' ),
               'choices'        => array(
                   'top'    => __( 'Top', 'wiz' ),
                   'right'  => __( 'Right', 'wiz' ),
                   'bottom' => __( 'Bottom', 'wiz' ),
                   'left'   => __( 'Left', 'wiz' ),
               ),
           )
       )
   );
