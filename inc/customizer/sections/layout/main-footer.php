<?php
/**
* Bottom Footer Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option: Footer Widgets Layout Layout
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer]', array(
        'default'           => wiz_get_option( 'wiz-footer' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);

$wp_customize->add_control(
    new Wiz_Control_Radio_Image(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer]', array(
            'type'    => 'leap-radio-image',
            'label'   => __( 'Footer Widgets Layout', 'wiz' ),
            'priority'       => 5,
            'section' => 'section-wiz-footer',
            'choices' => array(
                'disabled' => array(
                    'label' => __( 'Disable', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/disable-footer.png',
                ),
                'layout-1' => array(
                    'label' => __( 'Layout 1', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-1.png',
                ),
                'layout-2' => array(
                    'label' => __( 'Layout 2', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-2.png',
                ),
                'layout-3' => array(
                    'label' => __( 'Layout 3', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-3.png',
                ),
                'layout-4' => array(
                    'label' => __( 'Layout 4', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-4.png',
                ),
                'layout-5' => array(
                    'label' => __( 'Layout 5', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-5.png',
                ),
                'layout-6' => array(
                    'label' => __( 'Layout 6', 'wiz' ),
                    'path'  => WIZ_THEME_URI . '/assets/images/footer-layout-6.png',
                ),
            ),
        )
    )
);

/**
* Option: Footer Content Align Center
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[enable-footer-content-center]', array(
        'default'           => wiz_get_option( 'enable-footer-content-center' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_checkbox' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[enable-footer-content-center]', array(
        'type'            => 'checkbox',
        'section'         => 'section-wiz-footer',
        'label'           => __( 'Footer Content Align Center', 'wiz' ),
        'priority'        => 9,
    )
);

/**
* Option: Footer widget Background
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-bg-obj]', array(
        'default'           => wiz_get_option( 'wiz-footer-bg-obj' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_background_obj' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Background(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-bg-obj]', array(
            'type'    => 'leap-background',
            'section' => 'section-wiz-footer',
            'priority' => 11,
            'label'   => __( 'Footer Background', 'wiz' ),
        )
    )
);

/**
* Option: Text Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-text-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-text-color]', array(
            'label'   => __( 'Footer Text Color', 'wiz' ),
            'priority'       => 15,
            'section' => 'section-wiz-footer',
        )
    )
);

/**
* Option: Footer Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-font-size]', array(
        'default'           => wiz_get_option( 'footer-font-size' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-font-size]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-wiz-footer',
            'priority'       => 20,
            'label'          => __( 'Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);
/**
* Option: Footer Font Family
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-font-family]', array(
        'default'           => 'inherit',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-font-family]', array(
            'type'     => 'leap-font-family',
            'label'    => __( 'Font Family', 'wiz' ),
            'section'  => 'section-wiz-footer',
            'priority' => 25,
            'connect'  => WIZ_THEME_SETTINGS . '[footer-font-weight]',
        )
    )
);

/**
* Option: Footer Font Weight
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-font-weight]', array(
        'default'           => wiz_get_option( 'footer-font-weight' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-font-weight]', array(
            'type'     => 'leap-font-weight',
            'label'    => __( 'Font Weight', 'wiz' ),
            'section'  => 'section-wiz-footer',
            'priority' => 30,
            'connect'  => WIZ_THEME_SETTINGS . '[footer-font-family]',

        )
    )
);

/**
* Option: Footer Text Transform
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-text-transform]', array(
        'default'           => wiz_get_option( 'footer-text-transform' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[footer-text-transform]', array(
        'section'  => 'section-wiz-footer',
        'label'    => __( 'Text Transform', 'wiz' ),
        'type'     => 'select',
        'priority' => 35,
        'choices'  => array(
            ''           => __( 'Inherit', 'wiz' ),
            'none'       => __( 'None', 'wiz' ),
            'capitalize' => __( 'Capitalize', 'wiz' ),
            'uppercase'  => __( 'Uppercase', 'wiz' ),
            'lowercase'  => __( 'Lowercase', 'wiz' ),
        ),
    )
);

/**
* Option: Footer Line Height
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-line-height]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-line-height]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-wiz-footer',
            'priority'    => 40,
            'label'       => __( 'Line Height', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 1,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option - Footer Spacing
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-padding]', array(
        'default'           => wiz_get_option( 'footer-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-padding]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'section-wiz-footer',
            'priority'       => 45,
            'label'          => __( 'Footer Padding', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Title
*/
$wp_customize->add_control(
    new Wiz_Control_Title(
        $wp_customize, WIZ_THEME_SETTINGS . '[leap-footer-widgets-title]', array(
            'type'     => 'leap-title',
            'label'    => __( 'Footer Widgets Settings', 'wiz' ),
            'section'  => 'section-wiz-footer',
            'priority' => 46,
            'settings' => array(),
        )
    )
);

/**
* Option - Widget Spacing
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-widget-padding]', array(
        'default'           => wiz_get_option( 'footer-widget-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-widget-padding]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'section-wiz-footer',
            'priority'       => 46,
            'label'          => __( 'Widget Spacing', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Widget Title Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-widget-title-font-size]', array(
        'default'           => wiz_get_option( 'wiz-footer-widget-title-font-size' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-widget-title-font-size]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-wiz-footer',
            'priority'       => 20,
            'label'          => __( 'Widget Title Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Widget Title Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-color]', array(
            'label'   => __( 'Widget Title Color', 'wiz' ),
            'priority'       => 55,
            'section' => 'section-wiz-footer',
        )
    )
);
/**
* Option: Widget Title Font Family
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-family]', array(
        'default'           => 'inherit',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-family]', array(
            'type'     => 'leap-font-family',
            'label'    => __( 'Footer Widget Title Font Family', 'wiz' ),
            'section'  => 'section-wiz-footer',
            'priority' => 56,
            'connect'  => WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-weight]',
        )
    )
);

/**
* Option: Widget Title Font Weight
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-weight]', array(
        'default'           => wiz_get_option( 'wiz-footer-wgt-title-font-weight' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-weight]', array(
            'type'     => 'leap-font-weight',
            'label'    => __( 'Widget Title Font Weight', 'wiz' ),
            'section'  => 'section-wiz-footer',
            'priority' => 57,
            'connect'  => WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-font-family]',

        )
    )
);

/**
* Option: Widget Title Text Transform
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-text-transform]', array(
        'default'           => wiz_get_option( 'wiz-footer-wgt-title-text-transform' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-text-transform]', array(
        'section'  => 'section-wiz-footer',
        'label'    => __( 'Widget Title Text Transform', 'wiz' ),
        'type'     => 'select',
        'priority' => 58,
        'choices'  => array(
            ''           => __( 'Inherit', 'wiz' ),
            'none'       => __( 'None', 'wiz' ),
            'capitalize' => __( 'Capitalize', 'wiz' ),
            'uppercase'  => __( 'Uppercase', 'wiz' ),
            'lowercase'  => __( 'Lowercase', 'wiz' ),
        ),
    )
);

/**
* Option: Widget Title Line Height
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-line-height]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-wgt-title-line-height]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-wiz-footer',
            'priority'    => 59,
            'label'       => __( 'Widget Title Line Height', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 1,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option: Link Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-link-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-link-color]', array(
            'label'   => __( 'Link Color', 'wiz' ),
            'priority'       => 60,
            'section' => 'section-wiz-footer',
        )
    )
);

/**
* Option: Link Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-link-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-link-h-color]', array(
            'label'   => __( 'Link Hover Color', 'wiz' ),
            'priority'       => 65,
            'section' => 'section-wiz-footer',
        )
    )
);

/**
* Option: Widget Meta Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[wiz-footer-widget-meta-color]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[wiz-footer-widget-meta-color]', array(
            'label'   => __( 'Widget Meta Color', 'wiz' ),
            'priority'       => 70,
            'section' => 'section-wiz-footer',
        )
    )
);

/**
* Option: Button Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-button-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-button-color]', array(
            'section' => 'section-wiz-footer',
            'label'   => __( 'Button Text Color', 'wiz' ),
            'priority'       => 80,
        )
    )
);

/**
* Option: Button Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-button-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-button-h-color]', array(
            'priority'       => 85,
            'section' => 'section-wiz-footer',
            'label'   => __( 'Button Text Hover Color', 'wiz' ),
        )
    )
);

/**
* Option: Button Background Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-button-bg-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-button-bg-color]', array(
            'priority'       => 90,
            'section' => 'section-wiz-footer',
            'label'   => __( 'Button Background Color', 'wiz' ),
        )
    )
);

/**
* Option: Button Background Hover Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-button-bg-h-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-button-bg-h-color]', array(
            'priority'       => 95,
            'section' => 'section-wiz-footer',
            'label'   => __( 'Button Background Hover Color', 'wiz' ),
        )
    )
);

/**
* Option: Button Radius
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-button-radius]', array(
        'default'           => wiz_get_option( 'footer-button-radius' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-button-radius]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-wiz-footer',
            'priority'       => 100,
            'label'          => __( 'Button Radius', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>100,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
                '%' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' => 100,
                ),
            ),
        )
    )
);

/**
* Option: Footer Input color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-input-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-input-color]', array(
            'section' => 'section-wiz-footer',
            'label'   => __( 'Footer Input Color', 'wiz' ),
            'priority'       => 110,
        )
    )
);

/**
* Option: Footer Input Background Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-input-bg-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-input-bg-color]', array(
            'priority'       => 115,
            'section' => 'section-wiz-footer',
            'label'   => __( 'Footer Input Background Color', 'wiz' ),
        )
    )
);

/**
* Option: Footer Input border Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[footer-input-border-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[footer-input-border-color]', array(
            'priority'       => 120,
            'section' => 'section-wiz-footer',
            'label'   => __( 'Footer Input Border Color', 'wiz' ),
        )
    )
);
