<?php
/**
* Widget Options for Wiz Theme.
*
* @package     Wiz
* @author      Wiz
* @copyright   Copyright ( c ) 2019, Wiz
* @link        https://themes.leap13.com/wiz/
* @since       Wiz 1.0.0
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
* Option - Widgets Spacing
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-padding]', array(
        'default'           => wiz_get_option( 'widget-padding' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_spacing' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Spacing(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-padding]', array(
            'type'           => 'leap-responsive-spacing',
            'section'        => 'section-widgets',
            'priority'       => 5,
            'label'          => __( 'Widget Spacing', 'wiz' ),
            'linked_choices' => true,
            'unit_choices'   => array( 'px', 'em', '%' ),
            'choices'        => array(
                'top'    => __( 'Top', 'wiz' ),
                'right'  => __( 'Right', 'wiz' ),
                'bottom' => __( 'Bottom', 'wiz' ),
                'left'   => __( 'Left', 'wiz' ),
            ),
        )
    )
);

/**
* Option: Widgets Background Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[Widget-bg-color]', array(
        'default'           => '',
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[Widget-bg-color]', array(
            'priority'       => 10,
            'section' => 'section-widgets',
            'label'   => __( 'Widget Background Color', 'wiz' ),
        )
    )
);

/**
* Option: Widgets Margin Bottom
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-margin-bottom]', array(
        'default'           => '',
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-margin-bottom]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-widgets',
            'priority'    => 15,
            'label'       => __( 'Widget Margin Bottom', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 0.5,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option:Widgets Title Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-color]', array(
        'default'           => wiz_get_option( 'widget-title-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-color]', array(
            'label'   => __( 'Widget Title Color', 'wiz' ),
            'priority'       => 20,
            'section' => 'section-widgets',
        )
    )
);

/**
* Option: Widget Font Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-font-size]', array(
        'default'           => wiz_get_option( 'widget-title-font-size' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_responsive_slider' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Responsive_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-font-size]', array(
            'type'           => 'leap-responsive-slider',
            'section'        => 'section-contents',
            'priority'       => 25,
            'label'          => __( 'Widget Title Font Size', 'wiz' ),
            'unit_choices'   => array(
                'px' => array(
                    'min' => 1,
                    'step' => 1,
                    'max' =>200,
                ),
                'em' => array(
                    'min' => 0.1,
                    'step' => 0.1,
                    'max' => 10,
                ),
            ),
        )
    )
);

/**
* Option: Widget Font Family
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-font-family]', array(
        'default'           => 'inherit',
        'type'              => 'option',
        'sanitize_callback' => 'sanitize_text_field',
    )
);

$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-font-family]', array(
            'type'        => 'leap-font-family',
            'leap_inherit' => __( 'Default System Font', 'wiz' ),
            'section'     => 'section-widgets',
            'priority'    => 30,
            'label'       => __( 'Widget Title Font Family', 'wiz' ),
            'connect'     => WIZ_THEME_SETTINGS . '[widget-title-font-weight]',
        )
    )
);

/**
* Option: Widget Font Weight
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-font-weight]', array(
        'default'           => wiz_get_option( 'widget-title-font-weight' ),
        'type'              => 'option',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_font_weight' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Typography(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-font-weight]', array(
            'type'        => 'leap-font-weight',
            'leap_inherit' => __( 'Default', 'wiz' ),
            'section'     => 'section-widgets',
            'priority'    => 35,
            'label'       => __( 'Widget Title Font Weight', 'wiz' ),
            'connect'     => WIZ_THEME_SETTINGS . '[widget-title-font-family]',
        )
    )
);

/**
* Option: Widget Text Transform
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-text-transform]', array(
        'default'           => wiz_get_option( 'widget-title-text-transform' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_choices' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[widget-title-text-transform]', array(
        'type'     => 'select',
        'section'  => 'section-widgets',
        'priority' => 45,
        'label'    => __( 'Widget Title Text Transform', 'wiz' ),
        'choices'  => array(
            ''           => __( 'Default', 'wiz' ),
            'none'       => __( 'None', 'wiz' ),
            'capitalize' => __( 'Capitalize', 'wiz' ),
            'uppercase'  => __( 'Uppercase', 'wiz' ),
            'lowercase'  => __( 'Lowercase', 'wiz' ),
        ),
    )
);

/**
* Option: widget Line Height
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-line-height]', array(
        'default'           => wiz_get_option( 'widget-title-line-height' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number_n_blank' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Slider(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-line-height]', array(
            'type'        => 'leap-slider',
            'section'     => 'section-widgets',
            'priority'    => 50,
            'label'       => __( 'Widget Title Line Height', 'wiz' ),
            'suffix'      => '',
            'input_attrs' => array(
                'min'  => 1,
                'step' => 0.01,
                'max'  => 5,
            ),
        )
    )
);

/**
* Option: Widget Title Border Size
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-border-size]', array(
        'default'           => wiz_get_option( 'widget-title-border-size' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_number' ),
    )
);
$wp_customize->add_control(
    WIZ_THEME_SETTINGS . '[widget-title-border-size]', array(
        'type'        => 'number',
        'section'     => 'section-widgets',
        'priority'    => 55,
        'label'       => __( 'Widget Title Border Size', 'wiz' ),
        'input_attrs' => array(
            'min'  => 0,
            'step' => 1,
            'max'  => 600,
        ),
    )
);

/**
* Option: Widget Title Border Color
*/
$wp_customize->add_setting(
    WIZ_THEME_SETTINGS . '[widget-title-border-color]', array(
        'default'           => wiz_get_option( 'widget-title-border-color' ),
        'type'              => 'option',
        'transport'         => 'postMessage',
        'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
    )
);
$wp_customize->add_control(
    new Wiz_Control_Color(
        $wp_customize, WIZ_THEME_SETTINGS . '[widget-title-border-color]', array(
            'section'  => 'section-widgets',
            'priority' => 60,
            'label'    => __( 'Widget Title Border Color', 'wiz' ),
        )
    )
);
