<?php
/**
 * Styling Options for Wiz Theme.
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
	/**
	 * Option: Theme Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[theme-color]', array(
			'default'           => '#191919',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[theme-color]', array(
				'section'  => 'section-colors-body',
				'priority' => 5,
				'label'    => __( 'Theme Color', 'wiz' ),
			)
		)
	);

	/**
	 * Option: Link Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[link-color]', array(
			'default'           => '#191919',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[link-color]', array(
				'section'  => 'section-colors-body',
				'priority' => 5,
				'label'    => __( 'Link Color', 'wiz' ),
			)
		)
	);

	/**
	 * Option: Text Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[text-color]', array(
			'default'           => '#333333',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[text-color]', array(
				'section'  => 'section-colors-body',
				'priority' => 10,
				'label'    => __( 'Text Color', 'wiz' ),
			)
		)
	);


	/**
	 * Option: Link Hover Color
	 */
	$wp_customize->add_setting(
		WIZ_THEME_SETTINGS . '[link-h-color]', array(
			'default'           => '#000000',
			'type'              => 'option',
			'sanitize_callback' => array( 'Wiz_Customizer_Sanitizes', 'sanitize_alpha_color' ),
		)
	);
	$wp_customize->add_control(
		new Wiz_Control_Color(
			$wp_customize, WIZ_THEME_SETTINGS . '[link-h-color]', array(
				'section'  => 'section-colors-body',
				'priority' => 15,
				'label'    => __( 'Link Hover Color', 'wiz' ),
			)
		)
	);
