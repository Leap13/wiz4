/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and
 * then make any necessary changes to the page using jQuery.
 *
 * @package Wiz
 */

/**
 * Generate font size in PX & REM
 */
function wiz_font_size_rem(size, with_rem, device) {

	var css = '';

	if (size != '') {

		var device = (typeof device != undefined) ? device : 'desktop';

		// font size with 'px'.
		css = 'font-size: ' + size + 'px;';

		// font size with 'rem'.
		if (with_rem) {
			var body_font_size = wp.customize('wiz-settings[font-size-body]').get();

			body_font_size['desktop'] = (body_font_size['desktop'] != '') ? body_font_size['desktop'] : 15;
			body_font_size['tablet'] = (body_font_size['tablet'] != '') ? body_font_size['tablet'] : body_font_size['desktop'];
			body_font_size['mobile'] = (body_font_size['mobile'] != '') ? body_font_size['mobile'] : body_font_size['tablet'];

			css += 'font-size: ' + (size / body_font_size[device]) + 'rem;';
		}
	}

	return css;
}

/**
 * Responsive Font Size CSS
 */
function wiz_responsive_font_size(control, selector) {

	wp.customize(control, function (value) {
		value.bind(function (value) {

			if (value.desktop || value.mobile || value.tablet) {
				// Remove <style> first!
				control = control.replace('[', '-');
				control = control.replace(']', '');
				jQuery('style#' + control).remove();

				var fontSize = '',
					TabletFontSize = '',
					MobileFontSize = '';


				if ('' != value.desktop) {
					fontSize = 'font-size: ' + value.desktop + value['desktop-unit'];
				}
				if ('' != value.tablet) {
					TabletFontSize = 'font-size: ' + value.tablet + value['tablet-unit'];
				}
				if ('' != value.mobile) {
					MobileFontSize = 'font-size: ' + value.mobile + value['mobile-unit'];
				}

				if (value['desktop-unit'] == 'px') {
					fontSize = wiz_font_size_rem(value.desktop, true, 'desktop');
				}

				// Concat and append new <style>.
				jQuery('head').append(
					'<style id="' + control + '">'
					+ selector + '	{ ' + fontSize + ' }'
					+ '@media (max-width: 768px) {' + selector + '	{ ' + TabletFontSize + ' } }'
					+ '@media (max-width: 544px) {' + selector + '	{ ' + MobileFontSize + ' } }'
					+ '</style>'
				);

			} else {

				jQuery('style#' + control).remove();
			}

		});
	});
}

/**
 * Responsive Spacing CSS
 */
function wiz_responsive_spacing(control, selector, type, side) {

	wp.customize(control, function (value) {
		value.bind(function (value) {

			var sidesString = "";
			var spacingType = "padding";
			if (value.desktop.top || value.desktop.right || value.desktop.bottom || value.desktop.left || value.tablet.top || value.tablet.right || value.tablet.bottom || value.tablet.left || value.mobile.top || value.mobile.right || value.mobile.bottom || value.mobile.left) {
				if (typeof side != undefined) {
					sidesString = side + "";
					sidesString = sidesString.replace(/,/g, "-");
				}
				if (typeof type != undefined) {
					spacingType = type + "";
				}
				// Remove <style> first!
				control = control.replace('[', '-');
				control = control.replace(']', '');
				jQuery('style#' + control + '-' + spacingType + '-' + sidesString).remove();

				var desktopPadding = '',
					tabletPadding = '',
					mobilePadding = '';

				var paddingSide = (typeof side != undefined) ? side : ['top', 'bottom', 'right', 'left'];

				jQuery.each(paddingSide, function (index, sideValue) {
					if ('' != value['desktop'][sideValue]) {
						desktopPadding += spacingType + '-' + sideValue + ': ' + value['desktop'][sideValue] + value['desktop-unit'] + ';';
					}
				});

				jQuery.each(paddingSide, function (index, sideValue) {
					if ('' != value['tablet'][sideValue]) {
						tabletPadding += spacingType + '-' + sideValue + ': ' + value['tablet'][sideValue] + value['tablet-unit'] + ';';
					}
				});

				jQuery.each(paddingSide, function (index, sideValue) {
					if ('' != value['mobile'][sideValue]) {
						mobilePadding += spacingType + '-' + sideValue + ': ' + value['mobile'][sideValue] + value['mobile-unit'] + ';';
					}
				});

				// Concat and append new <style>.
				jQuery('head').append(
					'<style id="' + control + '-' + spacingType + '-' + sidesString + '">'
					+ selector + '	{ ' + desktopPadding + ' }'
					+ '@media (max-width: 768px) {' + selector + '	{ ' + tabletPadding + ' } }'
					+ '@media (max-width: 544px) {' + selector + '	{ ' + mobilePadding + ' } }'
					+ '</style>'
				);

			} else {
				wp.customize.preview.send('refresh');
				jQuery('style#' + control + '-' + spacingType + '-' + sidesString).remove();
			}

		});
	});
}
/**
 * Responsive Spacing CSS
 */
function wiz_responsive_slider(control, selector, type) {

	wp.customize(control, function (value) {
		value.bind(function (value) {
			var spacingType = "width";

			if (value.desktop || value.tablet || value.mobile) {
				if (typeof type != undefined) {
					spacingType = type + "";
				}
				// Remove <style> first!
				control = control.replace('[', '-');
				control = control.replace(']', '');
				jQuery('style#' + control + '-' + spacingType).remove();

				var desktopWidth = '',
					tabletWidth = '',
					mobileWidth = '';


				desktopWidth += spacingType + ': ' + value['desktop'] + value['desktop-unit'] + ' ;';

				tabletWidth += spacingType + ': ' + value['tablet'] + value['tablet-unit'] + ' ;';

				mobileWidth += spacingType + ': ' + value['mobile'] + value['mobile-unit'] + ' ;';

				// Concat and append new <style>.
				jQuery('head').append(
					'<style id="' + control + '-' + spacingType + '">'
					+ selector + '	{ ' + desktopWidth + ' }'
					+ '@media (max-width: 768px) {' + selector + '	{ ' + tabletWidth + ' } }'
					+ '@media (max-width: 544px) {' + selector + '	{ ' + mobileWidth + ' } }'
					+ '</style>'
				);

			} else {
				wp.customize.preview.send('refresh');
				jQuery('style#' + control + '-' + spacingType).remove();
			}

		});
	});
}
/**
 * CSS
 */
function wiz_css_font_size(control, selector) {

	wp.customize(control, function (value) {
		value.bind(function (size) {

			if (size) {

				// Remove <style> first!
				control = control.replace('[', '-');
				control = control.replace(']', '');
				jQuery('style#' + control).remove();

				var fontSize = 'font-size: ' + size;
				if (!isNaN(size) || size.indexOf('px') >= 0) {
					size = size.replace('px', '');
					fontSize = wiz_font_size_rem(size, true);
				}

				// Concat and append new <style>.
				jQuery('head').append(
					'<style id="' + control + '">'
					+ selector + '	{ ' + fontSize + ' }'
					+ '</style>'
				);

			} else {

				jQuery('style#' + control).remove();
			}

		});
	});
}

/**
 * Return get_hexdec()
 */
function get_hexdec(hex) {
	var hexString = hex.toString(16);
	return parseInt(hexString, 16);
}

/**
 * Apply CSS for the element
 */
function wiz_css(control, css_property, selector, unit) {

	wp.customize(control, function (value) {
		value.bind(function (new_value) {

			// Remove <style> first!
			control = control.replace('[', '-');
			control = control.replace(']', '');

			if (new_value) {

				/**
				 *	If ( unit == 'url' ) then = url('{VALUE}')
				 *	If ( unit == 'px' ) then = {VALUE}px
				 *	If ( unit == 'em' ) then = {VALUE}em
				 *	If ( unit == 'rem' ) then = {VALUE}rem.
				 */
				if ('undefined' != typeof unit) {

					if ('url' === unit) {
						new_value = 'url(' + new_value + ')';
					} else {
						new_value = new_value + unit;
					}
				}

				// Remove old.
				jQuery('style#' + control).remove();

				// Concat and append new <style>.
				jQuery('head').append(
					'<style id="' + control + '">'
					+ selector + '	{ ' + css_property + ': ' + new_value + ' }'
					+ '</style>'
				);

			} else {

				wp.customize.preview.send('refresh');

				// Remove old.
				jQuery('style#' + control).remove();
			}

		});
	});
}


/**
 * Dynamic Internal/Embedded Style for a Control
 */
function wiz_add_dynamic_css(control, style) {
	control = control.replace('[', '-');
	control = control.replace(']', '');
	jQuery('style#' + control).remove();

	jQuery('head').append(
		'<style id="' + control + '">' + style + '</style>'
	);
}

/**
 * Generate background_obj CSS
 */
function wiz_background_obj_css(wp_customize, bg_obj, ctrl_name, style) {
	
	var gen_bg_css = '';
	var bg_img = bg_obj['background-image'];
	var bg_color = bg_obj['background-color'];

	if ('' === bg_color && '' === bg_img) {
		wp_customize.preview.send('refresh');
	} else {
		if ('' !== bg_img && '' !== bg_color) {
			if (undefined !== bg_color) {
				gen_bg_css = 'background-image: linear-gradient(to right, ' + bg_color + ', ' + bg_color + '), url(' + bg_img + ');';
			}
		} else if ('' !== bg_img) {
			gen_bg_css = 'background-image: url(' + bg_img + ');';
		} else if ('' !== bg_color) {
			gen_bg_css = 'background-color: ' + bg_color + ';';
			gen_bg_css += 'background-image: none;';
		}

		if ('' !== bg_img) {

			gen_bg_css += 'background-repeat: ' + bg_obj['background-repeat'] + ';';
			gen_bg_css += 'background-position: ' + bg_obj['background-position'] + ';';
			gen_bg_css += 'background-size: ' + bg_obj['background-size'] + ';';
			gen_bg_css += 'background-attachment: ' + bg_obj['background-attachment'] + ';';
		}
		var dynamicStyle = style.replace("{{css}}", gen_bg_css);

		wiz_add_dynamic_css(ctrl_name, dynamicStyle);
	}
}

(function ($) {

	wiz_responsive_slider('wiz-settings[leap-header-responsive-logo-width]', '#sitehead .site-logo-img .custom-logo-link img', 'max-width');
	wiz_responsive_slider('wiz-settings[leap-header-responsive-logo-width]', '.wiz-logo-svg', 'width');
	/*
	 * Full width layout
	 */
	wp.customize('wiz-settings[site-content-width]', function (setting) {
		setting.bind(function (width) {


			var dynamicStyle = '@media (min-width: 554px) {';
			dynamicStyle += '.leap-container, .fl-builder #content .entry-header { max-width: ' + (40 + parseInt(width)) + 'px } ';
			dynamicStyle += '}';
			if (jQuery('body').hasClass('leap-page-builder-template')) {
				dynamicStyle += '@media (min-width: 554px) {';
				dynamicStyle += '.leap-page-builder-template .comments-area { max-width: ' + (40 + parseInt(width)) + 'px } ';
				dynamicStyle += '}';
			}

			wiz_add_dynamic_css('site-content-width', dynamicStyle);

		});
	});

	/*
	 * Full width layout
	 */
	wp.customize('wiz-settings[header-main-menu-label]', function (setting) {
		setting.bind(function (label) {
			if ($('button.main-header-menu-toggle .mobile-menu-wrap .mobile-menu').length > 0) {
				if (label != '') {
					$('button.main-header-menu-toggle .mobile-menu-wrap .mobile-menu').text(label);
				} else {
					$('button.main-header-menu-toggle .mobile-menu-wrap').remove();
				}
			} else {
				var html = $('button.main-header-menu-toggle').html();
				if ('' != label) {
					html += '<div class="mobile-menu-wrap"><span class="mobile-menu">' + label + '</span> </div>';
				}
				$('button.main-header-menu-toggle').html(html)
			}
		});
	});

	/*
	 * Layout Body Background
	 */
	wp.customize('wiz-settings[site-layout-outside-bg-obj]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = 'body,.leap-separate-container { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'site-layout-outside-bg-obj', dynamicStyle);
		});
	});

    /*
	 * Boxed Inner Background
	 */
	wp.customize('wiz-settings[site-boxed-inner-bg]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'site-boxed-inner-bg', dynamicStyle);
		});
	});

	/*
	 * Blog Custom Width
	 */
	wp.customize('wiz-settings[blog-max-width]', function (setting) {
		setting.bind(function (width) {

			var dynamicStyle = '@media all and ( min-width: 921px ) {';

			if (!jQuery('body').hasClass('leap-woo-shop-archive')) {
				dynamicStyle += '.blog .site-content > .leap-container,.archive .site-content > .leap-container{ max-width: ' + (parseInt(width)) + 'px } ';
			}

			if (jQuery('body').hasClass('leap-fluid-width-layout')) {
				dynamicStyle += '.blog .site-content > .leap-container,.archive .site-content > .leap-container{ padding-left:20px; padding-right:20px; } ';
			}
			dynamicStyle += '}';
			wiz_add_dynamic_css('blog-max-width', dynamicStyle);

		});
	});


	/*
	 * Single Blog Custom Width
	 */
	wp.customize('wiz-settings[blog-single-max-width]', function (setting) {
		setting.bind(function (width) {

			var dynamicStyle = '@media all and ( min-width: 921px ) {';

			dynamicStyle += '.single-post .site-content > .leap-container{ max-width: ' + (40 + parseInt(width)) + 'px } ';

			if (jQuery('body').hasClass('leap-fluid-width-layout')) {
				dynamicStyle += '.single-post .site-content > .leap-container{ padding-left:20px; padding-right:20px; } ';
			}
			dynamicStyle += '}';
			wiz_add_dynamic_css('blog-single-max-width', dynamicStyle);

		});
	});

	/**
	 * Primary Width Option
	 */
	wp.customize('wiz-settings[site-sidebar-width]', function (setting) {
		setting.bind(function (width) {

			if (!jQuery('body').hasClass('leap-no-sidebar') && width >= 15 && width <= 50) {

				var dynamicStyle = '@media (min-width: 769px) {';

				dynamicStyle += '#primary { width: ' + (100 - parseInt(width)) + '% } ';
				dynamicStyle += '#secondary { width: ' + width + '% } ';
				dynamicStyle += '}';

				wiz_add_dynamic_css('site-sidebar-width', dynamicStyle);
			}

		});
	});

	/**
	 * Small Footer Top Border
	 */
	wp.customize('wiz-settings[footer-copyright-divider]', function (value) {
		value.bind(function (border_width) {
			jQuery('.leap-footer-copyright').css('border-top-width', border_width + 'px');
		});
	});

	/**
	 * Small Footer Top Border Color
	 */
	wp.customize('wiz-settings[footer-copyright-divider-color]', function (value) {
		value.bind(function (border_color) {
			jQuery('.leap-footer-copyright').css('border-top-color', border_color);
		});
	});

	/**
	 * Button Border Radius
	 */
	wiz_responsive_slider('wiz-settings[button-radius]', '.menu-toggle,button,.leap-button,input#submit,input[type="button"],input[type="submit"],input[type="reset"]', 'border-radius');
	if (jQuery('body').hasClass('woocommerce')) {
		wiz_responsive_slider('wiz-settings[button-radius]', '.woocommerce a.button, .woocommerce button.button, .woocommerce .product a.button, .woocommerce .woocommerce-message a.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce input.button,.woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled]', 'border-radius');
	}
	/**
	 * Button Border Color
	 */

	wp.customize('wiz-settings[btn-border-color]', function (value) {
		value.bind(function (border_color) {
			jQuery('.menu-toggle, button, .leap-button, input[type=button], input[type=button]:focus, input[type=button]:hover, input[type=reset], input[type=reset]:focus, input[type=reset]:hover, input[type=submit], input[type=submit]:focus, input[type=submit]:hover').css('border-color', border_color);
			if (jQuery('body').hasClass('woocommerce')) {
				jQuery('.woocommerce a.button, .woocommerce button.button, .woocommerce .product a.button, .woocommerce .woocommerce-message a.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce input.button,.woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled').css('border-color', border_color);
			}
		});
	});
	wp.customize('wiz-settings[btn-border-h-color]', function (value) {
		value.bind(function (color) {
			if (color == '') {
				wp.customize.preview.send('refresh');
			}
			if (color) {

				var dynamicStyle = 'button:focus, .menu-toggle:hover, button:hover, .leap-button:hover, .button:hover, input[type=reset]:hover, input[type=reset]:focus, input#submit:hover, input#submit:focus, input[type="button"]:hover, input[type="button"]:focus, input[type="submit"]:hover, input[type="submit"]:focus { border-color: ' + color + '; } ';
				wiz_add_dynamic_css('btn-border-h-color', dynamicStyle);
			}
			if (jQuery('body').hasClass('woocommerce')) {
				jQuery('.woocommerce a.button, .woocommerce button.button, .woocommerce .product a.button, .woocommerce .woocommerce-message a.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce input.button,.woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled').css('border-color', border_color);
			}
		});
	});
	wp.customize('wiz-settings[btn-border-size]', function (setting) {
		setting.bind(function (border) {

			var dynamicStyle = '.menu-toggle, button, .leap-button, input[type=button], input[type=button]:focus, input[type=button]:hover, input[type=reset], input[type=reset]:focus, input[type=reset]:hover, input[type=submit], input[type=submit]:focus, input[type=submit]:hover { border-width: ' + border + 'px }';

			wiz_add_dynamic_css('btn-border-size', dynamicStyle);
		});
	});

	/**
	 * Button Vertical Padding
	 */
	wp.customize('wiz-settings[button-v-padding]', function (setting) {
		setting.bind(function (padding) {

			var dynamicStyle = '.menu-toggle,button,.leap-button,input#submit,input[type="button"],input[type="submit"],input[type="reset"] { padding-top: ' + (parseInt(padding)) + 'px; padding-bottom: ' + (parseInt(padding)) + 'px } ';

			if (jQuery('body').hasClass('woocommerce')) {
				dynamicStyle += '.woocommerce a.button, .woocommerce button.button, .woocommerce .product a.button, .woocommerce .woocommerce-message a.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce input.button,.woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled] { padding-top: ' + (parseInt(padding)) + 'px; padding-bottom: ' + (parseInt(padding)) + 'px } ';
			}
			wiz_add_dynamic_css('button-v-padding', dynamicStyle);

		});
	});

	/**
	 * Button Horizontal Padding
	 */
	wp.customize('wiz-settings[button-h-padding]', function (setting) {
		setting.bind(function (padding) {

			var dynamicStyle = '.menu-toggle,button,.leap-button,input#submit,input[type="button"],input[type="submit"],input[type="reset"] { padding-left: ' + (parseInt(padding)) + 'px; padding-right: ' + (parseInt(padding)) + 'px } ';
			if (jQuery('body').hasClass('woocommerce')) {
				dynamicStyle += '.woocommerce a.button, .woocommerce button.button, .woocommerce .product a.button, .woocommerce .woocommerce-message a.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce input.button,.woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled] { padding-left: ' + (parseInt(padding)) + 'px; padding-right: ' + (parseInt(padding)) + 'px } ';
			}
			wiz_add_dynamic_css('button-h-padding', dynamicStyle);

		});
	});

	/**
	 * Header Bottom Border width
	 */
	wiz_responsive_slider('wiz-settings[header-main-sep]', 'body:not(.leap-header-break-point) .main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon', 'border-bottom-width');

	/**
     * widget Padding
     */
	wiz_responsive_spacing('wiz-settings[widget-padding]', '.sidebar-main .widget', 'padding', ['top', 'bottom', 'right', 'left']);

	// widget margin bottom.
	wp.customize('wiz-settings[widget-margin-bottom]', function (value) {
		value.bind(function (marginBottom) {
			if (marginBottom == '') {
				wp.customize.preview.send('refresh');
			}

			if (marginBottom) {
				var dynamicStyle = '.sidebar-main .widget { margin-bottom: ' + marginBottom + 'em; } ';
				wiz_add_dynamic_css('widget-margin-bottom', dynamicStyle);
			}

		});
	});

	/**
	 * Header Bottom Border color
	 */
	wp.customize('wiz-settings[header-main-sep-color]', function (value) {
		value.bind(function (color) {
			if (color == '') {
				wp.customize.preview.send('refresh');
			}

			if (color) {

				var dynamicStyle = ' body:not(.leap-header-break-point) .main-header-bar, .header-main-layout-5 .main-header-container.logo-menu-icon { border-bottom-color: ' + color + '; } ';
				dynamicStyle += ' body.leap-header-break-point .site-header { border-bottom-color: ' + color + '; } ';

				wiz_add_dynamic_css('header-main-sep-color', dynamicStyle);
			}

		});
	});
	/**
	 * Header background
	 */
	wp.customize('wiz-settings[header-bg-obj]', function (value) {
		value.bind(function (bg_obj) {
			
			var dynamicStyle = ' body:not(.leap-header-break-point) .site-header .main-header-bar { {{css}} }';
			wiz_background_obj_css(wp.customize, bg_obj, 'header-bg-obj', dynamicStyle);
		});
	});

	/**
     * Header Spacing
     */
	wiz_responsive_spacing('wiz-settings[header-padding]', '.main-header-bar', 'padding', ['top', 'bottom', 'right', 'left']);
	wiz_responsive_spacing('wiz-settings[last-menu-item-spacing]', '.site-header .leap-sitehead-custom-menu-items', 'padding', ['top', 'bottom', 'right', 'left']);

	/**
	 * menu background
	 */
	wp.customize('wiz-settings[menu-bg-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = '.main-header-menu { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'menu-bg-color', dynamicStyle);
		});
	});
	wiz_css('wiz-settings[menu-link-color]', 'color', '.main-header-menu a');
	wiz_css('wiz-settings[menu-link-bottom-border-color]', 'border-bottom-color', '.main-header-menu > .menu-item:hover > a');
	wiz_css('wiz-settings[menu-items-text-transform]', 'text-transform', '.main-header-menu a');
	wiz_css('wiz-settings[menu-items-line-height]', 'line-height', '.main-header-menu > .menu-item > a, .main-header-menu > .menu-item');

	wiz_css('wiz-settings[menu-link-h-color]', 'color', '.main-header-menu li:hover a, .main-header-menu .leap-sitehead-custom-menu-items a:hover');
	wiz_css('wiz-settings[menu-link-active-color]', 'color', '.main-header-menu li.current-menu-item a, .main-header-menu li.current_page_item a, .main-header-menu .current-menu-ancestor > a');
	/**
	 * submenu background
	 */
	wiz_css('wiz-settings[sub-menu-items-text-transform]', 'text-transform', '.main-header-menu .sub-menu li a');
	wiz_css('wiz-settings[sub-menu-items-line-height]', 'line-height', '.main-header-menu .sub-menu li a');
	
	wp.customize('wiz-settings[submenu-bg-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = '.main-header-menu ul { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'submenu-bg-color', dynamicStyle);
		});
	});
	//Search
	wiz_css('wiz-settings[search-btn-bg-color]', 'background-color', '.leap-search-menu-icon .search-submit');
	wiz_css('wiz-settings[search-btn-h-bg-color]', 'background-color', '.leap-search-menu-icon .search-submit:hover');
	wiz_css('wiz-settings[search-btn-color]', 'color', '.leap-search-menu-icon .search-submit');
	wiz_css('wiz-settings[search-input-bg-color]', 'background-color', '.leap-search-menu-icon form .search-field');
	wiz_css('wiz-settings[search-input-color]', 'color', '.leap-search-menu-icon form .search-field');
	wp.customize('wiz-settings[search-border-size]', function (setting) {
		setting.bind(function (border) {
			var dynamicStyle = '.search-box .leap-search-menu-icon .search-form { border-width: ' + border + 'px } .top-bar-search-box .wiz-top-header-section .leap-search-menu-icon .search-form { border-width: ' + border + 'px }';

			wiz_add_dynamic_css('search-border-size', dynamicStyle);
		});
	});
	wp.customize('wiz-settings[search-border-color]', function (value) {
		value.bind(function (border_color) {
			jQuery('.leap-search-menu-icon form').css('border-color', border_color);
			jQuery('.leap-search-menu-icon form').css('background-color', border_color);
		});
	});
	/**submenu color */
	wiz_css('wiz-settings[submenu-link-color]', 'color', '.main-header-menu .sub-menu li a');
	wiz_css('wiz-settings[submenu-link-h-color]', 'color', '.main-header-menu .sub-menu li:hover > a');

	/**
	 * SubMenu Top Border
	 */
	wp.customize('wiz-settings[submenu-top-border-size]', function (setting) {
		setting.bind(function (border) {

			var dynamicStyle = '.main-header-menu ul.sub-menu { border-top-width: ' + border + 'px }';

			wiz_add_dynamic_css('submenu-top-border-size', dynamicStyle);

		});
	});

	/**
	 * SubMenu Top Border color
	 */
	wp.customize('wiz-settings[submenu-top-border-color]', function (value) {
		value.bind(function (color) {
			if (color == '') {
				wp.customize.preview.send('refresh');
			}

			if (color) {

				var dynamicStyle = '.main-header-menu ul.sub-menu { border-top-color: ' + color + '; } ';

				wiz_add_dynamic_css('submenu-top-border-color', dynamicStyle);
			}

		});
	});

	/**
	 * SubMenu Border color
	 */
	wp.customize('wiz-settings[submenu-border-color]', function (value) {
		value.bind(function (border_color) {
			jQuery('.main-header-menu .sub-menu a').css('border-color', border_color);
		});
	});

	wiz_css('wiz-settings[submenu-link-h-color]', 'color', '.main-header-menu .sub-menu li:hover > a');

	/**
	 * Mobile Menu color
	 */
	wiz_css('wiz-settings[mobile-menu-icon-color]', 'color', '.leap-mobile-menu-buttons .menu-toggle .menu-toggle-icon');
	wiz_css('wiz-settings[mobile-menu-icon-h-color]', 'color', '.leap-mobile-menu-buttons .menu-toggle:hover .menu-toggle-icon, .leap-mobile-menu-buttons .menu-toggle.toggled .menu-toggle-icon');
	wiz_css('wiz-settings[mobile-menu-icon-bg-color]', 'background-color', '.leap-mobile-menu-buttons .menu-toggle');
	wiz_css('wiz-settings[mobile-menu-icon-bg-h-color]', 'background-color', '.leap-mobile-menu-buttons .menu-toggle:hover, .leap-mobile-menu-buttons .menu-toggle.toggled');
	wiz_css('wiz-settings[mobile-menu-items-color]', 'color', '.toggle-on li a');
	wiz_css('wiz-settings[mobile-menu-items-h-color]', 'color', '.toggle-on .main-header-menu li a:hover, .toggle-on .main-header-menu li.current-menu-item a, .toggle-on .main-header-menu li.current_page_item a, .toggle-on .main-header-menu .current-menu-ancestor > a, .toggle-on .main-header-menu .sub-menu li:hover a');

	/**
	 * Container Inner Spacing
	 */
	wiz_responsive_spacing('wiz-settings[container-inner-spacing]', '.leap-separate-container .leap-article-post, .leap-separate-container .leap-article-single, .leap-separate-container .comment-respond, .single.leap-separate-container .leap-author-details, .leap-separate-container .leap-related-posts-wrap, .leap-separate-container .leap-woocommerce-container', 'padding', ['top', 'bottom', 'right', 'left']);
    /**
	 * Site Identity Spacing
	 */
	wiz_responsive_spacing('wiz-settings[site-identity-spacing]', '#sitehead.site-header .leap-site-identity', 'padding', ['top', 'right', 'bottom', 'left']);

    /**
     * Footer Padding
     */
	wiz_responsive_spacing('wiz-settings[footer-padding]', '.wiz-footer .leap-container', 'padding', ['top', 'bottom', 'right', 'left']);
	wiz_responsive_spacing('wiz-settings[footer-widget-padding]', '.wiz-footer .wiz-footer-widget', 'padding', ['top', 'bottom', 'right', 'left']);
	wiz_responsive_slider('wiz-settings[menu-font-size]', '.main-header-menu a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-site-tagline]', '.site-header .site-description', 'font-size');
	wiz_responsive_slider('wiz-settings[site-title-font-size]', '.site-title', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-entry-title]', '.leap-single-post .entry-title, .page-title', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-archive-summary-title]', '.leap-archive-description .leap-archive-title', 'font-size');
	wiz_responsive_slider('wiz-settings[wiz-footer-widget-title-font-size]', '.wiz-footer .widget-title', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-page-title]', 'body:not(.leap-single-post) .entry-title', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-page-meta]', 'body:not(.leap-single-post) .entry-meta', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h1]', 'h1, .entry-content h1, .entry-content h1 a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h2]', 'h2, .entry-content h2, .entry-content h2 a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h3]', 'h3, .entry-content h3, .entry-content h3 a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h4]', 'h4, .entry-content h4, .entry-content h4 a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h5]', 'h5, .entry-content h5, .entry-content h5 a', 'font-size');
	wiz_responsive_slider('wiz-settings[font-size-h6]', 'h6, .entry-content h6, .entry-content h6 a', 'font-size');
	/**
	 * Content Heading Color
	 */
	wiz_css('wiz-settings[font-color-h1]', 'color', 'h1, .entry-content h1, .entry-content h1 a');
	wiz_css('wiz-settings[font-color-h2]', 'color', 'h2, .entry-content h2, .entry-content h2 a');
	wiz_css('wiz-settings[font-color-h3]', 'color', 'h3, .entry-content h3, .entry-content h3 a');
	wiz_css('wiz-settings[font-color-h4]', 'color', 'h4, .entry-content h4, .entry-content h4 a');
	wiz_css('wiz-settings[font-color-h5]', 'color', 'h5, .entry-content h5, .entry-content h5 a');
	wiz_css('wiz-settings[font-color-h6]', 'color', 'h6, .entry-content h6, .entry-content h6 a');
	wiz_css('wiz-settings[site-title-color]', 'color', '.site-title a');
	wiz_css('wiz-settings[site-title-h-color]', 'color', '.site-title a:hover');

	wiz_css('wiz-settings[body-line-height]', 'line-height', 'body, button, input, select, textarea');

	// paragraph margin bottom.
	wp.customize('wiz-settings[para-margin-bottom]', function (value) {
		value.bind(function (marginBottom) {
			if (marginBottom == '') {
				wp.customize.preview.send('refresh');
			}

			if (marginBottom) {
				var dynamicStyle = ' p, .entry-content p { margin-bottom: ' + marginBottom + 'em; } ';
				wiz_add_dynamic_css('para-margin-bottom', dynamicStyle);
			}

		});
	});

	wiz_css('wiz-settings[body-text-transform]', 'text-transform', 'body, button, input, select, textarea');

	wiz_css('wiz-settings[headings-text-transform]', 'text-transform', 'h1, .entry-content h1, .entry-content h1 a, h2, .entry-content h2, .entry-content h2 a, h3, .entry-content h3, .entry-content h3 a, h4, .entry-content h4, .entry-content h4 a, h5, .entry-content h5, .entry-content h5 a, h6, .entry-content h6, .entry-content h6 a');

	/**
     * widget Padding
     */
	wiz_responsive_spacing('wiz-settings[widget-padding]', '.sidebar-main .widget', 'padding', ['top', 'bottom', 'right', 'left']);
	//Sidebar Widget Title Typography
	wiz_css('wiz-settings[widget-title-text-transform]', 'text-transform', '.sidebar-main .widget-title');
	wiz_css('wiz-settings[widget-title-line-height]', 'line-height', '.sidebar-main .widget-title');
	wiz_responsive_slider('wiz-settings[widget-title-font-size]', '.sidebar-main .widget-title', 'font-size');
	wiz_css('wiz-settings[widget-title-color]', 'color', '.sidebar-main .widget-title');
	/**
	 * widget Title Border width
	 */
	wp.customize('wiz-settings[widget-title-border-size]', function (value) {
		value.bind(function (border) {

			var dynamicStyle = '.sidebar-main .widget-title { border-bottom-width: ' + border + 'px }';
			dynamicStyle += '}';

			wiz_add_dynamic_css('widget-title-border-size', dynamicStyle);

		});
	});

	/**
	 * widget Title Border color
	 */
	wp.customize('wiz-settings[widget-title-border-color]', function (value) {
		value.bind(function (color) {
			if (color == '') {
				wp.customize.preview.send('refresh');
			}

			if (color) {

				var dynamicStyle = '.sidebar-main .widget-title { border-bottom-color: ' + color + '; } ';

				wiz_add_dynamic_css('widget-title-border-color', dynamicStyle);
			}

		});
	});


	/**
	 * Widget Title Font 
	 */
	wiz_css('wiz-settings[wiz-footer-wgt-title-text-transform]', 'text-transform', '.wiz-footer .leap-no-widget-row .widget-title');
	wiz_css('wiz-settings[wiz-footer-wgt-title-line-height]', 'line-height', '.wiz-footer .widget-title');

	// Footer Bar.
	wiz_css('wiz-settings[footer-color]', 'color', '.leap-footer-copyright');
	wiz_css('wiz-settings[footer-link-color]', 'color', '.leap-footer-copyright a');
	wiz_css('wiz-settings[footer-link-h-color]', 'color', '.leap-footer-copyright a:hover');
	wiz_css('wiz-settings[font-color-entry-title]', 'color', '.leap-single-post .entry-title, .page-title');

	wp.customize('wiz-settings[footer-bg-obj]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = ' .leap-footer-copyright > .leap-footer-copyright-content { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'footer-bg-obj', dynamicStyle);
		});
	});

	// Footer Font
	wiz_css('wiz-settings[footer-text-transform]', 'text-transform', '.wiz-footer');
	wiz_css('wiz-settings[footer-line-height]', 'line-height', '.wiz-footer');
	wiz_responsive_slider('wiz-settings[footer-font-size]', '.wiz-footer', 'font-size');

	// Footer Widgets.
	wiz_css('wiz-settings[wiz-footer-wgt-title-color]', 'color', '.wiz-footer .widget-title, .wiz-footer .widget-title a');
	wiz_css('wiz-settings[wiz-footer-text-color]', 'color', '.site-footer');
	wiz_css('wiz-settings[wiz-footer-widget-meta-color]', 'color', '.wiz-footer .post-date');
	wiz_css('wiz-settings[footer-button-color]', 'color', '.wiz-footer button, .wiz-footer .leap-button, .wiz-footer .button, .wiz-footer input#submit, .wiz-footer input[type=“button”], .wiz-footer input[type=“submit”], .wiz-footerinput[type=“reset”]');
	wiz_css('wiz-settings[footer-button-h-color]', 'color', '.wiz-footer button:focus, .wiz-footer button:hover, .wiz-footer .leap-button:hover, .wiz-footer .button:hover, .wiz-footer input[type=reset]:hover, .wiz-footer input[type=reset]:focus, .wiz-footer input#submit:hover, .wiz-footer input#submit:focus, .wiz-footer input[type="button"]:hover, .wiz-footer input[type="button"]:focus, .wiz-footer input[type="submit"]:hover, .wiz-footer input[type="submit"]:focus');

	wp.customize('wiz-settings[wiz-footer-bg-obj]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = ' .wiz-footer-overlay { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'wiz-footer-bg-obj', dynamicStyle);
		});
	});


	wp.customize('wiz-settings[footer-button-bg-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = ' .wiz-footer > input[type="submit"] { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'footer-button-bg-color', dynamicStyle);
		});
	});

	wp.customize('wiz-settings[footer-button-bg-h-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = ' .wiz-footer > input[type="submit"]:hover { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'footer-button-bg-h-color', dynamicStyle);
		});
	});

	/**
	 * Footer Button Border Radius
	 */
	wiz_responsive_slider('wiz-settings[footer-button-radius]', '.wiz-footer button, .wiz-footer .leap-button, .wiz-footer .button, .wiz-footer input#submit, .wiz-footer input[type=“button”], .wiz-footer input[type=“submit”], .wiz-footerinput[type=“reset”]', 'border-radius');

	/*
	 * Woocommerce Shop Archive Custom Width
	 */
	wp.customize('wiz-settings[shop-archive-max-width]', function (setting) {
		setting.bind(function (width) {

			var dynamicStyle = '@media all and ( min-width: 921px ) {';

			dynamicStyle += '.leap-woo-shop-archive .site-content > .leap-container{ max-width: ' + (parseInt(width)) + 'px } ';

			if (jQuery('body').hasClass('leap-fluid-width-layout')) {
				dynamicStyle += '.leap-woo-shop-archive .site-content > .leap-container{ padding-left:20px; padding-right:20px; } ';
			}
			dynamicStyle += '}';
			wiz_add_dynamic_css('shop-archive-max-width', dynamicStyle);

		});
	});

	/**
	 * widget background
	 */
	wp.customize('wiz-settings[Widget-bg-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = '.sidebar-main .widget { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'Widget-bg-color', dynamicStyle);
		});
	});
	wiz_responsive_spacing('wiz-settings[sidebar-padding]', 'div.sidebar-main', 'padding', ['top', 'bottom', 'right', 'left']);
	/**
	 * sidebar Background
	 */
	wiz_css('wiz-settings[sidebar-text-color]', 'color', '.sidebar-main *');
	wiz_css('wiz-settings[sidebar-link-color', 'color', '.sidebar-main a');
	wiz_css('wiz-settings[sidebar-link-h-color]', 'color', '.sidebar-main a:hover');
	wiz_css('wiz-settings[sidebar-input-color]', 'color', '.sidebar-main input,.sidebar-main input[type="text"],.sidebar-main input[type="email"],.sidebar-main input[type="url"],.sidebar-main input[type="password"],.sidebar-main input[type="reset"],.sidebar-main input[type="search"],.sidebar-main textarea ,.sidebar-main select');
	wp.customize('wiz-settings[sidebar-bg-obj]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = ' .sidebar-main { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'sidebar-bg-obj', dynamicStyle);
		});
	});
	/**
	 * sidebar input backgroundcolor 
	 */
	wp.customize('wiz-settings[sidebar-input-bg-color]', function (value) {
		value.bind(function (bg_obj) {

			var dynamicStyle = '.sidebar-main input,.sidebar-main input[type="text"],.sidebar-main input[type="email"],.sidebar-main input[type="url"],.sidebar-main input[type="password"],.sidebar-main input[type="reset"],.sidebar-main input[type="search"],.sidebar-main textarea ,.sidebar-main select { {{css}} }';

			wiz_background_obj_css(wp.customize, bg_obj, 'sidebar-input-bg-color', dynamicStyle);
		});
	});
	/**
	 * sidebar input border color 
	 */
	wp.customize('wiz-settings[sidebar-input-border-color]', function (value) {
		value.bind(function (border_color) {
			jQuery('.sidebar-main input,.sidebar-main input[type="text"],.sidebar-main input[type="email"],.sidebar-main input[type="url"],.sidebar-main input[type="password"],.sidebar-main input[type="reset"],.sidebar-main input[type="search"],.sidebar-main textarea ,.sidebar-main select').css('border-color', border_color);
		});
	});
	/**
	 * Content Text Color
	 */
	wiz_css('wiz-settings[content-text-color]', 'color', '.entry-content');
	//wiz_css('wiz-settings[post-meta-color]', 'color', 'body:not(.leap-single-post) .entry-meta * , body:not(.leap-single-post) .entry-meta');
	wp.customize('wiz-settings[post-meta-color]', function (value) {
		value.bind(function (color) {
			var dynamicStyle = 'body:not(.leap-single-post) .entry-meta * { color: ' + color + '; } ';
			dynamicStyle += 'body:not(.leap-single-post) .entry-meta { color: ' + color + '; } ';
			wiz_add_dynamic_css('post-meta-color', dynamicStyle);
		});
	});
	//Content Link Color
	wiz_css('wiz-settings[content-link-color]', 'color', '.entry-content a');
	/**
	 * Content link Hover Color
	 */
	wiz_css('wiz-settings[content-link-h-color]', 'color', '.entry-content a:hover');
	/**
	 * Listing Post 
	 */
	wiz_css('wiz-settings[listing-post-title-color]', 'color', '.content-area .entry-title a');
	wiz_css('wiz-settings[readmore-text-color]', 'color', '.content-area .read-more a');
	wiz_css('wiz-settings[readmore-text-h-color]', 'color', '.content-area .read-more a:hover');
	wiz_responsive_spacing('wiz-settings[readmore-padding]', '.content-area .read-more', 'padding', ['top', 'bottom', 'right', 'left']);

	wiz_css('wiz-settings[readmore-bg-color]', 'background-color', '.content-area .read-more a');
	wiz_css('wiz-settings[readmore-bg-h-color]', 'background-color', '.content-area .read-more a:hover');
	wiz_responsive_slider('wiz-settings[readmore-border-size]', '.content-area .read-more a', 'border-width');

	wiz_responsive_slider('wiz-settings[readmore-border-radius]', '.content-area .read-more a', 'border-radius');

	wp.customize('wiz-settings[readmore-border-color]', function (value) {
		value.bind(function (border_color) {

			var dynamicStyle = '.content-area .read-more a { border-color: ' + border_color + '; } ';

			wiz_add_dynamic_css('readmore-border-color', dynamicStyle);
		});
	});
	wp.customize('wiz-settings[readmore-border-h-color]', function (value) {
		value.bind(function (color) {
			if (color == '') {
				wp.customize.preview.send('refresh');
			}

			if (color) {

				var dynamicStyle = '.content-area .read-more a:hover { border-color: ' + color + '; } ';

				wiz_add_dynamic_css('readmore-border-h-color', dynamicStyle);
			}

		});
	});




})(jQuery);
