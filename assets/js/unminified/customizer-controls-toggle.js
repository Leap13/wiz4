/**
 * Customizer controls toggles
 *
 * @package Wiz
 */

( function( $ ) {

	/* Internal shorthand */
	var api = wp.customize;

	/**
	 * Trigger hooks
	 */
	WIZControlTrigger = {

	    /**
	     * Trigger a hook.
	     *
	     * @method triggerHook
	     * @param {String} hook The hook to trigger.
	     * @param {Array} args An array of args to pass to the hook.
		 */
	    triggerHook: function( hook, args )
	    {
	    	$( 'body' ).trigger( 'wiz-control-trigger.' + hook, args );
	    },

	    /**
	     * Add a hook.
	     *
	     * @method addHook
	     * @param {String} hook The hook to add.
	     * @param {Function} callback A function to call when the hook is triggered.
	     */
	    addHook: function( hook, callback )
	    {
	    	$( 'body' ).on( 'wiz-control-trigger.' + hook, callback );
	    },

	    /**
	     * Remove a hook.
	     *
	     * @method removeHook
	     * @param {String} hook The hook to remove.
	     * @param {Function} callback The callback function to remove.
	     */
	    removeHook: function( hook, callback )
	    {
		    $( 'body' ).off( 'wiz-control-trigger.' + hook, callback );
	    },
	};

	/**
	 * Helper class that contains data for showing and hiding controls.
	 *
	 * @class WIZCustomizerToggles
	 */
	WIZCustomizerToggles = {

		'wiz-settings[display-site-title]' :
		[
			{
				controls: [
					'wiz-settings[divider-section-header-typo-title]',
					'wiz-settings[site-title-font-size]',
				],
				callback: function( value ) {

					if ( value ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[logo-title-inline]',
				],
				callback: function( value ) {

					var site_tagline = api( 'wiz-settings[display-site-tagline]' ).get();
					var has_custom_logo = api( 'custom_logo' ).get();
					var has_retina_logo = api( 'wiz-settings[leap-header-retina-logo]' ).get();

					if ( ( value || site_tagline ) && ( has_custom_logo || has_retina_logo ) ) {
						return true;
					}
					return false;
				}
			},
		],

		'wiz-settings[display-site-tagline]' :
		[
			{
				controls: [
					'wiz-settings[divider-section-header-typo-tagline]',
					'wiz-settings[font-size-site-tagline]',
				],
				callback: function( value ) {

					if ( value ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[logo-title-inline]',
				],
				callback: function( value ) {

					var site_title = api( 'wiz-settings[display-site-title]' ).get();
					var has_custom_logo = api( 'custom_logo' ).get();
					var has_retina_logo = api( 'wiz-settings[leap-header-retina-logo]' ).get();

					if ( ( value || site_title ) && ( has_custom_logo || has_retina_logo ) ) {
						return true;
					}
					return false;
				}
			},
		],

		'wiz-settings[leap-header-retina-logo]' :
		[
			{
				controls: [
					'wiz-settings[logo-title-inline]',
				],
				callback: function( value ) {

					var has_custom_logo = api( 'custom_logo' ).get();
					var site_tagline = api( 'wiz-settings[display-site-tagline]' ).get();
					var site_title = api( 'wiz-settings[display-site-title]' ).get();

					if ( ( value || has_custom_logo ) && ( site_title || site_tagline ) ) {
						return true;
					}
					return false;
				}
			},
		],

		'custom_logo' :
		[
			{
				controls: [
					'wiz-settings[logo-title-inline]',
				],
				callback: function( value ) {

					var has_retina_logo = api( 'wiz-settings[leap-header-retina-logo]' ).get();
					var site_tagline = api( 'wiz-settings[display-site-tagline]' ).get();
					var site_title = api( 'wiz-settings[display-site-title]' ).get();

					if ( ( value || has_retina_logo ) && ( site_title || site_tagline ) ) {
						return true;
					}
					return false;
				}
			},
		],
		//Search Style
		'wiz-settings[search-style]':
		[
			{
				controls: [
					'wiz-settings[search-btn-bg-color]',
					'wiz-settings[search-btn-h-bg-color]',
					'wiz-settings[search-btn-color]',
					'wiz-settings[search-border-size]',
				],
				callback: function (value) {
					var last_menu_item = api('wiz-settings[header-main-rt-section]').get();
					
					if (('search-box' == value) && (last_menu_item == 'search')) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[search-box-shadow]',
				],
				callback: function (value) {
					var last_menu_item = api('wiz-settings[header-main-rt-section]').get();

					if (('search-icon' == value) && (last_menu_item == 'search')) {
						return true;
					}
					return false;
				}
			},
		],
		//Search Style
		'wiz-settings[header-layouts]':
		[
			{
				controls: [
					'wiz-settings[menu-alignment]',
				],
				callback: function (value) {

					if (('header-main-layout-1' == value) || ('header-main-layout-2' == value) || ('header-main-layout-3' == value)) {
						return true;
					}
					return false;
				}
			},
		],
		/**
		 * Section - Header
		 *
		 * @link  ?autofocus[section]=section-header
		 */

		/**
		 * Layout 2
		 */
		// Layout 2 > Right Section > Text / HTML
		// Layout 2 > Right Section > Search Type
		// Layout 2 > Right Section > Search Type > Search Box Type.
		'wiz-settings[header-main-rt-section]' :
		[
			{
				controls: [
					'wiz-settings[header-main-rt-section-html]'
				],
				callback: function( val ) {

					if ( val.includes('text-html') ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[search-style]',
					'wiz-settings[search-box-shadow]',
					'wiz-settings[search-input-bg-color]',
					'wiz-settings[search-input-color]',
					'wiz-settings[search-border-color]',
				],
				callback: function (val) {

					if (val.includes('search')) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[search-btn-bg-color]',
					'wiz-settings[search-btn-h-bg-color]',
					'wiz-settings[search-btn-color]',
					'wiz-settings[search-border-size]',
				],
				callback: function (val) {
					var search_style = api('wiz-settings[search-style]').get();
					if ('search' == val && search_style == 'search-box') {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[header-main-menu-label]',
					'wiz-settings[header-main-menu-label-divider]',
				],
				callback: function( custom_menu ) {
					var menu = api( 'wiz-settings[disable-primary-nav]' ).get();
					if ( !menu || 'none' !=  custom_menu) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[header-display-outside-menu]',
				],
				callback: function( custom_menu ) {
					
					if ( 'none' !=  custom_menu ) {
						return true;
					}
					return false;
				}
			}
		],

		/**
		 * Blog
		 */
		'wiz-settings[blog-width]' :
		[
			{
				controls: [
					'wiz-settings[blog-max-width]'
				],
				callback: function( blog_width ) {

					if ( 'custom' == blog_width ) {
						return true;
					}
					return false;
				}
		}
		],
		'wiz-settings[blog-post-structure]' :
		[
			{
				controls: [
					'wiz-settings[blog-meta]',
				],
				callback: function( blog_structure ) {
					if ( jQuery.inArray ( "title-meta", blog_structure ) !== -1 ) {
						return true;
					}
					return false;
				}
			}
		],

		/**
		 * Blog Single
		 */
		 'wiz-settings[blog-single-post-structure]' :
		[
			{
				controls: [
					'wiz-settings[blog-single-meta]',
				],
				callback: function( blog_structure ) {
					if ( jQuery.inArray ( "single-title-meta", blog_structure ) !== -1 ) {
						return true;
					}
					return false;
				}
			}
		],
		'wiz-settings[blog-single-width]' :
		[
			{
				controls: [
					'wiz-settings[blog-single-max-width]'
				],
				callback: function( blog_width ) {

					if ( 'custom' == blog_width ) {
						return true;
					}
					return false;
				}
		}
		],
		'wiz-settings[blog-single-meta]' :
		[
			{
				controls: [	
					'wiz-settings[blog-single-meta-author]',
					'wiz-settings[blog-single-meta-cat]',
					'wiz-settings[blog-single-meta-date]',
					'wiz-settings[blog-single-meta-comments]',
					'wiz-settings[blog-single-meta-tag]',
				],
				callback: function( enable_postmeta ) {

					if ( '1' == enable_postmeta ) {
						return true;
					}
					return false;
				}
		}
		],

		/**
		 * Small Footer
		 */
		'wiz-settings[copyright-footer-layout]' :
		[
			{
				controls: [
					'wiz-settings[footer-copyright-section-1]',
					'wiz-settings[footer-copyright-section-2]',
					'wiz-settings[section-leap-footer-copyright-background-styling]',
					'wiz-settings[leap-footer-copyright-color]',
					'wiz-settings[leap-footer-copyright-link-color]',
					'wiz-settings[leap-footer-copyright-link-hover-color]',
					'wiz-settings[leap-footer-copyright-bg-img]',
					'wiz-settings[leap-footer-copyright-text-font]',
					'wiz-settings[footer-copyright-font-size]',
					'wiz-settings[footer-copyright-divider]',
					'wiz-settings[footer-layout-width]',
					'wiz-settings[footer-color]',
					'wiz-settings[footer-link-color]',
					'wiz-settings[footer-link-h-color]',
					'wiz-settings[footer-bg-obj]',
					'wiz-settings[divider-footer-image]',
				],
				callback: function( copyright_footer_layout ) {

					if ( 'disabled' != copyright_footer_layout ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[footer-copyright-section-1-part]',
				],
				callback: function( copyright_footer_layout ) {

					var footer_section_1 = api( 'wiz-settings[footer-copyright-section-1]' ).get();

					if ( 'disabled' != copyright_footer_layout && 'custom' == footer_section_1 ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[footer-copyright-section-2-part]',
				],
				callback: function( copyright_footer_layout ) {

					var footer_section_2 = api( 'wiz-settings[footer-copyright-section-2]' ).get();

					if ( 'disabled' != copyright_footer_layout && 'custom' == footer_section_2 ) {
						return true;
					}
					return false;
				}
			},
			{
				controls: [
					'wiz-settings[footer-copyright-divider-color]',
				],
				callback: function( copyright_footer_layout ) {

					var border_width = api( 'wiz-settings[footer-copyright-divider]' ).get();

					if ( '1' <= border_width && 'disabled' != copyright_footer_layout ) {
						return true;
					}
					return false;
				}
			},
		],
		'wiz-settings[footer-copyright-section-1]' :
		[
			{
				controls: [
					'wiz-settings[footer-copyright-section-1-part]',
				],
				callback: function( enabled_section_1 ) {

					var footer_layout = api( 'wiz-settings[copyright-footer-layout]' ).get();

					if ( 'custom' == enabled_section_1 && 'disabled' != footer_layout ) {
						return true;
					}
					return false;
				}
			}
		],
		'wiz-settings[footer-copyright-section-2]' :
		[
			{
				controls: [
					'wiz-settings[footer-copyright-section-2-part]',
				],
				callback: function( enabled_section_2 ) {

					var footer_layout = api( 'wiz-settings[copyright-footer-layout]' ).get();

					if ( 'custom' == enabled_section_2 && 'disabled' != footer_layout ) {
						return true;
					}
					return false;
				}
			}
		],

		'wiz-settings[footer-copyright-divider]' :
		[
			{
				controls: [
					'wiz-settings[footer-copyright-divider-color]',
				],
				callback: function( border_width ) {

					var footer_layout = api( 'wiz-settings[copyright-footer-layout]' ).get();

					if ( '1' <= border_width && 'disabled' != footer_layout ) {
						return true;
					}
					return false;
				}
			},
		],

		'wiz-settings[header-main-sep]' :
		[
			{
				controls: [
					'wiz-settings[header-main-sep-color]',
				],
				callback: function( border_width ) {

					if ( '1' <= border_width ) {
						return true;
					}
					return false;
				}
			},
		],

		'wiz-settings[disable-primary-nav]' :
		[
			{
				controls: [
					'wiz-settings[header-main-menu-label]',
					'wiz-settings[header-main-menu-label-divider]',
				],
				callback: function( menu ) {
					var custom_menu = api( 'wiz-settings[header-main-rt-section]' ).get();
					if ( !menu || 'none' !=  custom_menu) {
						return true;
					}
					return false;
				}
			},
		],



		/**
		 * Footer Widgets
		 */
		'wiz-settings[wiz-footer]' :
		[
			{
				controls: [
					'wiz-settings[wiz-footer-background-divider]',
					'wiz-settings[wiz-footer-wgt-title-color]',
					'wiz-settings[wiz-footer-text-color]',
					'wiz-settings[wiz-footer-link-color]',
					'wiz-settings[wiz-footer-link-h-color]',
					'wiz-settings[wiz-footer-bg-obj]',
				],
				callback: function( footer_widget_area ) {

					if ( 'disabled' != footer_widget_area ) {
						return true;
					}
					return false;
				}
			},
		],
		'wiz-settings[shop-archive-width]' :
		[
			{
				controls: [
					'wiz-settings[shop-archive-max-width]'
				],
				callback: function( blog_width ) {

					if ( 'custom' == blog_width ) {
						return true;
					}
					return false;
				}
			}
		],
	};
} )( jQuery );