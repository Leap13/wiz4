<?php
/**
 * Template for Blog
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

?>
<div <?php wiz_blog_layout_class( 'blog-layout-1' ); ?>>

	<div class="post-content leap-col-md-12">

		<?php wiz_blog_post_order(); ?>

	</div><!-- .post-content -->

</div> <!-- .blog-layout-1 -->
