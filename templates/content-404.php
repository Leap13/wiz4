<?php
/**
 * Template part for displaying a 404 page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wiz
 * @since 1.0.0
 */

?>

<?php wiz_entry_before(); ?>

<section class="error-404 not-found">

	<?php wiz_entry_top(); ?>

	<?php wiz_404_page(); ?>

	<?php wiz_entry_bottom(); ?>

</section><!-- .error-404 -->

<?php wiz_entry_after(); ?>
