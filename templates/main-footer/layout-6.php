<?php
/**
 * Footer Layout 5
 *
 * @since   Wiz 1.0.0
 */

/**
 * Hide main footer markup if:
 *
 * - User is not logged in. [AND]
 * - All widgets are not active.
 */
if ( ! is_user_logged_in() ) {
	if (
		! is_active_sidebar( 'main-footer-widget-1' ) &&
		! is_active_sidebar( 'main-footer-widget-2' ) &&
		! is_active_sidebar( 'main-footer-widget-3' ) &&
        ! is_active_sidebar( 'main-footer-widget-4' ) &&
        ! is_active_sidebar( 'main-footer-widget-5' )
        
	) {
		return;
	}
}

$classes[] = 'wiz-footer';
$classes[] = 'wiz-footer-layout-5';
if(wiz_get_option('enable-footer-content-center')) {
	$classes[] = 'wiz-footer-align-center';
}
$classes   = implode( ' ', $classes );
?>

<div class="<?php echo esc_attr( $classes ); ?>">
	<div class="wiz-footer-overlay">
		<div class="leap-container">
			<div class="leap-row">
				<div class="leap-col-lg-2 leap-col-md-2 leap-col-sm-12 leap-col-xs-12 wiz-footer-widget wiz-footer-widget-1">
					<?php wiz_get_footer_widget( 'main-footer-widget-1' ); ?>
				</div>
				<div class="leap-col-lg-2 leap-col-md-2 leap-col-sm-12 leap-col-xs-12 wiz-footer-widget wiz-footer-widget-2">
					<?php wiz_get_footer_widget( 'main-footer-widget-2' ); ?>
				</div>
				<div class="leap-col-lg-2 leap-col-md-2 leap-col-sm-12 leap-col-xs-12 wiz-footer-widget wiz-footer-widget-3">
					<?php wiz_get_footer_widget( 'main-footer-widget-3' ); ?>
				</div>
				<div class="leap-col-lg-2 leap-col-md-2 leap-col-sm-12 leap-col-xs-12 wiz-footer-widget wiz-footer-widget-4">
					<?php wiz_get_footer_widget( 'main-footer-widget-4' ); ?>
				</div>
                <div class="leap-col-lg-2 leap-col-md-2 leap-col-sm-12 leap-col-xs-12 wiz-footer-widget wiz-footer-widget-5">
					<?php wiz_get_footer_widget( 'main-footer-widget-5' ); ?>
				</div>
			</div><!-- .leap-row -->
		</div><!-- .leap-container -->
	</div><!-- .wiz-footer-overlay-->
</div><!-- .leap-theme-footer .wiz-footer-layout-5 -->
