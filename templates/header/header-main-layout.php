<?php
/**
 * Template for Header
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

?>

<div class="main-header-bar-wrap">
	<div class="main-header-bar">
		<?php wiz_main_header_bar_top(); ?>
		<div class="leap-container">

			<div class="leap-flex main-header-container">
				<?php wiz_sitehead_content(); ?>
			</div><!-- Main Header Container -->
		</div><!-- leap-row -->
		<?php wiz_main_header_bar_bottom(); ?>
	</div> <!-- Main Header Bar -->
</div> <!-- Main Header Bar Wrap -->
