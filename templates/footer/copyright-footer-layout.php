<?php
/**
 * Template for Footer Copyright Layout 1
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

$section_1 = wiz_get_copyright_footer( 'footer-copyright-section-1' );
$section_2 = wiz_get_copyright_footer( 'footer-copyright-section-2' );

?>

<div class="leap-footer-copyright copyright-footer-layout-1">
	<div class="leap-footer-copyright-content">
		<div class="leap-container">
			<div class="leap-footer-copyright-wrap" >
				<?php if ( $section_1 ) : ?>
					<div class="leap-footer-copyright-section leap-footer-copyright-section-1" >
                        <?php echo wp_kses_post($section_1); ?>
					</div>
				<?php endif; ?>

				<?php if ( $section_2 ) : ?>
					<div class="leap-footer-copyright-section leap-footer-copyright-section-2" >
						<?php echo wp_kses_post($section_2); ?>
					</div>
				<?php endif; ?>

			</div><!-- .leap-row .leap-footer-copyright-wrap -->
		</div><!-- .leap-container -->
	</div><!-- .leap-footer-copyright-content -->
</div><!-- .leap-footer-copyright-->
