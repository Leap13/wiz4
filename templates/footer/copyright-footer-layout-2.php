<?php
/**
 * Template for Small Footer Layout 2
 *
 * @package     Wiz
 * @author      Wiz
 * @copyright   Copyright (c) 2019, Wiz
 * @link        https://themes.leap13.com/wiz/
 * @since       Wiz 1.0.0
 */

$section_1 = wiz_get_copyright_footer( 'footer-copyright-section-1' );
$section_2 = wiz_get_copyright_footer( 'footer-copyright-section-2' );
$sections  = 0;

if ( '' != $section_1 ) {
	$sections++;
}

if ( '' != $section_2 ) {
	$sections++;
}

switch ( $sections ) {

	case '2':
			$section_class = 'leap-footer-copyright-section-equally leap-col-md-6 leap-col-xs-12';
		break;

	case '1':
	default:
			$section_class = 'leap-footer-copyright-section-equally leap-col-xs-12';
		break;
}

?>

<div class="leap-footer-copyright copyright-footer-layout-2">
	<div class="leap-footer-copyright-content">
		<div class="leap-container">
			<div class="leap-footer-copyright-wrap" >
					<div class="leap-row leap-flex">

					<?php if ( $section_1 ) : ?>
						<div class="leap-footer-copyright-section leap-footer-copyright-section-1 <?php echo esc_attr( $section_class ); ?>" >
							<?php echo $section_1; ?>
						</div>
				<?php endif; ?>

					<?php if ( $section_2 ) : ?>
						<div class="leap-footer-copyright-section leap-footer-copyright-section-2 <?php echo esc_attr( $section_class ); ?>" >
							<?php echo $section_2; ?>
						</div>
				<?php endif; ?>

					</div> <!-- .leap-row.leap-flex -->
			</div><!-- .leap-footer-copyright-wrap -->
		</div><!-- .leap-container -->
	</div><!-- .leap-footer-copyright-content -->
</div><!-- .leap-footer-copyright-->
