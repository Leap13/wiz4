<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wiz
 * 
 */

?>

			</div> <!-- Wiz-container -->

		</div><!-- #content --> 

		<?php wiz_footer(); ?>

	</div><!-- #page -->

	<?php wiz_body_bottom(); ?>

	<?php wp_footer(); ?>

	</body>
</html>
